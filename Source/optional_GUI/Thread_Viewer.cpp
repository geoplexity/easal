/*
 This file is part of EASAL. 

 EASAL is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 EASAL is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "Thread_Viewer.h"

#include "AtlasNodeSelectionWindow.h"
#include "ActiveConstraintGraph.h"
#include "ActiveConstraintRegion.h"
#include "AtlasNode.h"
#include "CayleySpaceView.h"
#include "SweepView.h"


#ifdef __APPLE__
    #include <GLUT/glut.h>
#else
    #include <GL/glut.h>
    #include <GL/glu.h>
#endif

#include <fox/fx.h>



#include <string>
#include <vector>
#include <queue>

using namespace std;


pthread_mutex_t space_sync;
pthread_mutex_t sweep_sync;

////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////

struct View {
	double tranX,tranY,tranZ;//the translations of the view
	double rotX,rotY;//the rotations of the view
	double scale;//scale of the view
};

/*
 * The Window structure holds the data to keep the window views information when multiple views are used.
 */
struct Window {
    int win;//identifier assigned by glut
    int pMouseX, pMouseY; //the previous position of the mouse in window space
    int mouseX, mouseY;   //the position of the mouse in window space
    // double rotX,rotY;      //the rotations of the view
    // double s[3];         //scale of the view
    View v[3];       // view information for the window

    GLfloat lft, rght, bot, top, ner, fr;//frustum dimentions


    int subwin; // id for the subwindow assigned by glut;
    double srotX,srotY;      //the rotations of the subview
    double sSub;      //the scale of the subview




    double ratio;           // ratio of the view;
    bool wire;          //rendering option for the sweep view
    // bool grav;           //whether the "gravity" is used in the roadmap view
    // bool tree;           //show only the part of the graph which is related the the active point
    unsigned int view_mode;   //Which View Mode the window is in.
    ActiveConstraintGraph* cgk; //the contact graph associated with the window
    ActiveConstraintRegion* region;
    AtlasNode* rnode;
    int nodenum;        // the node number associated with the window
    int prevnodenum;     // the node number previously associated with the window
    int bound;          //  The decided bound;

    int flip;     // the flipnumber number associated with the realization showed in the subwindow
    CayleyPoint* pnt;   // the point for display in the subwindow

    CayleySpaceView* spaceView; // each window has it own space view settings
    SweepView* sweepView; // each window has its own sweep "realization" view settings
};

bool operator==(Window a, Window b){
    return (a.win == b.win);
}


////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////


namespace tvvars {

    const unsigned int
        ROAD  = 0, //denotes that the Roadmap graph is shown
        SPACE = 1, //show the array of cubes(points) as a spacial relation of the samples
        SWEEP = 2; //shows the actual realizations of the molecules
	string hint[3];

    int argc;
    char** argv;

    vector<Window> windows;

    bool atlas_builder_paused;
    bool loadingFlag;

    static float mat_white[4]={1.0, 1.0, 1.0, 1.0};
    static float mat_specular[]={.5, .5, .5, 1.0};
    static float mat_ambient[]={1.0, 1.0, 1.0, 1.0};
    static float mat_shininess={100.0};
    static float light_ambient[]={0.3, 0.3, 0.3, 1.0};
    static float light_diffuse[]={0.9, 0.9, 0.9, 1.0};
    static float light_specular[]={1.0, 1.0, 1.0, 1.0};
    static float light_position[]={5.0, 20.0,40.0, 0.0};

    const double size = 40.0; //size of rendering axis limits

    bool track = false;


    MolecularUnit *muA;
    MolecularUnit *muB;

    AtlasDisplay *atlas_display;
    Thread_BackEnd *tb;
}


// Gets the current index of the window being used.
int currentWindowIndex() {
    if (tvvars::windows.size() <= 1) return 0;

    int curwin = glutGetWindow();
    for (size_t windowID = 0; windowID < tvvars::windows.size(); windowID++) {
        if (curwin == tvvars::windows[windowID].win || curwin == tvvars::windows[windowID].subwin) {
            return windowID;
        }
    }
    return 0;
}


Window* currentWindow() {
    return &(tvvars::windows[currentWindowIndex()]);
}


bool weAreStarting() { 
	struct  stat buffer;
	string node0(ThreadShare::save_loader->relativePath+"Node0.txt");
	string settingsini(ThreadShare::save_loader->relativePath+"settings.ini");
	if((stat(node0.c_str(),&buffer)==0) && (stat(settingsini.c_str(),&buffer)==0) ) {
		return false;
	} else {
		return true;
	}
}


void display();
void idleDisplay();
int interrupt();
void setActiveNode(Window* window, int num);
void keyboard(unsigned char,int,int);
void specialKeyboard(int,int,int);
void mouse(int,int,int,int);
void passMouse(int,int);
void mouseMotion(int,int);
void reshape(int,int);
void drawString(double,double,double,string);
void drawString24(double,double,double,string);
void drawString8by13(double,double,double,string);

void mouseMotion_subwindow(int,int);
void display_subwindow();
void keyboard_subwindow(unsigned char,int,int);

void newWindow(string name,int node = 0);



////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////



Thread_Viewer::Thread_Viewer(
    int argc,
    char **argv,
    MolecularUnit *muA,
    MolecularUnit *muB,
    SaveLoader *save_loader,
    AtlasBuilder *atlas_builder,
    Atlas *atlas_view, Thread_BackEnd *tb)
    : Thread()
{
	pthread_mutex_init(&space_sync,NULL);
	pthread_mutex_init(&sweep_sync,NULL);
    tvvars::argc = argc;
    tvvars::argv = argv;
    tvvars::muA = muA;
    tvvars::muB = muB;
    ThreadShare::save_loader = save_loader;
    ThreadShare::atlas_builder = atlas_builder;
    ThreadShare::atlas_view = atlas_view;

    tvvars::atlas_display = new AtlasDisplay(ThreadShare::atlas_view, ThreadShare::save_loader);
    tvvars::tb = tb;
}



void Thread_Viewer::run() {
    cout << "Thread_Viewer: Run." << endl;

    glutInit(&tvvars::argc, tvvars::argv);  // comment here if you need just statistic method
    glutInitDisplayMode (GLUT_DOUBLE | GLUT_RGBA | GLUT_DEPTH);


    // the hint array contains the information which appears in the upper left hand corner of the heads up display (HUD)
    tvvars::hint[tvvars::ROAD] = "ATLAS VIEW";
		//	\n\n[ESC] Close program\n[SPACE] Change to space view\n[+],[-] change zoom\n[T] toggle tree view\n[G] toggle contracting gravity\n[F] toggle forces\n[L] forceless graph arrangement\n[I],[J],[K] align view to axis\n[1] toggle number visibility";
    tvvars::hint[tvvars::SPACE] = "SPACE VIEW";
	//\n\n[ESC] Change to RoadMap view\n[SPACE] Change to realization view\n[+],[-] change zoom\n[B] toggle boundary view\n[2],[3] change display to 2D or 3D\n[<-],[->] move 4th dim slider\n[up],[down] move 3rd dim slider";
    tvvars::hint[tvvars::SWEEP] = "REALIZATION VIEW";
	//\n\n[ESC] Change to space view\n[SPACE] Change to Atlas view\n[+],[-] change zoom\n[V] view entire valid space\n[up],[down] change viewed realization";

    tvvars::atlas_builder_paused = false;
    tvvars::loadingFlag = false;

    newWindow("Molecular Bonding");
    cerr << "\nThread_Viewer: Window done init\n";

    glutMainLoop();
}

/*
 * This function is set to update the render in all of the windows.
 */
void idleDisplay() {
    for (vector<Window>::iterator it = tvvars::windows.begin();
        it != tvvars::windows.end();
        it++)
    {
        glutSetWindow(it->subwin);
        glutPostRedisplay();

        glutSetWindow(it->win);
        glutPostRedisplay();
    }
}




/*
 * This function sets up a new glut window for the system.
 * Setting all of the initial information in the associated Window struct
 */
void newWindow(string name, int node){
    int cur_wind_idx = currentWindowIndex();

	int window_width = glutGet(GLUT_SCREEN_WIDTH);
	int window_height = glutGet(GLUT_SCREEN_HEIGHT);

    glutInitWindowSize(window_width, window_height);
    glutInitWindowPosition(cur_wind_idx*10+10,cur_wind_idx*10+20);
    Window w;
    w.sweepView = new SweepView(tvvars::muA, tvvars::muB);

    AtlasNode* rnod = ThreadShare::save_loader->loadAtlasNode(node);
    w.rnode = rnod;
    w.cgk = rnod->getCG();
    w.region = rnod->getACR();
    ThreadShare::save_loader->loadNode(node, w.region);

    w.spaceView = new CayleySpaceView(w.rnode, ThreadShare::atlas_view, ThreadShare::save_loader);

    w.sweepView->setSpace(w.spaceView->getActiveIndex(),w.rnode, w.spaceView->getBoundList() );

    w.nodenum = node;
    w.prevnodenum = node;
    w.win= glutCreateWindow(name.c_str());
    w.flip = 0;
    w.bound = -1;
    w.pnt = w.spaceView->getCanonPnt();
    w.v[tvvars::ROAD].scale = 5.0;
    w.v[tvvars::SPACE].scale= 3.0;
    w.v[tvvars::SWEEP].scale = 2.0;
    w.sSub = 2.0;

    w.view_mode=tvvars::ROAD;

    w.lft = w.bot = w.ner = -1 * tvvars::size;
    w.rght = w.top = w.fr = tvvars::size;

    w.wire = false;
    // "camera" rotations
    for(size_t i = 0;i < 3;i++){
        w.v[i].rotX = w.v[i].rotY = 0.0;
        w.v[i].tranX = w.v[i].tranY = w.v[i].tranZ = 0.0;
    }
    w.srotX = w.srotY = 0.0;
    
    // register callbacks for first window, which is now current
    glLightfv(GL_LIGHT0, GL_POSITION, tvvars::light_position);
    glLightfv(GL_LIGHT0, GL_AMBIENT, tvvars::light_ambient);
    glLightfv(GL_LIGHT0, GL_DIFFUSE, tvvars::light_diffuse);
    glLightfv(GL_LIGHT0, GL_SPECULAR, tvvars::light_specular);

    glMaterialfv(GL_FRONT, GL_SPECULAR, tvvars::mat_specular);
    glMaterialfv(GL_FRONT, GL_AMBIENT, tvvars::mat_ambient);
    glMaterialfv(GL_FRONT, GL_DIFFUSE, tvvars::mat_white);
    glMaterialf(GL_FRONT, GL_SHININESS, tvvars::mat_shininess);

    glColor3f(1.0, 0.0, 0.0);
    glLineWidth(2.0);
    glutReshapeFunc(reshape);
    glutMouseFunc(mouse);
    glutMotionFunc(mouseMotion);
    glutPassiveMotionFunc(passMouse);
    glutKeyboardFunc(keyboard);
    glutSpecialFunc(specialKeyboard);
    glutDisplayFunc(display);
    glutIdleFunc(idleDisplay);

    glClearColor (1.0, 1.0, 1.0, 1.0);

    glEnable(GL_SMOOTH);
    glEnable(GL_LIGHTING);
    glEnable(GL_LIGHT0);

    glDepthFunc(GL_LEQUAL);
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_NORMALIZE);
    glEnable(GL_AUTO_NORMAL);

    glEnable(GL_LINE_SMOOTH);
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA);
    glHint(GL_LINE_SMOOTH_HINT,GL_NICEST);

    glutInitWindowSize (300, 300);
    w.ratio = 1.0;
    w.subwin = glutCreateSubWindow(w.win,400,000,200,200);

    glutDisplayFunc(display_subwindow);
    glutMotionFunc(mouseMotion_subwindow);
    glutPassiveMotionFunc(passMouse);
    glutKeyboardFunc(keyboard_subwindow);
    glutIdleFunc(idleDisplay);


    glClearColor (0.9, 0.9, 0.9, 1.0);


    glEnable(GL_SMOOTH);
    glEnable(GL_LIGHTING);
    glEnable(GL_LIGHT0);


    glEnable(GL_LINE_SMOOTH);
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA);
    glHint(GL_LINE_SMOOTH_HINT,GL_NICEST);

    glDepthFunc(GL_LEQUAL);
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_NORMALIZE);
    glEnable(GL_AUTO_NORMAL);

    glutSetWindow(w.win);

    tvvars::windows.push_back(w);
}


//TODO:closes the currentWindow (not quite working)
void closeWindow(){
	//Stop sampling the atlas
	Settings::AtlasBuilding::stop = true;
	tvvars::atlas_builder_paused = true;
	//Save the current RoadMap
	ThreadShare::save_loader->saveRoadMap(ThreadShare::atlas_view);
    for(size_t wi = 0; wi < tvvars::windows.size(); wi++){
        if(glutGetWindow() == tvvars::windows[wi].win){
            glutDestroyWindow(tvvars::windows[wi].win);
            delete tvvars::windows[wi].cgk; tvvars::windows[wi].cgk= NULL;
            tvvars::windows.erase(find(tvvars::windows.begin(),tvvars::windows.end(),tvvars::windows[wi]));
            if(tvvars::windows.empty()){
                exit(0);
            }
        }
    }
}



/*
 * The axis on the Atlas is drawn with this function.
 */
void showRoadAxis(){
    glPushMatrix();
        glDisable(GL_LIGHTING);
        glLineWidth(3);
        glColor4f(1,.5,.5,.5);
        glBegin(GL_LINES);
            glVertex3i(-5,0,0);
            glVertex3i(5,0,0);
        glEnd();
        glColor4f(.5,1,.5,.5);
        glBegin(GL_LINES);
            glVertex3i(0,-5,0);
            glVertex3i(0,5,0);
        glEnd();
        glColor4f(.5,.5,1,.5);
        glBegin(GL_LINES);
            glVertex3i(0,0,-5);
            glVertex3i(0,0,5);
        glEnd();
        glEnable(GL_LIGHTING);
    glPopMatrix();
}

/*
 * The axis on the spaceview and sweepview is drawn with this function.
 */
void showAxis(){
    glPushMatrix();
        glDisable(GL_LIGHTING);
        glLineWidth(3);
        // glColor4f(1,.5,.5,.5);
        glColor4f(1,0,0,.5);
        glBegin(GL_LINES);
            glVertex3i(10,0,0);
            glVertex3i(0,0,0);
        glEnd();
        // glColor4f(.5,1,.5,.5);
        glColor4f(0,1,0,.5);
        glBegin(GL_LINES);
            glVertex3i(0,10,0);
            glVertex3i(0,0,0);
        glEnd();
        // glColor4f(.5,.5,1,.5);
        glColor4f(0,0,1,.5);
        glBegin(GL_LINES);
            glVertex3i(0,0,10);
            glVertex3i(0,0,0);
        glEnd();

        glEnable(GL_LIGHTING);
    glPopMatrix();
}

/*
 * This function draws the strings in the Hud and on the roadmap
 * it uses scale independent bitmap fonts.
 */
void drawString(double x, double y, double z, string strn)
{
   glRasterPos3f(x, y, z);
   int row = 0;
   for (string::size_type c = 0; c < strn.length(); c++)
   {
       if(strn[c] == '\n'){
           row++;
           glRasterPos3f(x, y - (row * .04), z);
       }
      glutBitmapCharacter(GLUT_BITMAP_TIMES_ROMAN_10, strn[c]); // Updates the position
   }
}



void drawString24(double x, double y, double z, string strn){
   glRasterPos3f(x, y, z);
   int row = 0;
   for (string::size_type c = 0; c < strn.length(); c++)
   {
       if(strn[c] == '\n'){
           row++;
           glRasterPos3f(x, y - (row * .04), z);
       }
      glutBitmapCharacter(GLUT_BITMAP_HELVETICA_18, strn[c]); // Updates the position
   }
}

void drawString8by13(double x, double y, double z, string strn){
   glRasterPos3f(x, y, z);
   int row = 0;
   for (string::size_type c = 0; c < strn.length(); c++)
   {
       if(strn[c] == '\n'){
           row++;
           glRasterPos3f(x, y - (row * .04), z);
       }
      glutBitmapCharacter(GLUT_BITMAP_8_BY_13, strn[c]); // Updates the position
   }
}
		
/*
 * This function makes the showing of the contact graph easier by assigning
 * the index numbers to certain positions in the names array.
 */
void addNames(int* names, vector<pair<int,int> > links,int &left, int &right){

    bool l,r;

    for(vector< pair<int,int> >::iterator iter = links.begin(); iter !=links.end(); iter++)
    {
        l = r = false;
        for(int i  = 0; i<6 && ( !l || !r );i++) {
            if(iter->second == names[i * 2] || iter->second < 0){
                r = true;
            }
            if(iter->first == names[(i * 2) + 1] || iter->first < 0){
                l = true;
            }
        }
        if(!r){
            names[right] = iter->second;
            right += 2;
        }
        if(!l){
            names[left] = iter->first;
            left += 2;
        }

    }
}



void displayHUD_contactGraph(ActiveConstraintGraph* active) {
    double axis[6][3]= {  {0.8, 0.0, 0.0},{0.0, 0.8, 0.0},{0.0, 0.0, 0.8},{.8, .8, 0.0},{0.0, .8, .8},{0.8, 0.8, 0.8}};

    double loc[12][2]= { {-.5,-.95}, {-.8,-.95},
                      {-.5,-.87}, {-.8,-.87},
                      {-.5,-.79}, {-.8,-.79},
                      {-.5,-.71}, {-.8,-.71},
                      {-.5,-.63}, {-.8,-.63},
                      {-.5,-.55}, {-.8,-.55}
                      };


    vector< pair<int,int> > cg =  active->getParticipants();
    vector< pair<int,int> > cgParm =  active->getParamLines();
    vector< pair<int,int> > thrd;
    if(active->getVerticesA().size()>3 && active->getVerticesB().size()>3)
    {   pair<int,int> tpr = make_pair( active->getVerticesA()[2], active->getVerticesB()[2] ) ;
        thrd.push_back(tpr);
    }

    int left =1, right =0;
    int names[] = {-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1};

    addNames(names,cg,left,right);
    addNames(names,cgParm,left,right);
    if(cg.size()>0 && cg.size()<3)addNames(names,thrd,left,right);

    for(int i = 0; i<12;i++)if(names[i] >=0){
        glPushMatrix();
        glTranslatef(loc[i][0],loc[i][1],-9);

        float material3[] = {0.7,0.7,0.7,1.0};
        if(i<4){
            material3[0] = .8;
            material3[1] = .8;
            material3[2] = .8;
        }
        if( (i % 2) != 0){
            material3[1] = 1.0;
        }else{
            material3[2] = 1.0;
        }
        glMaterialfv(GL_FRONT,GL_AMBIENT_AND_DIFFUSE,material3);
        glEnable(GL_LIGHTING);
        glutSolidSphere(.03,32,4);
        glDisable(GL_LIGHTING);
        glPopMatrix();

        glColor3f(1,1,0);
        stringstream ss;
        ss << names[i];
        string prnt = ss.str();
        drawString24(loc[i][0],loc[i][1],0,prnt);
        glEnable(GL_LIGHTING);
    }


    glLineWidth(3);
    glColor3f(0.5, 0.5, 0.5);
    glBegin(GL_LINES);
    for(vector< pair<int,int> >::iterator iter = cg.begin(); iter !=cg.end(); iter++)
    {

        for(int i = 0; i<6;i++){
            if(names[(i*2)+1] == iter->first){
                glVertex3f(loc[(i*2)+1][0],loc[(i*2)+1][1],0);
            }
            if(names[i*2] == iter->second){
                glVertex3f(loc[i*2][0],loc[i*2][1],0);
            }
        }
    }
    int ax =0;
    for(vector< pair<int,int> >::iterator iter = cgParm.begin(); iter !=cgParm.end(); iter++)
    {
        glColor3f(axis[ax][0],axis[ax][1],axis[ax][2]);
        double lineLoc[2][3]; // location for line endpoints
        for(int i = 0; i<6;i++){
            if(names[(i*2)+1] == iter->first){
                // glVertex3f(loc[(i*2)+1][0],loc[(i*2)+1][1],0);
                lineLoc[0][0] = loc[(i*2)+1][0];
                lineLoc[0][1] = loc[(i*2)+1][1];
                lineLoc[0][2] = 0;
            }
            if(names[i*2] == iter->second){
                // glVertex3f(loc[i*2][0],loc[i*2][1],0);
                lineLoc[1][0] = loc[i*2][0];
                lineLoc[1][1] = loc[i*2][1];
                lineLoc[1][2] = 0;
            }
        }
        for(double f = 1.0;f > 0;f -=.222){
            double x1,y1;
            double x2,y2;
            double f2 = f - .111;
            x1 = ((lineLoc[0][0] * f) + (lineLoc[1][0] * (1 - f)) );
            y1 = ((lineLoc[0][1] * f) + (lineLoc[1][1] * (1 - f)) );
            glVertex3f(x1,y1,0);
            x2 = ((lineLoc[0][0] * f2) + (lineLoc[1][0] * (1 - f2)) );
            y2 = ((lineLoc[0][1] * f2) + (lineLoc[1][1] * (1 - f2)) );
            glVertex3f(x2,y2,0);
        }
        ax++;
    }
    glEnd();
}

void displayHUD_hint(const unsigned int view_mode, int activeNum) {
    glDisable(GL_LIGHTING);

    glColor3f(0.5, 0.5, 0.5);

    stringstream ss;

    // hints
    drawString24(-0.9, 0.95, 0.0, tvvars::hint[view_mode]);

    // Node and Sampling Mode info
    ss.str("");
    ss << "Selected Node =" << activeNum;
	ss << "\nCurrent Node =" << ThreadShare::atlas_view->number_of_nodes();
	ss<<"\nRun Mode ="<<ModeNames[Settings::Sampling::runMode];
    drawString24(-0.15,0.9,0.0,ss.str());

	if(Settings::Sampling::runMode != Stopped) {
			Settings::General::Status = string("Sampling in " + ModeNames[Settings::Sampling::runMode] + " Mode");
	} else {
			Settings::General::Status = string("Sampling Stopped");
	}
	ss.str(Settings::General::Status);
	drawString24(0.95, -0.99, 0.0, ss.str());
    // loading node message
    if(tvvars::loadingFlag){
        glBegin(GL_LINE_LOOP);
            glVertex3f(-0.1,0.85,0.0);
            glVertex3f(-0.1,0.78,0.0);
            glVertex3f(0.3,0.78,0.0);
            glVertex3f(0.3,0.85,0.0);
        glEnd();
        drawString(0.0,0.8,0.0, "Loading a node");
    }

    glEnable(GL_LIGHTING);
}

void displayHUD_buttons(const unsigned int view_mode) {
    glDisable(GL_LIGHTING);

    glColor3f(0.5, 0.5, 0.5);

    glPushName(10);
    glPushName(0);//up-previous arrow

        glBegin(GL_TRIANGLES);
            glVertex3f(-0.955, 0.96, 0.0);
            glVertex3f(-0.975, 0.98, 0.0);
            glVertex3f(-0.995, 0.96, 0.0);
        glEnd();

    glLoadName(1);//down-next arrow

        glBegin(GL_TRIANGLES);
            glVertex3f(-0.955, 0.955, 0.0);
            glVertex3f(-0.975, 0.935, 0.0);
            glVertex3f(-0.995, 0.955, 0.0);
        glEnd();

    if (view_mode == tvvars::ROAD) {
        glLoadName(3);//Stop Sampling
            glBegin(GL_QUADS);
                glVertex3f(-0.990, .45, 1.0);
                glVertex3f(-0.890, .45, 1.0);
                glVertex3f(-0.890, .35, 1.0);
                glVertex3f(-0.990, .35, 1.0);
            glEnd();

	glColor3f(0.7, 0, 0);
		glBegin(GL_QUADS); //For the inner red box
		glVertex3f(-0.920, .38, 1.0);
		glVertex3f(-0.960, .38, 1.0);
		glVertex3f(-0.960, .42, 1.0);
		glVertex3f(-0.920, .42, 1.0);
		glEnd();

       glColor3f(0.5, 0.5, 0.5);


	if(Settings::AtlasBuilding::stop ==  true || Settings::Sampling::runMode == Stopped){
	glLoadName(2);//Constraint Selection Dialogue
	glBegin(GL_QUADS);
                glVertex3f(-0.990, .33, 1.0);
                glVertex3f(-0.890, .33, 1.0);
                glVertex3f(-0.890, .23, 1.0);
                glVertex3f(-0.990, .23, 1.0);
            glEnd();
	glColor3f(0.3, 0.8, 0.3);
			glBegin(GL_QUADS); //Quads inside the dumbbell
                glVertex3f(-0.97, 0.32, 0.0);
                glVertex3f(-0.95, 0.32, 0.0);
                glVertex3f(-0.95, 0.3, 0.0);
                glVertex3f(-0.97, 0.3, 0.0);

                glVertex3f(-0.93, 0.32, 0.0);
                glVertex3f(-0.91, 0.32, 0.0);
                glVertex3f(-0.91, 0.3, 0.0);
                glVertex3f(-0.93, 0.3, 0.0);

                glVertex3f(-0.97, 0.25, 0.0);
                glVertex3f(-0.95, 0.25, 0.0);
                glVertex3f(-0.95, 0.27, 0.0);
                glVertex3f(-0.97, 0.27, 0.0);

                glVertex3f(-0.93, 0.25, 0.0);
                glVertex3f(-0.91, 0.25, 0.0);
                glVertex3f(-0.91, 0.27, 0.0);
                glVertex3f(-0.93, 0.27, 0.0);
            glEnd();
		glLoadName(4); //TreeSample
		glColor3f(0.5, 0.5, 0.5);
		glBegin(GL_QUADS);
                glVertex3f(-0.990, .21, 1.0);
                glVertex3f(-0.890, .21, 1.0);
                glVertex3f(-0.890, .11, 1.0);
                glVertex3f(-0.990, .11, 1.0);
		glEnd();
		
		glLoadName(5); //ForestSample 
		glColor3f(0.5, 0.5, 0.5);
		glBegin(GL_QUADS);
                glVertex3f(-0.990, .09, 1.0);
                glVertex3f(-0.890, .09, 1.0);
                glVertex3f(-0.890, -.01, 1.0);
                glVertex3f(-0.990, -.01, 1.0);
		glEnd(); 

		
		glLoadName(6); //ForestSampleAS 
		glColor3f(0.5, 0.5, 0.5);
		glBegin(GL_QUADS);
                glVertex3f(-0.990, -.03, 1.0);
                glVertex3f(-0.890, -.03, 1.0);
                glVertex3f(-0.890, -.13, 1.0);
                glVertex3f(-0.990, -.13, 1.0);
		glEnd();
		
		glLoadName(7); //BFS 
		glColor3f(0.5, 0.5, 0.5);
		glBegin(GL_QUADS);
                glVertex3f(-0.990,  -.15, 1.0);
                glVertex3f(-0.890, -.15, 1.0);
                glVertex3f(-0.890, -.25, 1.0);
                glVertex3f(-0.990, -.25, 1.0);
		glEnd();
		
		glColor3f(0,0,0); 
		glBegin(GL_LINES);//The Tree inside the Quad
		glVertex3f(-0.940, -.17, 1.0);
		glVertex3f(-0.920, -.19, 1.0);
		glEnd(); 
	
		glBegin(GL_LINES);
		glVertex3f(-0.940, -.17, 1.0);
		glVertex3f(-0.960, -.19, 1.0);
		glEnd(); 
		
		glBegin(GL_LINES);
		glVertex3f(-0.975, -.22, 1.0);
		glVertex3f(-0.960, -.19, 1.0);
		glEnd(); 
		
		glBegin(GL_LINES);
		glVertex3f(-0.945, -.22, 1.0);
		glVertex3f(-0.960, -.19, 1.0);
		glEnd(); 
		
		glBegin(GL_LINES);
		glVertex3f(-0.905, -.22, 1.0);
		glVertex3f(-0.920, -.19, 1.0);
		glEnd(); 
		
		glBegin(GL_LINES);
		glVertex3f(-0.935, -.22, 1.0);
		glVertex3f(-0.920, -.19, 1.0);
		glEnd(); 
		
		glBegin(GL_LINES);
		glVertex3f(-0.900, -.24, 1.0);
		glVertex3f(-0.980, -.24, 1.0);
		glEnd(); 
		
		glBegin(GL_LINES);
		glVertex3f(-0.900, -.24, 1.0);
		glVertex3f(-0.91, -.23, 1.0);
		glEnd();
		
		glBegin(GL_LINES);
		glVertex3f(-0.900, -.24, 1.0);
		glVertex3f(-0.91, -.25, 1.0);
		glEnd();
		
		glLoadName(8); // Refine
		glColor3f(0.5, 0.5, 0.5);
		glBegin(GL_QUADS);
                glVertex3f(-0.990, -.27, 1.0);
                glVertex3f(-0.890, -.27, 1.0);
                glVertex3f(-0.890, -.37, 1.0);
                glVertex3f(-0.990, -.37, 1.0);
		glEnd();
		
		glBegin(GL_QUADS);//Inner refine quads
		glColor3f(0,0,0);
		glVertex3f(-0.980, -0.28, 0.0);
		glVertex3f(-0.950, -0.28, 0.2);
		glVertex3f(-0.950, -0.36, 0.0);
		glVertex3f(-0.980, -0.36, 0.0); 
		
		glVertex3f(-0.940, -0.28, 0.0);
		glVertex3f(-0.920, -0.28, 0.2);
		glVertex3f(-0.920, -0.36, 0.0);
		glVertex3f(-0.940, -0.36, 0.0); 
		
		glVertex3f(-0.910, -0.28, 0.0);
		glVertex3f(-0.90, -0.28, 0.0);
		glVertex3f(-0.90, -0.36, 0.0);
		glVertex3f(-0.910, -0.36, 0.0); 
		glEnd(); 
		
		drawString8by13(-0.967, 0.15, 1, string("CUR"));
		drawString8by13(-0.991, 0.03, 1, string("Cleanup"));
		drawString8by13(-0.967, -0.09, 1, string("A-S"));
	 }
}



    glPopName();
    glPopName();

    glEnable(GL_LIGHTING);
}

/*
 * This function draws the HUD elements that are common to all the screens.
 *  the contact graph.
 *  the hint.
 *  the screen changing control.
 */
void displayHUD(Window* window, bool select = false) {

    ActiveConstraintGraph* active = window->cgk;
    int activeNum = window->nodenum;

    if (active == NULL) //can happen at start
        return;

    glDisable(GL_DEPTH_TEST);
    glMatrixMode(GL_PROJECTION);

    glLoadIdentity();


    if(select){
        GLuint selectbuf[512];
        GLint viewport[4];

        glSelectBuffer(512,selectbuf);
        glGetIntegerv(GL_VIEWPORT,viewport);
        gluPickMatrix(window->mouseX, viewport[3] - window->mouseY,
                3,3,viewport);
    }


    glOrtho(-1 * window->ratio, window->ratio,-1,1,-1,10);
    glMatrixMode(GL_MODELVIEW);
    glPushMatrix();
    glLoadIdentity();

    {
        displayHUD_contactGraph(active);
        displayHUD_hint(window->view_mode, activeNum);
        displayHUD_buttons(window->view_mode);
    }

    glMatrixMode(GL_PROJECTION);

    glMatrixMode(GL_MODELVIEW);
    glPopMatrix();
    glEnable(GL_DEPTH_TEST);
}

/*
 * the display_subwindow is the function to show the sweepview in the upper right part of the screen.
 */
void display_subwindow() {
    Window *cur_wind = currentWindow();

    glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);

    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
        GLfloat lft, rght,bot,top,ner,fr;
        lft = bot = ner = -1 * tvvars::size;
        rght = top = fr = tvvars::size;
    glOrtho(lft,rght,bot,top,ner*2,fr*2);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    glPushMatrix();
    glRotatef(cur_wind->srotX,1,0,0);
    glRotatef(cur_wind->srotY,0,1,0);
    glScalef(cur_wind->sSub,cur_wind->sSub,cur_wind->sSub);

    cur_wind->sweepView->display();

    glPopMatrix();
    glutSwapBuffers();

}

/*
 * This is the main render loop where it shows the various windows.
 */
void display(){

    int cur_wind_idx = currentWindowIndex();
    // Window *cur_wind = currentWindow();
    Window *cur_wind = &(tvvars::windows[cur_wind_idx]);

    glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);

    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glOrtho(cur_wind->lft, cur_wind->rght,cur_wind->bot,cur_wind->top,cur_wind->ner*10,cur_wind->fr*10);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

    glPushMatrix();
    View vw = cur_wind->v[cur_wind->view_mode];
    glRotatef(vw.rotX,1,0,0);
    glRotatef(vw.rotY,0,1,0);
    glScalef(vw.scale,vw.scale,vw.scale);
    glTranslatef(vw.tranX,vw.tranY,vw.tranZ);
    //this section is for the OpenGL functions or calls in order to display
    // things in 3D space

    if(cur_wind->view_mode == tvvars::ROAD){// roadMapView
        showRoadAxis();
        if(cur_wind_idx == 0){
            tvvars::atlas_display->applyForces();
        }
        tvvars::atlas_display->display(cur_wind->nodenum);
        tvvars::atlas_display->displayHUD(cur_wind->ratio);
    } else if(cur_wind->view_mode == tvvars::SPACE) {//Spaceview
        double* trans = cur_wind->spaceView->getXYZmid();
        glTranslatef(-1 * trans[0] ,-1 * trans[1] ,-1 * trans[2 ] );
        showAxis();
        cur_wind->spaceView->display();
        cur_wind->spaceView->displayHUD(cur_wind->cgk,cur_wind->nodenum,cur_wind->ratio);
    } else if(cur_wind->view_mode == tvvars::SWEEP) {//Sweep- realization view.
        showAxis();
        cur_wind->sweepView->display(cur_wind->wire);
        cur_wind->sweepView->displayHUD(cur_wind->ratio);
    }
    displayHUD(cur_wind);
    glPopMatrix();

    glutSwapBuffers();
}


/*
 * This function takes care of the actions needed to change which node is active.
 * Including Loading and changing of the data.
 */
void setActiveNode(Window* window, int num) {
    tvvars::loadingFlag = true;
    window->v[window->view_mode].tranX = 0.0;
    window->v[window->view_mode].tranY = 0.0;
    window->v[window->view_mode].tranZ = 0.0;
    display();

    AtlasNode* rnode = (*ThreadShare::atlas_view)[num];
    window->rnode = rnode;
    window->cgk = rnode->getCG();  //new ActiveConstraintGraph();

    window->region->trim();
    window->region = rnode->getACR();

    ThreadShare::save_loader->loadNode(num, window->region);
	pthread_mutex_lock(&space_sync);
    window->spaceView->setSpace(window->rnode);
    window->sweepView->setSpace(window->spaceView->getActiveIndex() ,rnode , window->spaceView->getBoundList());
	pthread_mutex_unlock(&space_sync);
    window->prevnodenum = window->nodenum;
    window->nodenum = num;
    if(ThreadShare::atlas_view->isConnected(window->prevnodenum,window->nodenum)){
        window->bound = window->prevnodenum;
        window->pnt = window->spaceView->getBoundPnt(window->bound);
        if(window->pnt == NULL){
            window->pnt = window->spaceView->getCanonPnt();
        }
    }else{
        window->pnt = window->spaceView->getCanonPnt();
    }
    idleDisplay();
    tvvars::loadingFlag = false;
	ThreadShare::atlas_view->findpath(window->nodenum, window->prevnodenum);
}


void keyboard_road(Window* window, unsigned char key, int x, int y) {
    switch(key) {
        //roadmapview to toggle gravity
        case '1':
            tvvars::atlas_display->toggleNums();
            break;

        //roadmapview to toggle gravity
        case 'g':
        case 'G':
            tvvars::atlas_display->toggleGrav();
            break;


        case 'W':
        case 'w':
            tvvars::atlas_display->showAtlasEdges();
            break;

        //roadmapview to toggle gravity
        case 'f':
        case 'F':
            tvvars::atlas_display->toggleForce();
            break;

        //roadmapview to toggle gravity
        case 'l':
        case 'L':
            tvvars::atlas_display->applyLevels();
            break;

	//Toggle Tree View
        case 't':
        case 'T':
            tvvars::atlas_display->toggleTree(window->nodenum);
            break;

        //escape to end
        case 27:
            {
				//Stop sampling the atlas
                Settings::AtlasBuilding::stop = true;
                tvvars::atlas_builder_paused = true;
				//Save the current RoadMap
				ThreadShare::save_loader->saveRoadMap(ThreadShare::atlas_view);
				//Exit
                exit(0);
            }
            break;
        case 'd':
        case 'D':
            {
		if(weAreStarting())
			ThreadShare::atlas_builder->setup();
		Settings::AtlasBuilding::stop = true;
		tvvars::atlas_builder_paused = true;
                FXApp app("Atlas Node Select","UF CISE");
                app.init(tvvars::argc,tvvars::argv);
                AtlasNodeSelectionWindow dumbbellDialog(&app,ThreadShare::atlas_view);
                dumbbellDialog.setMolecularUnits(tvvars::muA,tvvars::muB);
                app.create();
                app.run();
                ActiveConstraintGraph* grph = dumbbellDialog.getGraph();
                if(grph != NULL){
                    grph->setStepSize(Settings::Sampling::stepSize);
                    int num = ThreadShare::atlas_view->findNodeNum(grph);
                    if(num > -1){
						delete grph;
						setActiveNode(window, num);
                    }else{
						ThreadShare::atlas_view->addNode(grph, num);
						//TODO:ThreadShare::save_loader->saveNode( ThreadShare::atlas_view->getNode(num) );
						setActiveNode(window, num);

						AtlasNode* rnode_ID = (*ThreadShare::atlas_view)[num];
						ThreadShare::save_loader->appendDimension(rnode_ID );
                    }
                }
            }
            break;
        //save to data;
        case 's':
        case 'S':
            {
                ThreadShare::save_loader->saveRoadMap(ThreadShare::atlas_view);
            }
        break;
        case 'a':
        case 'A':
            cout << "AA" << endl;
            tvvars::atlas_display->toggleShowAll();
            break;
    }
}

void keyboard_space(Window* window, unsigned char key, int x, int y) {
    switch(key) {
        //3D view of sample points
        case '6':
            window->spaceView->setViewdim(6);
            break;

        //3D view of sample points
        case '3':
            window->spaceView->setViewdim(3);
            break;
        //2D View of sample points.
        case '2':
            window->spaceView->setViewdim(2);
            break;
        //Show boundaries
        case 'b':
        case 'B':
            window->spaceView->toggleBoundaries();
            break;

        //projection on grid view for jacobian sampling
        case 'j':
        case 'J':
            window->spaceView->setGridView();
            break;

        //decrement boundaries
        case ',':
        case '<':
            window->spaceView->incBoundaries(-1);
            break;

        //increment boundaries
        case '.':
        case '>':
            window->spaceView->incBoundaries(1);
            break;
    }
}

void keyboard_sweep(Window* window, unsigned char key, int x, int y) {
    switch(key) {
        //sweepview to track specific flip
        case 't':
        case 'T':
            {
                tvvars::track = !tvvars::track;
                window->sweepView->trackFlip(-1, tvvars::track);
            }
            break;

        //sweepview to show all
        case 'v':
        case 'V':
            window->sweepView->toggleShowall();
            break;

        //sweepview to showinterior
        case 'r':
            window->sweepView->showinterior();
            break;

        //Show flip 1
        case '1':
            if(!tvvars::track)
                window->sweepView->setMulti(0);
            else
                window->sweepView->trackFlip(1, tvvars::track);
            break;

        //Show flip 2
        case '2':
            if(tvvars::track)
                window->sweepView->trackFlip(2, tvvars::track);
            break;

        //Show flip 3
        case '3':
            if(!tvvars::track)
                window->sweepView->setMulti(2);
            else
                window->sweepView->trackFlip(3, tvvars::track);
            break;

        //Show flip 4
        case '4':
            if(tvvars::track)
                window->sweepView->trackFlip(4, tvvars::track);
            break;

        //Show flip 5
        case '5':
            if(!tvvars::track)
                window->sweepView->setMulti(4);
            else
                window->sweepView->trackFlip(5, tvvars::track);
            break;

        //Show flip 6
        case '6':
            if(tvvars::track)
                window->sweepView->trackFlip(6, tvvars::track);
            break;

        //Show flip 7
        case '7':
            if(tvvars::track)
                window->sweepView->trackFlip(7, tvvars::track);
            break;

        //Show flip 0
        case '0':
            if(tvvars::track)
                window->sweepView->trackFlip(0, tvvars::track);
            break;
        //show and hide the whole stree
        case 's':
        case 'S':
            window->sweepView->toggleShowstree();
            break;

		//Show Lines
        case 'l':
            window->sweepView->showLines();
            break;

    }
}


/*
 * Standard character keys are handled by this function.
 */
void keyboard(unsigned char key, int x, int y) {
    Window *cur_wind = currentWindow();

    // Handle specific views
    if      (cur_wind->view_mode == tvvars::ROAD)  keyboard_road(cur_wind, key, x, y);
    else if (cur_wind->view_mode == tvvars::SPACE) keyboard_space(cur_wind, key, x, y);
    else if (cur_wind->view_mode == tvvars::SWEEP) keyboard_sweep(cur_wind, key, x, y);

    // Handle keys that affect all of the views
    switch(key) {
        //change scale
        case '+':
        case '=':
            cur_wind->v[cur_wind->view_mode].scale *=1.1;
            break;

        case '-':
        case '_':
            cur_wind->v[cur_wind->view_mode].scale *=.91;
            break;
        case ' ':

            if(cur_wind->view_mode==tvvars::SPACE) {

            }else if(cur_wind->view_mode==tvvars::SWEEP){
                cur_wind->sweepView->togglesOff();

            }else{

            }
            cur_wind->view_mode += 1;
            cur_wind->view_mode = cur_wind->view_mode % 3;
            if(cur_wind->view_mode==tvvars::SPACE){
                cur_wind->spaceView->setSpace(cur_wind->rnode);
            }else if(cur_wind->view_mode==tvvars::SWEEP){
                cur_wind->sweepView->setSpace(cur_wind->spaceView->getActiveIndex(),cur_wind->rnode, cur_wind->spaceView->getBoundList() );
                glutSetWindow(cur_wind->subwin);
                glutHideWindow();
            }else{
                glutSetWindow(cur_wind->subwin);
                glutShowWindow();
            }

            break;
        //TODO: Making a new window (Experimental - seems to work)
        case 'N':
        case 'n':
            newWindow("alt",cur_wind->nodenum);
            break;

        // deleteing a new window (Experimental - seems not to work)
        case 'X':
        case 'x':
            closeWindow();
            break;
        //View along axis.
        case 'j':
        case 'J':
            cur_wind->v[cur_wind->view_mode].rotX =0;
            cur_wind->v[cur_wind->view_mode].rotY=0;
            break;
        case 'k':
        case 'K':
            cur_wind->v[cur_wind->view_mode].rotX =0;
            cur_wind->v[cur_wind->view_mode].rotY=90;
            break;
        case 'i':
        case 'I':
            cur_wind->v[cur_wind->view_mode].rotX =90;
            cur_wind->v[cur_wind->view_mode].rotY=0;
            break;

        case '?': //Prints current node
            cout << "Active" << endl;
            cout << *(cur_wind->cgk) << endl;
            cout << "\n road node version" <<endl;
            {
            int nodeID = ThreadShare::atlas_view->findNodeNum(cur_wind->cgk);
            cout << *( (*ThreadShare::atlas_view)[nodeID]->getCG() ) << endl;
            }
            break;

        case 13 ://enter
            setActiveNode(cur_wind, cur_wind->nodenum);
            break;

        //escape to end or go back a screen
        case 27:
            {
                cur_wind->view_mode--;
                cur_wind->view_mode = cur_wind->view_mode % 3;
                if(cur_wind->view_mode==tvvars::SPACE){
                    cur_wind->spaceView->setSpace(cur_wind->rnode);
                    cur_wind->spaceView->setActive(cur_wind->sweepView->getActive());
                    glutSetWindow(cur_wind->subwin);
                    glutHideWindow();
                }else if(cur_wind->view_mode==tvvars::SWEEP){
                    cur_wind->sweepView->setSpace(cur_wind->spaceView->getActiveIndex(),cur_wind->rnode, cur_wind->spaceView->getBoundList() );
                }else{
                    glutSetWindow(cur_wind->subwin);
                    glutShowWindow();
                }
            }
        }
}


/*
 * This function dictates the actions possible in the subdisplay window.
 */
void keyboard_subwindow(unsigned char key, int x, int y) {
    Window* cur_wind = currentWindow();

    switch(key){
    //change scale
        case '+':
        case '=':
            cur_wind->sSub *= 1.1;
            break;

        case '-':
        case '_':
            cur_wind->sSub *= 0.91;
            break;

        default :
            if(cur_wind->view_mode == tvvars::ROAD)
                keyboard(key,x,y);
    }
}

/*
 * Non-alphanumeric keys are handled by this function.
 */
void specialKeyboard(int key, int x, int y){
    Window* cur_wind = currentWindow();

    switch(cur_wind->view_mode){
        case tvvars::ROAD:
            break;

        case tvvars::SPACE:
            switch(key){
                case GLUT_KEY_RIGHT: //scroll through 4th dimension of space
                    cur_wind->spaceView->incDim(3,cur_wind->cgk->getStepSize());
                    break;
                case GLUT_KEY_LEFT:
                    cur_wind->spaceView->incDim(3,-1 * cur_wind->cgk->getStepSize());
                    break;
                case GLUT_KEY_UP: //scroll through 4th dimension of space
                    cur_wind->spaceView->incDim(2,cur_wind->cgk->getStepSize() );
                    break;
                case GLUT_KEY_DOWN:
                    cur_wind->spaceView->incDim(2,-1 * cur_wind->cgk->getStepSize() );
                    break;
            }
            break;

        case tvvars::SWEEP:

            switch(key){
                case GLUT_KEY_UP://scroll through valid configurations
                    cur_wind->sweepView->incIndex(1);
                    break;
                case GLUT_KEY_DOWN:
                    cur_wind->sweepView->incIndex(-1);
                    break;
            }
            break;
    }

    switch(key){
        case GLUT_KEY_F1:
            {// save the displayed orientation
                Orientation* orien = cur_wind->sweepView->getDisplayedOrient();
                ThreadShare::save_loader->writeOrientationInPDBformat(tvvars::muA, tvvars::muB, orien, "Combined.dat");
            }
            break;
    }

}

/*
 * This function translates the information in the 3D picking actions.
 */
vector<int> processHits(int hits, GLuint buffer[])
{
    int i = 0;
    GLuint j = 0, names = 0 , *ptr = NULL, minZ = 0,*ptrNames = NULL, numberOfNames = 0;
    vector<int> output;
    ptr = (GLuint *) buffer;
    minZ = 0xffffffff;
    for (i = 0; i < hits; i++) {
        names = *ptr;
        ptr++;
        if (*ptr < minZ) {
            numberOfNames = names;
            minZ = *ptr;
            ptrNames = ptr+2;
        }

        ptr += names+2;
    }
    ptr = ptrNames;
    for (j = 0; j < numberOfNames; j++,ptr++) {
        output.push_back(*ptr);
    }
    return output;
}


/*
 * This function handles mouse clicks.
 */
void mouse(int button, int state, int x, int y)
{
    Window *cur_wind = currentWindow();

    if(state == GLUT_DOWN){
        if(button == GLUT_LEFT_BUTTON )
            {
            GLuint selectbuf[512];
            GLint viewport[4];

            glSelectBuffer(512,selectbuf);
            glRenderMode(GL_SELECT);
            glGetIntegerv(GL_VIEWPORT,viewport);

            glMatrixMode(GL_PROJECTION);
            glLoadIdentity();
                GLfloat lft, rght,bot,top,ner,fr;
                lft = bot = ner = -1 * tvvars::size;
                rght = top = fr = tvvars::size;
            gluPickMatrix(x,viewport[3]-y,
                    3,3,viewport);
            glOrtho(cur_wind->lft,cur_wind-> rght,cur_wind->bot,cur_wind->top,cur_wind->ner*10,cur_wind->fr*10);
            glMatrixMode(GL_MODELVIEW);
            glLoadIdentity();

            glInitNames();
            glPushMatrix();
            View vw = cur_wind->v[cur_wind->view_mode];
            glRotatef(vw.rotX,1,0,0);
            glRotatef(vw.rotY,0,1,0);
            glScalef(vw.scale,vw.scale,vw.scale);
            glTranslatef(vw.tranX,vw.tranY,vw.tranZ);

            int hits = 0;

            if(cur_wind->view_mode == tvvars::ROAD){
                tvvars::atlas_display->display(cur_wind->nodenum,true);
                tvvars::atlas_display->displayHUD(cur_wind->ratio,true,x,y);
            }else if(cur_wind->view_mode == tvvars::SPACE){
                cur_wind->spaceView->displayHUD(cur_wind->cgk,cur_wind->nodenum,cur_wind->ratio,true,x,y);
                double* trans = cur_wind->spaceView->getXYZmid();
                glTranslatef(-1 * trans[0] ,-1 * trans[1] ,-1 * trans[2 ] );
                cur_wind->spaceView->display(true);
            }else if(cur_wind->view_mode == tvvars::SWEEP){
                cur_wind->sweepView->displayHUD(cur_wind->ratio,true,x,y);
            }


            displayHUD(cur_wind, true);
            // restoring the original projection matrix

            glPopMatrix();
            // returning to normal rendering mode
            hits = glRenderMode(GL_RENDER);

            vector<int> selectVector;
            // if there are hits process them
            if (hits > 0)
            {

                cur_wind->v[cur_wind->view_mode].tranX = 0.0;
                cur_wind->v[cur_wind->view_mode].tranY = 0.0;
                cur_wind->v[cur_wind->view_mode].tranZ = 0.0;
                selectVector = processHits(hits,selectbuf);


                if(selectVector.size() == 2 && selectVector[0] == 10){
                    switch(selectVector[1]){
                        case 0 ://prev view
                            {
                                int act = 0;
                            if(cur_wind->view_mode == tvvars::SWEEP){
                                act = cur_wind->sweepView->getActive();
                            }
                            keyboard(' ', 0, 0);
                            keyboard(' ', 0, 0);
                            if(cur_wind->view_mode == tvvars::SPACE){
                                cur_wind->spaceView->setActive(act);
                            }
                            }
                        break;

                        case 1 ://next view
                            keyboard(' ', 0, 0);
                        break;

                        case 2 ://Select a new root node and start sampling
				Settings::AtlasBuilding::stop = true;
				Settings::Sampling::runMode = Stopped;
				ThreadShare::save_loader->saveRoadMap(ThreadShare::atlas_view);
                            	keyboard('d', 0, 0);
				tvvars::tb->setNodeNumber(cur_wind->nodenum);
				Settings::Sampling::runMode = TreeSample;
				Settings::AtlasBuilding::stop = false;
				{

                            }
                        break;
                        case 3 ://Stop Sampling
						if(Settings::AtlasBuilding::stop == true) {
								Settings::Sampling::runMode = Stopped;
						} else {
								Settings::AtlasBuilding::stop = true;
								Settings::Sampling::runMode = Stopped;
						}
						break; 
						case 4://Tree Sampling
						{
								if(weAreStarting())
										return;
								Settings::AtlasBuilding::breadthFirst = false;
								cout<<"Tree Sampling"<<endl;
								if(Settings::Sampling::runMode != Stopped) {
										cout<<"There is a race condition. Tree Sampling called when mode is "<<ModeNames[Settings::Sampling::runMode]<<endl;
										exit(1);
								} else {
									Settings::Sampling::runMode = TreeSample;
									Settings::AtlasBuilding::stop = false;
								}
						}
						break;
						case 5://Forest Sampling
						{ 
								if(weAreStarting())
										return;
								Settings::AtlasBuilding::breadthFirst = false;
								cout<<"Forest Sampling."<<endl;
								if(Settings::Sampling::runMode != Stopped) {
										cout<<"There is a race condition. Forest Sampling called when mode is "<<ModeNames[Settings::Sampling::runMode]<<endl;
										exit(1);
								} else {
									Settings::Sampling::runMode = ForestSample;
									Settings::AtlasBuilding::stop = false;
								}
						}
						break;
						case 6://Forest Sampling Auto-Solve
						{ 
								Settings::AtlasBuilding::breadthFirst = false;
								cout<<"Forest Sampling Auto-Solve"<<endl;
								if(Settings::Sampling::runMode != Stopped) {
										cout<<"There is a race condition. Forest Sampling Auto-Solve called when mode is "<<ModeNames[Settings::Sampling::runMode]<<endl;
										exit(1);
								} else {
									Settings::Sampling::runMode = ForestSampleAS;
									Settings::AtlasBuilding::stop = false;
								}
						}
						break;
						case 7://BFS
						{
								if(weAreStarting())
										return;
								cout<<"Breadth First Search"<<endl;
								if(Settings::Sampling::runMode != Stopped) {
										cout<<"There is a race condition. Breadth First called when mode is "<<ModeNames[Settings::Sampling::runMode]<<endl;
										exit(1);
								} else {
									Settings::Sampling::runMode = BreadthFirst;
									Settings::AtlasBuilding::stop = false;
								}
						}
						break;
						case 8://Refine Sampling
						{
								if(weAreStarting())
										return;
								cout<<"Refine Sampling"<<endl;
								if(Settings::Sampling::runMode != Stopped) {
										cout<<"There is a race condition. Refine Sampling called when mode is "<<ModeNames[Settings::Sampling::runMode]<<endl;
										exit(1);
								} else {
									Settings::Sampling::runMode = RefineSampling;
									Settings::AtlasBuilding::stop = false;
								}
						}
						break;
					}
				}else if(selectVector.size() == 3 && selectVector[1] == 10){
                        setActiveNode(cur_wind, selectVector[2]);

                }else if(cur_wind->view_mode == tvvars::ROAD)
                    {
						if(selectVector.size() == 0) {
							return;
						}
                        if(selectVector.size() == 2 && selectVector[0] == ROADHUDID){
                            tvvars::atlas_display->takePick(selectVector);
                        }else{
                            int num = selectVector[0];
                            setActiveNode(cur_wind, num);
                        }
                    }
                else if(cur_wind->view_mode == tvvars::SPACE)
                    {
                        if(selectVector[1] == 5){
                            tvvars::loadingFlag = true;
                            display();
                        }
                        cur_wind->spaceView->takePick(selectVector);

                        tvvars::loadingFlag = false;
                        cur_wind->sweepView->setActive(cur_wind->spaceView->getActiveIndex());

                    }
                else if(cur_wind->view_mode == tvvars::SWEEP)
                    {
                        cur_wind->sweepView->takePick(selectVector);

                    }
                }
            }

    }

}

/*
 * This function handles mouse movement.
 */
void passMouse(int X, int Y){
    Window *cur_wind = currentWindow();

    cur_wind->pMouseX = cur_wind->mouseX;
    cur_wind->pMouseY = cur_wind->mouseY;
    cur_wind->mouseX = X;
    cur_wind->mouseY = Y;

}


/*
 * This function handles mouse dragging.
 */
void mouseMotion(int X, int Y)
{
    Window* cur_wind = currentWindow();

    cur_wind->pMouseX = cur_wind->mouseX;
    cur_wind->pMouseY = cur_wind->mouseY;
    cur_wind->mouseX = X;
    cur_wind->mouseY = Y;
    double deltaX = cur_wind->mouseX - cur_wind->pMouseX;
    double deltaY = cur_wind->mouseY - cur_wind->pMouseY;


    if(glutGetModifiers() == GLUT_ACTIVE_SHIFT ){
        glMatrixMode(GL_MODELVIEW);
        glPushMatrix();
            glLoadIdentity();
            glRotatef(cur_wind->v[cur_wind->view_mode].rotX,1,0,0);
            glRotatef(cur_wind->v[cur_wind->view_mode].rotY,0,1,0);
            float modelview[16];

            glGetFloatv(GL_MODELVIEW_MATRIX, modelview);
            double right[3],up[3];
                right[0] = modelview[0];
                right[1] = modelview[4];
                right[2] = modelview[8];
                up[0] = modelview[1];
                up[1] = modelview[5];
                up[2] = modelview[9];
            deltaX *= 0.10;
            deltaY *= -0.10;
            cur_wind->v[cur_wind->view_mode].tranX += right[0] * deltaX  + up[0] * deltaY;
            cur_wind->v[cur_wind->view_mode].tranY += right[1] * deltaX  + up[1] * deltaY;
            cur_wind->v[cur_wind->view_mode].tranZ += right[2] * deltaX  + up[2] * deltaY;


        glPopMatrix();


    }else{
        cur_wind->v[cur_wind->view_mode].rotY += (deltaX);
        cur_wind->v[cur_wind->view_mode].rotX += (deltaY);
    }
}

/*
 * This function handles mouse dragging in the subdisplay.
 */
void mouseMotion_subwindow(int X, int Y)
{
    Window* cur_wind = currentWindow();

    cur_wind->pMouseX = cur_wind->mouseX;
    cur_wind->pMouseY = cur_wind->mouseY;
    cur_wind->mouseX = X;
    cur_wind->mouseY = Y;
    cur_wind->srotY += (cur_wind->mouseX - cur_wind->pMouseX);
    cur_wind->srotX += (cur_wind->mouseY - cur_wind->pMouseY);
}
/*
 * When the window is reshaped the height and width are altered within this function.
 */
void reshape(int w, int h)
{
    Window* cur_wind = currentWindow();

    double ratio = double(w)/(double)h;
    cur_wind->ratio = ratio;
    glViewport(0,0,w,h);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();

    cur_wind->lft = -1 * tvvars::size;
    cur_wind->rght = tvvars::size;
    cur_wind->lft *= ratio;
    cur_wind->rght *= ratio;
    glOrtho(cur_wind->lft, cur_wind->rght,cur_wind->bot,cur_wind->top,cur_wind->ner*2,cur_wind->fr*2);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    glutSetWindow(cur_wind->subwin);
    if(h<w)
    {
        glutPositionWindow(w - h/4,0);
        glutReshapeWindow(h / 4 ,h / 4);
    }else
    {
        glutPositionWindow(w - w/4,0);
        glutReshapeWindow(w / 4 ,w / 4);
    }
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();

    glOrtho(cur_wind->lft, cur_wind->rght,cur_wind->bot,cur_wind->top,cur_wind->ner*2,cur_wind->fr*2);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
}
