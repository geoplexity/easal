/*
 This file is part of EASAL. 

 EASAL is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 EASAL is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "SelectionWindow.h"

#include "readIn.h"
// #include "MolecularUnit.h"
// #include "DistanceSelectionWindow.h"

#include <cmath>
#include <cstdlib>
#include <string>
// #include <sstream>
// #include <iostream>
// #include <utility>

using namespace std;





void SelectionWindow::init_table(FXVerticalFrame *vertical_frame, string filename) {
	this->file_used = filename;

	// get the data from the file table
	vector<vector<string> > file_data = readData(filename);

	size_t max_row_length = file_data.front().size();;
	for (size_t x = 0; x < file_data.size(); x++) {
		max_row_length = max(max_row_length, file_data[x].size());
	}

	// scroll window
	FXScrollWindow *scroll_area = new FXScrollWindow(vertical_frame,FRAME_GROOVE|LAYOUT_FIX_HEIGHT,0,0,0,200);
	scroll_area->setScrollStyle(HSCROLLING_OFF);
	scroll_area->setBackColor(this->getBackColor());

	// initialize the table
	// +1 for the ignore checkboxes
	FXMatrix *table = new FXMatrix(scroll_area, max_row_length+1, MATRIX_BY_COLUMNS);
	new FXLabel(table, "ignore", NULL, LAYOUT_CENTER_X);

	// make the string for the dropdown boxes
	// items are separated by newlines
	string label_str = " ";
	size_t max_label_length = 1;
	for (size_t i = 0; i < _column_labels.size(); i++) {
		label_str.append("\n"+_column_labels[i].label);
		max_label_length = max(max_label_length, _column_labels[i].label.length());
	}

	// make the dropdowns
	for (size_t i = 0; i < max_row_length; i++){
		label_dropdowns.push_back(new FXComboBox(table, max_label_length, this,
			SelectionWindow::ID_APPROVE, LAYOUT_CENTER_X));
		label_dropdowns[i]->fillItems(label_str.c_str());
		label_dropdowns[i]->setNumVisible(_column_labels.size()+1);
		label_dropdowns[i]->setEditable(false);
	}

	// if(filename.find(".dat") != string::npos){
	// 	if(values[0][0] == "ATOM"){   // PDB style
	// 		label_dropdowns[3]->setCurrentItem(1);
	// 		for(size_t x = 0;x<3;x++){
	// 			label_dropdowns[x+6]->setCurrentItem(x+2);
	// 		}

	// 	}else{
	// 		for(size_t x = 2;x<6;x++){ //standard data
	// 			label_dropdowns[x]->setCurrentItem(x);
	// 		}
	// 	}
	// }



	int max_entries_in_row = 500;	//FXMatrix can't hold more than 512
	for (size_t i = 0; i < min(int(file_data.size()), max_entries_in_row); i++){
		ignore_checks.push_back(new FXCheckButton(table, "", this,
			SelectionWindow::ID_CHECK,ICON_BEFORE_TEXT|LAYOUT_FILL_X,
			0,0,0,0,10,10,5,5));

		vector<string> row = file_data[i];
		size_t l = row.size();
		if (l > max_row_length) {
			l = max_row_length;
			cout << "Row " << i << " is too long to display." << endl;
		}

		for (size_t j = 0; j < l; j++){
			// new FXLabel(table, row[j].c_str(), NULL, FRAME_GROOVE|LAYOUT_FILL_X);

			FXTextField *tf  = new FXTextField(table,10,NULL,ID_LAST,LAYOUT_NORMAL);
			tf->setText(row[j].c_str());
			tf->setEditable(false);
		}

	}

	// scroll_area->resize(scroll_area->getContentWidth(),scroll_area->getContentWidth() / 2);
	// scroll_area->setHeight(scroll_area->getHeight() + 200 );
}

void SelectionWindow::init_bottom_buttons(FXVerticalFrame *vertical_frame) {
	FXHorizontalFrame *horzf = new FXHorizontalFrame(vertical_frame,FRAME_GROOVE| LAYOUT_BOTTOM |LAYOUT_FILL_X);
	accept_button_p = new FXButton(horzf,"Accept",NULL,this,SelectionWindow::ID_ACCEPT,
			BUTTON_NORMAL|LAYOUT_BOTTOM|LAYOUT_FILL_X,
			0,0,0,0,10,10,5,5);
	this->accept_button_p->disable();
	new FXButton(horzf,"Cancel",NULL,this,FXDialogBox::ID_CANCEL,
			BUTTON_NORMAL|LAYOUT_BOTTOM|LAYOUT_FILL_X,
			0,0,0,0,10,10,5,5);
	new FXButton(horzf,"Clear all selections.",NULL,this,SelectionWindow::ID_CLEAR,
			BUTTON_NORMAL|LAYOUT_BOTTOM|LAYOUT_FILL_X,
			0,0,0,0,10,10,5,5);
}

FXuint SelectionWindow::execute() {
	//column labels
	for (int i = 0; i < label_dropdowns.size(); i++) {
		label_dropdowns[i]->setCurrentItem(0); //blank
	}

	for (int i = 0; i < _column_labels.size(); i++) {
		if (_column_labels[i].position >= 0
			&& _column_labels[i].position < label_dropdowns.size())
		{
			label_dropdowns[_column_labels[i].position]->setCurrentItem(i+1);
		}
	}


	// ignore checks
	for (int i = 0; i < ignore_checks.size(); i++) {
		ignore_checks[i]->setCheck(FALSE, FALSE);
	}

	for (int i = 0; i < _ignored_rows.size(); i++) {
		if (_ignored_rows[i] >= 0
			&& _ignored_rows[i] < ignore_checks.size())
		{
			ignore_checks[_ignored_rows[i]]->setCheck(TRUE, FALSE);
		}
	}



	set_default_values();
	return FX::FXDialogBox::execute();
}

long SelectionWindow::button_accept(FXObject *sender, FXSelector, void *d){
	this->handle(this,FXSEL(SEL_UPDATE,SelectionWindow::ID_APPROVE), d);
	this->handle(this,FXSEL(SEL_COMMAND,FXDialogBox::ID_ACCEPT), d);

	// save all the column label changes
	for (int i = 0; i < _column_labels.size(); i++) {
		_column_labels[i].position = -1;
	}

	for (int i = 0; i < label_dropdowns.size(); i++) {
		int label_picked = label_dropdowns[i]->getCurrentItem()-1;

		// can't be more than labels by constuction
		if (label_picked != -1) {
			_column_labels[label_picked].position = i;
		}
	}



	// save all the _ignored_rows changes
	_ignored_rows.clear();
	for (int i = 0; i < ignore_checks.size(); i++) {
		if (ignore_checks[i]->getCheck()) _ignored_rows.push_back(i);
	}



	save_changes();
	return 1;
}




long SelectionWindow::button_clearTable(FXObject *sender, FXSelector, void *) {
	for (int i = 0; i < label_dropdowns.size(); i++) {
		label_dropdowns[i]->setCurrentItem(0);
	}

	for (int i = 0; i < ignore_checks.size(); i++) {
		ignore_checks[i]->setCheck(FALSE, FALSE);
	}

	return 1;
}


long SelectionWindow::approve_selections(FXObject *sender, FXSelector, void *){
	if (good_input())
		this->accept_button_p->enable();
	else
		this->accept_button_p->disable();

	return 1;
}

bool SelectionWindow::same_file(string filename) {
	return filename.compare(this->file_used) == 0;
}

void SelectionWindow::set_column_of(unsigned int label, int *column) {
	_column_labels[label].position = *column;
	_column_labels[label].position_save_pointer = column;
}


void SelectionWindow::ignored_rows(std::vector<int> *rows) {
	_ignored_rows_save_pointer = rows;

	for (std::vector<int>::iterator it = rows->begin();
		it != rows->end();
		it++)
	{
		_ignored_rows.push_back(*it);
	}

}


void SelectionWindow::save_input() {
	// save column labels
	for (std::vector<column_label>::iterator it = _column_labels.begin();
		it != _column_labels.end();
		it++)
	{
		*(it->position_save_pointer) = it->position;
	}

	// save ignored rows
	_ignored_rows_save_pointer->clear();
	for (std::vector<int>::iterator it = _ignored_rows.begin();
		it != _ignored_rows.end();
		it++)
	{
		_ignored_rows_save_pointer->push_back(*it);
	}
}





////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////



FXDEFMAP(DataSelectionWindow) SelectWinMap_data[]={
		FXMAPFUNC(SEL_COMMAND,   DataSelectionWindow::ID_ACCEPT,  DataSelectionWindow::button_accept),
		FXMAPFUNC(SEL_COMMAND,   DataSelectionWindow::ID_CLEAR,  DataSelectionWindow::button_clearTable),
		FXMAPFUNC(SEL_UPDATE,   DataSelectionWindow::ID_APPROVE,  DataSelectionWindow::approve_selections)};

FXIMPLEMENT(DataSelectionWindow,FXDialogBox,SelectWinMap_data,ARRAYNUMBER(SelectWinMap_data));


DataSelectionWindow::DataSelectionWindow(FXWindow *owner, const FXString &name,
	std::string filename)
	:SelectionWindow(owner,name)
{
	this->_column_labels.push_back(column_label("x"));
	this->_column_labels.push_back(column_label("y"));
	this->_column_labels.push_back(column_label("z"));
	this->_column_labels.push_back(column_label("Radius"));
	this->_column_labels.push_back(column_label("Label"));
	this->_column_labels.push_back(column_label("Atom No"));




	// vertical frame
	FXVerticalFrame *verf = new FXVerticalFrame(this,LAYOUT_FILL_Y);

	// Explanation
	FXHorizontalFrame *horf = new FXHorizontalFrame(verf,LAYOUT_FILL_X);
	new FXLabel(horf,"Choose column labels and check rows to ignore.", NULL, LAYOUT_CENTER_X);

	init_table(verf, filename);
	init_bottom_buttons(verf);
}

// long DataSelectionWindow::approve_selections(FXObject *sender, FXSelector, void *) {
bool DataSelectionWindow::good_input() {
	size_t labels = _column_labels.size();

	bool *selected;
	selected = (bool*) calloc(labels, sizeof(bool)); // starts them all false

	bool at_most_once = true;

	// sets appropriate slots in selected to true;
	// checks that everything is used at most once
	for (int i = 0; i < label_dropdowns.size(); i++) {
		int label_picked = label_dropdowns[i]->getCurrentItem()-1;

		// can't be more than labels by constuction
		if (label_picked >= 0) {
			if (selected[label_picked]) {
				at_most_once = false;
				break;
			} else {
				selected[label_picked] = true;
			}
		}
	}


	bool ret = false;

	// check specifics
	if (at_most_once
		&& selected[x_col]
		&& selected[y_col]
		&& selected[z_col]
		&& (selected[label_col] || selected[radius_col]))
	{
		ret = true;
	}


	free(selected);
	return ret;
}







////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////




FXDEFMAP(DistanceSelectionWindow) SelectWinMap_dist[]={
		FXMAPFUNC(SEL_COMMAND,   DistanceSelectionWindow::ID_ACCEPT,  DistanceSelectionWindow::button_accept),
		FXMAPFUNC(SEL_COMMAND,   DistanceSelectionWindow::ID_CLEAR,  DistanceSelectionWindow::button_clearTable),
		FXMAPFUNC(SEL_UPDATE,   DistanceSelectionWindow::ID_APPROVE,  DistanceSelectionWindow::approve_selections)};

FXIMPLEMENT(DistanceSelectionWindow,FXDialogBox,SelectWinMap_dist,ARRAYNUMBER(SelectWinMap_dist));


DistanceSelectionWindow::DistanceSelectionWindow(FXWindow *owner, const FXString &name,
	std::string filename)
	:SelectionWindow(owner,name)
{
	this->_column_labels.push_back(column_label("Label #1"));
	this->_column_labels.push_back(column_label("Label #2"));
	this->_column_labels.push_back(column_label("Radius"));
	this->_column_labels.push_back(column_label("Min Radius"));
	this->_column_labels.push_back(column_label("Max Radius"));




	// vertical frame
	FXVerticalFrame *verf = new FXVerticalFrame(this,LAYOUT_FILL_Y);

	// Explanation
	FXHorizontalFrame *horf = new FXHorizontalFrame(verf,LAYOUT_FILL_X);
	new FXLabel(horf,"Choose column labels and check rows to ignore.", NULL, LAYOUT_CENTER_X);

	// checkboxes
	FXHorizontalFrame *horf2 = new FXHorizontalFrame(verf,LAYOUT_CENTER_X);

/*	this->cb_matrix = new FXCheckButton(horf2, "Matrix",
		this, DistanceSelectionWindow::ID_CHECK, ICON_BEFORE_TEXT,
		0, 0, 0, 0, 10, 10, 5, 5);

	this->cb_radius = new FXCheckButton(horf2, "Global radius",
		this, DistanceSelectionWindow::ID_CHECK, ICON_BEFORE_TEXT,
		0, 0, 0, 0, 10, 10, 5, 5);

	// textfields
	FXHorizontalFrame *horf3 = new FXHorizontalFrame(verf,LAYOUT_CENTER_X);

	new FXLabel(horf3, "Global radius: ", NULL, LAYOUT_FILL_X);

	new FXLabel(horf3, "Low =", NULL, LAYOUT_FILL_X);
	this->tf_rangeLow = new FXTextField(horf3, 10, NULL, 0,
		LAYOUT_CENTER_X|LAYOUT_CENTER_Y);

	new FXLabel(horf3, "High =", NULL, LAYOUT_FILL_X);
	this->tf_rangeHigh = new FXTextField(horf3, 10, NULL, 0,
		LAYOUT_CENTER_X|LAYOUT_CENTER_Y);
*/
	init_table(verf, filename);
	init_bottom_buttons(verf);
}

void DistanceSelectionWindow::set_default_values() {
	// for converting numbers to strings to fill textboxes
/*	std::stringstream ss;

	this->cb_matrix->setCheck(matrixCheck, FALSE);
	this->cb_radius->setCheck(radiusCheck, FALSE);

	ss.str("");
	ss << this->rangeLow;
	this->tf_rangeLow->setText(ss.str().c_str());

	ss.str("");
	ss << this->rangeHigh;
	this->tf_rangeHigh->setText(ss.str().c_str());
	*/
}

void DistanceSelectionWindow::save_changes() {
	//TODO: I need to do the matrix check thingy here.
	/*matrixCheck = cb_matrix->getCheck();
	radiusCheck = cb_radius->getCheck();
	rangeLow = atof(tf_rangeLow->getText().text());
	rangeHigh = atof(tf_rangeHigh->getText().text());*/
}

// long DistanceSelectionWindow::approve_selections(FXObject *sender, FXSelector, void *) {
bool DistanceSelectionWindow::good_input() {
//	if (cb_matrix->getCheck()) return true;

	size_t labels = _column_labels.size();

	bool *selected;
	selected = (bool*) calloc(labels, sizeof(bool)); // starts them all false

	bool at_most_once = true;

	// sets appropriate slots in selected to true;
	// checks that everything is used at most once
	for (int i = 0; i < label_dropdowns.size(); i++) {
		int label_picked = label_dropdowns[i]->getCurrentItem()-1;

		// can't be more than labels by constuction
		if (label_picked >= 0) {
			if (selected[label_picked]) {
				at_most_once = false;
				break;
			} else {
				selected[label_picked] = true;
			}
		}
	}


	bool ret = false;

	// check specifics
	if (at_most_once
		&& selected[label1_col]
		&& selected[label2_col]
		&& (selected[radius_col] || (selected[radiusMin_col] && selected[radiusMax_col])))
	{
		ret = true;
	}


	free(selected);
	return ret;
}



void DistanceSelectionWindow::setSettings(bool matrixCheck, bool radiusCheck,
	double rangeLow, double rangeHigh)
{
	/*this->matrixCheck = matrixCheck;
	this->radiusCheck = radiusCheck;
	this->rangeLow = rangeLow;
	this->rangeHigh = rangeHigh;*/
}
/*
bool DistanceSelectionWindow::matrix() {return matrixCheck;}
bool DistanceSelectionWindow::radius() {return radiusCheck;}
double DistanceSelectionWindow::radiusMin() {return rangeLow;}
double DistanceSelectionWindow::radiusMax() {return rangeHigh;}
*/


FXDEFMAP(AdvancedOptionsWindow) SelectWinMap_advOpts[]={
		FXMAPFUNC(SEL_COMMAND,   AdvancedOptionsWindow::ID_ACCEPT,  AdvancedOptionsWindow::button_accept),
		FXMAPFUNC(SEL_COMMAND,   AdvancedOptionsWindow::ID_CLEAR,  AdvancedOptionsWindow::button_clearTable)};
//		FXMAPFUNC(SEL_UPDATE,   AdvancedOptionsWindow::ID_APPROVE, AdvancedOptionsWindow::approve_selections)};


FXIMPLEMENT(AdvancedOptionsWindow, FXDialogBox,SelectWinMap_advOpts, ARRAYNUMBER(SelectWinMap_advOpts));

AdvancedOptionsWindow::AdvancedOptionsWindow(FXWindow *owner, const FXString &name,int max_atoms, double max_len, double max_zlen):SelectionWindow(owner,name)
{
	this->max_atoms = max_atoms;
	this->max_len = max_len;
	this->max_zlen = max_zlen;
	FXVerticalFrame *tab1  = new FXVerticalFrame(this, LAYOUT_FILL_Y);
	FXMatrix *matrix;
	FXHorizontalFrame *row;


	matrix = new FXMatrix(tab1, 3, MATRIX_BY_COLUMNS|FRAME_GROOVE);

	this->cb_reverseWitness = new FXCheckButton(matrix, "Reverse Witness",
		this, DistanceSelectionWindow::ID_CHECK, ICON_BEFORE_TEXT,
		0, 0, 0, 0, 10, 10, 5, 5);
	this->cb_wholeCollisions = new FXCheckButton(matrix, "Whole Collisions",
		this, DistanceSelectionWindow::ID_CHECK, ICON_BEFORE_TEXT,
		0, 0, 0, 0, 10, 10, 5, 5);
	this->cb_fourDRootNode = new FXCheckButton(matrix, "4D Root Node",
		this, DistanceSelectionWindow::ID_CHECK, ICON_BEFORE_TEXT,
		0, 0, 0, 0, 10, 10, 5, 5);
	this->cb_dynamicStepSizeAmong = new FXCheckButton(matrix, "Use Dynamic Step Size",
		this, DistanceSelectionWindow::ID_CHECK, ICON_BEFORE_TEXT,
		0, 0, 0, 0, 10, 10, 5, 5);
	this->cb_reversePairDumbbells = new FXCheckButton(matrix, "Reverse Pair Dumbbells",
		this, DistanceSelectionWindow::ID_CHECK, ICON_BEFORE_TEXT,
		0, 0, 0, 0, 10, 10, 5, 5);
	this->cb_useParticipatingAtomZDistance = new FXCheckButton(matrix, "Use Participating Atoms Z Distance", this, DistanceSelectionWindow::ID_CHECK, ICON_BEFORE_TEXT,
		0, 0, 0, 0, 10, 10, 5, 5);

	new FXLabel(matrix,"Participating Atoms Z Distance", NULL, LAYOUT_CENTER_X);
	this->tf_ParticipatingAtomZDistance = new FXTextField(matrix, 15, NULL, 0, LAYOUT_CENTER_X|LAYOUT_CENTER_Y);

	this->cb_shortRangeSampling = new FXCheckButton(matrix, "Short Range Sampling",
		this, DistanceSelectionWindow::ID_CHECK, ICON_BEFORE_TEXT,
		0, 0, 0, 0, 10, 10, 5, 5);
    matrix = new FXMatrix(tab1, 4, MATRIX_BY_COLUMNS|FRAME_GROOVE);
	//row = new FXHorizontalFrame(tab1, LAYOUT_FILL_X);

	new FXLabel(matrix,"Participating Atom Index Low", NULL, LAYOUT_CENTER_X);
	this->tf_participatingAtomIndex_low = new FXTextField(matrix, 15, NULL, 0, LAYOUT_CENTER_X|LAYOUT_CENTER_Y);

	new FXLabel(matrix,"Participating Atom Index High", NULL, LAYOUT_CENTER_X);
	this->tf_participatingAtomIndex_high = new FXTextField(matrix, 15, NULL, 0, LAYOUT_CENTER_X|LAYOUT_CENTER_Y);
//New stuff I put in
	new FXLabel(matrix,"Initial 4D Contact Seperation Low", NULL, LAYOUT_CENTER_X);
	this->tf_initial4DContactSeparation_low = new FXTextField(matrix, 15, NULL, 0, LAYOUT_CENTER_X|LAYOUT_CENTER_Y);

	new FXLabel(matrix,"Initial 4D Contact Seperation High", NULL, LAYOUT_CENTER_X);
	this->tf_initial4DContactSeparation_high = new FXTextField(matrix, 15, NULL, 0, LAYOUT_CENTER_X|LAYOUT_CENTER_Y);
//New stuff I put in
	new FXLabel(matrix,"Save Points Frequency", NULL, LAYOUT_CENTER_X);
	this->tf_frequency = new FXTextField(matrix, 15, NULL, 0, LAYOUT_CENTER_X|LAYOUT_CENTER_Y);
	
	row = new FXHorizontalFrame(tab1, LAYOUT_FILL_X);

	accept_button_p = new FXButton(row,"Accept",NULL,this,SelectionWindow::ID_ACCEPT,
			BUTTON_NORMAL|LAYOUT_FILL_X,
			0,0,0,20,10,10,5,5);
	new FXButton(row,"Cancel",NULL,this,FXDialogBox::ID_CANCEL,
			BUTTON_NORMAL|LAYOUT_FILL_X,
			0,0,0,20,10,10,5,5);

}

void AdvancedOptionsWindow::set_default_values(){

	if(Settings::General::reverseWitness == true) {
		this->cb_reverseWitness->setCheck(true);
	}

	if(Settings::Constraint::wholeCollision == true) {
		this->cb_wholeCollisions->setCheck(true);
	}

	if(Settings::RootNodeCreation::dimension_of_rootNodes == 4) {
		this->cb_fourDRootNode->setCheck(true);
	}

	if(Settings::Sampling::dynamicStepSizeAmong == true) {
		this->cb_dynamicStepSizeAmong->setCheck(true);
	}

	//TODO:Dynamic step size within

	if(Settings::RootNodeCreation::reversePairDumbbells == true) {
		this->cb_reversePairDumbbells->setCheck(true);
	}

	if(Settings::RootNodeCreation::useParticipatingAtomZDistance == true) {
		this->cb_useParticipatingAtomZDistance->setCheck(true);
	}

	if(Settings::RootNodeCreation::ParticipatingAtomZDistance == 0) {
		this->tf_ParticipatingAtomZDistance->setText(to_string(max_zlen).c_str());
	} else {
		this->tf_ParticipatingAtomZDistance->setText(to_string(Settings::RootNodeCreation::ParticipatingAtomZDistance).c_str());
	}

	this->tf_participatingAtomIndex_low->setText("0");
	this->tf_participatingAtomIndex_high->setText(to_string(max_atoms).c_str());
	this->tf_initial4DContactSeparation_low->setText("0");
	this->tf_initial4DContactSeparation_high->setText(to_string(max_len).c_str());
	this->tf_frequency->setText("1000");
}


void AdvancedOptionsWindow::save_changes() {

	reverseWitness = cb_reverseWitness->getCheck();
	wholeCollisions = cb_wholeCollisions->getCheck();
	fourDRootNode = cb_fourDRootNode->getCheck();
	dynamicStepSizeAmong = cb_dynamicStepSizeAmong->getCheck();
	
	reversePairDumbbells = cb_reversePairDumbbells->getCheck();
	useParticipatingAtomZDistance = cb_useParticipatingAtomZDistance->getCheck();
	short_range_sampling = cb_shortRangeSampling->getCheck();

	ParticipatingAtomZDistance = atoi(tf_ParticipatingAtomZDistance->getText().text());

	participatingAtomIndex_low = atoi(tf_participatingAtomIndex_low->getText().text());
	participatingAtomIndex_high = atoi(tf_participatingAtomIndex_high->getText().text());
	initial4DContactSeparation_low = atof(tf_initial4DContactSeparation_low->getText().text());
	initial4DContactSeparation_high = atof(tf_initial4DContactSeparation_high->getText().text());
	frequency = atoi(tf_frequency->getText().text());
}



bool AdvancedOptionsWindow::good_input() {
	if(atof(tf_ParticipatingAtomZDistance->getText().text()) > max_len || atof(tf_ParticipatingAtomZDistance->getText().text()) < 0) {
		return false;
	}

	if(atof(tf_initial4DContactSeparation_low->getText().text()) > max_len || atof(tf_initial4DContactSeparation_low->getText().text()) < 0 ) {
		return false;
	}

	if((atof(tf_initial4DContactSeparation_high->getText().text()) > max_len) || (atof(tf_initial4DContactSeparation_high->getText().text()) < 0) ) {
		return false;
	}

	if(atof(tf_initial4DContactSeparation_low->getText().text()) > max_len || atof(tf_initial4DContactSeparation_low->getText().text()) < 0 ) {
		return false;
	}


	if(atoi(tf_participatingAtomIndex_high->getText().text()) > max_atoms || atoi(tf_participatingAtomIndex_high->getText().text()) < 0) {
		return false;
	}

	if(atoi(tf_participatingAtomIndex_low->getText().text()) > max_atoms || atoi(tf_participatingAtomIndex_low->getText().text()) < 0) {
		return false;
	}
	//Set Participating Atom index low = 0 high = numatoms
	//Set Initial 4D contact low = 0 high = len of helix
	//Participating Atom z-dist len of helix
	return true;
}

bool AdvancedOptionsWindow::validate_input(string &errors) {
	if(atof(tf_ParticipatingAtomZDistance->getText().text()) > max_len || atof(tf_ParticipatingAtomZDistance->getText().text()) < 0) {
		errors += "Invalid value for ParticipatingAtomZDistance.\n";
		return false;
	}

	if(atof(tf_initial4DContactSeparation_low->getText().text()) > max_len || atof(tf_initial4DContactSeparation_low->getText().text()) < 0 ) {
		errors += "Invalid value for Initial 4D Contact Separation low.\n";
		return false;
	}

/*	if((atof(tf_initial4DContactSeparation_high->getText().text()) > max_len) || (atof(tf_initial4DContactSeparation_high->getText().text()) < 0) ) {
		errors += "Invalid value for Initial 4D Contact Separation high.\n";
		return false;
	}*/

	if(atof(tf_initial4DContactSeparation_low->getText().text()) > max_len || atof(tf_initial4DContactSeparation_low->getText().text()) < 0 ) {
		errors += "Invalid value for Initial 4D Contact Separation low.\n";
		return false;
	}


	if(atoi(tf_participatingAtomIndex_high->getText().text()) > max_atoms || atoi(tf_participatingAtomIndex_high->getText().text()) < 0) {
		errors += "Invalid value for Participating Atom Index high.\n";
		return false;
	}

	if(atoi(tf_participatingAtomIndex_low->getText().text()) > max_atoms || atoi(tf_participatingAtomIndex_low->getText().text()) < 0) {
		errors += "Invalid value for Participating Atom Index low.\n";
		return false;
	}
	//Set Participating Atom index low = 0 high = numatoms
	//Set Initial 4D contact low = 0 high = len of helix
	//Participating Atom z-dist len of helix
	return true;
}



void AdvancedOptionsWindow::save_settings() {
	Settings::General::reverseWitness  = reverseWitness;
	Settings::RootNodeCreation::reversePairDumbbells = reversePairDumbbells;
	Settings::Sampling::dynamicStepSizeAmong = dynamicStepSizeAmong;
	
	Settings::Constraint::wholeCollision = wholeCollisions;
	if(fourDRootNode == true) {
		Settings::RootNodeCreation::dimension_of_rootNodes = 4;
	} else {
		Settings::RootNodeCreation::dimension_of_rootNodes = 5;
	}
	Settings::RootNodeCreation::useParticipatingAtomZDistance = useParticipatingAtomZDistance;

    Settings::RootNodeCreation::ParticipatingAtomZDistance = ParticipatingAtomZDistance;
	Settings::Sampling::short_range_sampling = short_range_sampling;

	Settings::RootNodeCreation::participatingAtomIndex_low = participatingAtomIndex_low;
    Settings::RootNodeCreation::participatingAtomIndex_high = participatingAtomIndex_high;
	Settings::RootNodeCreation::initial4DContactSeparation_low = initial4DContactSeparation_low;
	Settings::RootNodeCreation::initial4DContactSeparation_high = initial4DContactSeparation_high;
	Settings::Saving::savePointsFrequency = frequency;
}

long AdvancedOptionsWindow::button_accept(FXObject *sender, FXSelector, void *d){
	this->handle(this,FXSEL(SEL_UPDATE,SelectionWindow::ID_APPROVE), d);
	this->handle(this,FXSEL(SEL_COMMAND,FXDialogBox::ID_ACCEPT), d);

	string errors;
	if(!validate_input(errors)) {
		FXMessageBox::warning(this,MBOX_OK,
				"Warning",
				errors.c_str()
				);
		return 0;
	}

	save_changes();
	return 1;
}
/*
long AdvancedOptionsWindow::clicked(FXObject *sender, FXSelector, void *d) {
	if(cb_fourDRootNode->getCheck() == true) {
		tf_initial4DContactSeparation_low->enable();
		tf_initial4DContactSeparation_high->enable();
	} else {
		tf_initial4DContactSeparation_low->disable();
		tf_initial4DContactSeparation_high->disable();
	}
	return 1;

}*/
