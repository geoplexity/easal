/*
 This file is part of EASAL. 

 EASAL is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 EASAL is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * AtlasNodeSelectionWindow.cpp
 *
 *  Created on: Oct 12, 2009
 *      Author: jrpence
 */

#include "AtlasNodeSelectionWindow.h"


#include <sstream>
#include <iostream>


using namespace std;

static  float mat_white[4]={1.0, 1.0, 1.0, 1.0};
static  float mat_green[4]={0.0, 1.0, 0.0, 1.0};
static  float mat_blue[4]={0.0, 0.0, 1.0, 1.0};
static	float mat_specular[]={.4, .4, .4, 1.0};
static	float mat_ambient[]={.2, .2, .2, 1.0};
static	float mat_shininess={.5};
static	float light_ambient[]={0.2, 0.2, 0.2, 1.0};
static	float light_diffuse[]={0.6, 0.6, 0.6, 1.0};
static	float light_specular[]={.2, .2, .2, 1.0};
static	float light_position[]={-3.0, 5.0,10.0, 0.0};



// Map FOX messages to our callback functions
FXDEFMAP(AtlasNodeSelectionWindow) MyWindowMap[]= {
  FXMAPFUNC(SEL_COMMAND,   			AtlasNodeSelectionWindow::ID_CANCEL,  AtlasNodeSelectionWindow::cancel),
  FXMAPFUNC(SEL_COMMAND,   			AtlasNodeSelectionWindow::ID_ACCEPT,  AtlasNodeSelectionWindow::accept),
  FXMAPFUNC(SEL_COMMAND,   			AtlasNodeSelectionWindow::ID_LOAD,  AtlasNodeSelectionWindow::loadGraph),
  FXMAPFUNC(SEL_PAINT,   			AtlasNodeSelectionWindow::ID_CANVAS,	AtlasNodeSelectionWindow::onPaint),
  FXMAPFUNC(SEL_LEFTBUTTONPRESS,	AtlasNodeSelectionWindow::ID_CANVAS,	AtlasNodeSelectionWindow::mouse),
  FXMAPFUNC(SEL_TIMEOUT, 			AtlasNodeSelectionWindow::ID_ANIMATION, AtlasNodeSelectionWindow::onPaint)
};
FXIMPLEMENT(AtlasNodeSelectionWindow,FXMainWindow,MyWindowMap,ARRAYNUMBER(MyWindowMap));


//
//
//    MyWindow::MyWindow()
//
//    Constructor for MyWindow
//
//    Create FOX OpenGL objects and start the first timer event after 30 milliseconds.
//
AtlasNodeSelectionWindow::AtlasNodeSelectionWindow(FXApp * a, Atlas * mapview):FXMainWindow(a,"Constraint Selection Dialogue Box",NULL,NULL,DECOR_BORDER|DECOR_TITLE|DECOR_RESIZE,0,0,800,500)
{
	this->optCont = 5;

	this->mapView = mapview;
	this->app = a;
	this->cgK = NULL;
	this->hela = NULL;
	this->helb = NULL;
	this->scaleA = 1;
	this->scaleB = 1;
  FXHorizontalFrame *hframe1 = new FXHorizontalFrame(this,FRAME_GROOVE | LAYOUT_FILL_X | LAYOUT_FILL_Y);
  FXVerticalFrame *vframe1 = new FXVerticalFrame(hframe1,FRAME_GROOVE |LAYOUT_FILL_X|LAYOUT_FILL_Y);
  FXHorizontalFrame *hframe2 = new FXHorizontalFrame(vframe1,LAYOUT_FILL_X | LAYOUT_FILL_Y);
  glvisual0 = new FXGLVisual(getApp(), VISUAL_DOUBLEBUFFER);    // Create a new visual

  dial1y = new FXDial(hframe2,NULL,0,DIAL_HAS_NOTCH |DIAL_VERTICAL | LAYOUT_FILL_Y,0,0,100,0);
  dial1y->setRange(-90,90,FALSE);
  dial1y->setValue(0,false);
  glcanvas0 = new FXGLCanvas(hframe2, glvisual0, this, ID_CANVAS,LAYOUT_NORMAL| LAYOUT_FILL_X | LAYOUT_FILL_Y,0,0,0,0);

  dial2y = new FXDial(hframe2,NULL,0,DIAL_HAS_NOTCH |DIAL_VERTICAL | LAYOUT_FILL_Y,0,0,100,0);
  dial2y->setRange(-90,90,FALSE);
  dial2y->setValue(0,false);

  getApp()->addTimeout(hframe2, ID_ANIMATION, 30, 0);    // Set a timer for 30 milliseconds
  FXHorizontalFrame *hframe3 = new FXHorizontalFrame(vframe1,LAYOUT_FILL_X);
  dial1x=new FXDial(hframe3,NULL,0,DIAL_CYCLIC|DIAL_HAS_NOTCH |DIAL_HORIZONTAL | LAYOUT_FILL_X,0,0,100,0);
  dial2x=new FXDial(hframe3,NULL,0,DIAL_CYCLIC|DIAL_HAS_NOTCH |DIAL_HORIZONTAL | LAYOUT_FILL_X,0,0,100,0);

  FXVerticalFrame *vframe2 = new FXVerticalFrame(hframe1,FRAME_GROOVE |LAYOUT_FILL_Y);
  new FXLabel(vframe2,"Zoom view in and out\nby clicking in corners\nof 3D window",NULL,LAYOUT_CENTER_Y);
  FXHorizontalFrame *hframe6 = new FXHorizontalFrame(vframe2,LAYOUT_CENTER_Y);
  {
	  stringstream ss;
	  ss << "Preload node number\n(0 - " << (this->mapView->number_of_nodes() -1) << ")";
	  new FXLabel(hframe6,ss.str().c_str());
  }
//  new FXLabel(hframe6,"Preload node number\n(0 - ");
  nodeNum = new FXTextField(hframe6,5,NULL,NULL,LAYOUT_CENTER_Y);
  nodeNum->setText("0");
  loadButton = new FXButton(hframe6,"Load",NULL,this,AtlasNodeSelectionWindow::ID_LOAD,
			BUTTON_NORMAL|LAYOUT_CENTER_Y|LAYOUT_CENTER_X,
			0,0,0,0,10,10,5,5);

  FXHorizontalFrame *hframe4 = new FXHorizontalFrame(vframe2,LAYOUT_CENTER_Y);
  pointA = new FXComboBox(hframe4,5,NULL,0,LAYOUT_CENTER_Y);
  pointA->setEditable(false);
  new FXLabel(hframe4,"--",NULL,LAYOUT_CENTER_Y);
  pointB = new FXComboBox(hframe4,5,NULL,0,LAYOUT_CENTER_Y);
  pointB->setEditable(false);


  FXHorizontalFrame *hfrm[optCont];
  for(int i = 0; i<this->optCont;i++){
	  hfrm[i] = new FXHorizontalFrame(vframe2,LAYOUT_CENTER_Y);
	  this->pointHA[i] = new FXComboBox(hfrm[i],5,NULL,0,LAYOUT_CENTER_Y);
	  this->pointHA[i]->setEditable(false);
	  new FXLabel(hfrm[i],"--",NULL,LAYOUT_CENTER_Y);
	  this->pointHB[i] = new FXComboBox(hfrm[i],5,NULL,0,LAYOUT_CENTER_Y);
	  this->pointHB[i]->setEditable(false);
	  this->conCheck[i] = new FXCheckButton(hfrm[i],"Connect",this,AtlasNodeSelectionWindow::ID_CHECK,ICON_BEFORE_TEXT|LAYOUT_FILL_X,0,0,0,0,10,10,5,5);
  }

  acceptButton = new FXButton(vframe2,"Accept",NULL,this,AtlasNodeSelectionWindow::ID_ACCEPT,
			BUTTON_NORMAL|LAYOUT_CENTER_Y|LAYOUT_CENTER_X,
			0,0,0,0,10,10,5,5);
  new FXButton(vframe2,"Cancel",NULL,this,AtlasNodeSelectionWindow::ID_CANCEL,
			BUTTON_NORMAL|LAYOUT_CENTER_Y|LAYOUT_CENTER_X,
			0,0,0,0,10,10,5,5);


  this->displayNumbersOnMolecularUnit = true;
  this->displayBonds = true;

}



AtlasNodeSelectionWindow::~AtlasNodeSelectionWindow()
{
	  delete glcanvas0;              // Used for OpenGL drawing
	  delete glvisual0;              // OpenGL Visual

	  delete dial1x;
	  delete dial1y;
	  delete dial2x;
	  delete dial2y;

	  delete pointA;
	  delete pointB;

	  delete acceptButton;

	  for(size_t x = 0;x<this->optCont;x++){
		  delete conCheck[x];
		  delete pointHA[x];
		  delete pointHB[x];
	  }
}


long AtlasNodeSelectionWindow::mouse(FXObject *obj , FXSelector sel, void *ptr){
	FXEvent *evnt = (FXEvent *) ptr;
	int width = this->glcanvas0->getWidth();
	int height = this->glcanvas0->getHeight();
	if(evnt->click_x < width * .3 && evnt->click_y < height * .3){
		this->scaleA *= 1.1;
	}else if(evnt->click_x > width * .7 && evnt->click_y < height * .3){
		this->scaleB *= 1.1;
	}else if(evnt->click_x < width * .3 && evnt->click_y > height * .7){
		this->scaleA /= 1.1;
	}else if(evnt->click_x > width * .7 && evnt->click_y > height * .7){
		this->scaleB /= 1.1;
	}

	return 1;
}

long AtlasNodeSelectionWindow::accept(FXObject *, FXSelector , void *)
{
	vector<pair<int,int> > parts;
	parts.push_back(make_pair(pointA->getCurrentItem(),pointB->getCurrentItem()) );

	for(size_t i = 0; i<optCont; i++){
		if(this->conCheck[i]->getCheck()){
			parts.push_back(make_pair(this->pointHA[i]->getCurrentItem(),
							this->pointHB[i]->getCurrentItem() ) );
		}
	}
	this->cgK = new ActiveConstraintGraph(parts,this->hela,this->helb);


	this->app->exit(1);

	return 1;
}

long AtlasNodeSelectionWindow::loadGraph(FXObject *, FXSelector , void *)
{
	stringstream ss;
	int number;
	ss << this->nodeNum->getText().text();
	ss >> number;
	if(number < 0 || number >= this->mapView->number_of_nodes() ){
		this->nodeNum->setTextColor(FXRGB(255,0,0));
		return 1;
	}

	this->nodeNum->setTextColor(FXRGB(0,0,0));

	ActiveConstraintGraph* cgk = (*this->mapView)[number]->getCG();

	vector<pair<int,int> > parts = cgk->getParticipants();
	pointA->setCurrentItem(parts[0].first);
	pointB->setCurrentItem(parts[0].second);

	for(size_t i = 1; i < parts.size(); i++){
		size_t j = i-1;
		this->conCheck[j]->setCheck(TRUE);
		this->pointHA[j]->setCurrentItem(parts[i].first);
		this->pointHB[j]->setCurrentItem(parts[i].second);
	}

	for(size_t i = parts.size(); i < 6; i++){
		int j = i-1;
		if( j < 0 ){
			cout << "there is no participants!!! " << endl;
			continue;
		}
		this->conCheck[j]->setCheck(FALSE);
		this->pointHA[j]->setCurrentItem(0);
		this->pointHB[j]->setCurrentItem(0);
	}

	return 1;
}

long AtlasNodeSelectionWindow::cancel(FXObject *, FXSelector , void *)
{
	this->cgK = NULL;
	this->hide();
	this->app->exit(1);
	return 1;
}

ActiveConstraintGraph* AtlasNodeSelectionWindow::getGraph(){
	return this->cgK;
}

void AtlasNodeSelectionWindow::setMolecularUnits(MolecularUnit *hela,MolecularUnit *helb){
	this->hela = hela;
	this->helb = helb;
	double hi[3],lo[3];
	hela->getLimits(hi,lo);
	scale = 1;
	for(int i = 0; i< 3; i++){
		mid1[i] = (hi[i] + lo[i]) / 2;
		if(hi[i] - lo[i] > scale){
			scale = hi[i] - lo[i];
		}
	}
	helb->getLimits(hi,lo);
	for(int i = 0; i< 3; i++){
		mid2[i] = (hi[i] + lo[i]) / 2;
		if(hi[i] - lo[i] > scale){
			scale = hi[i] - lo[i];
		}
	}
	scale = 1.9 / scale;
	  std::stringstream ssA,ssB;
	  for(size_t x = 0; x < hela->getAtoms().size();x++){
		  ssA << x << "\n";
	  }
	  for(size_t x = 0; x < helb->getAtoms().size();x++){
		  ssB << x << "\n";
	  }
	  pointA->fillItems(ssA.str().c_str());//("one\ntwo\nthree\none");
	  pointB->fillItems(ssB.str().c_str());

	  pointA->setNumVisible(5);
	  pointB->setNumVisible(5);

	  for(size_t i = 0; i<this->optCont; i++){
		  this->pointHA[i]->fillItems(ssA.str().c_str());
		  this->pointHB[i]->fillItems(ssB.str().c_str());
		  this->pointHA[i]->setCurrentItem(1);
		  this->pointHB[i]->setCurrentItem(1);
		  this->pointHA[i]->setNumVisible(5);
		  this->pointHB[i]->setNumVisible(5);
	  }
}

void AtlasNodeSelectionWindow::getMolecularUnits(MolecularUnit *hela,MolecularUnit *helb){
	hela = this->hela;
	hela = this->helb;
}

void AtlasNodeSelectionWindow::getDumbbellIndices(int &a1,int &b1,int &a2,int &b2){
	a1 = pointA->getCurrentItem();
	b1 = pointB->getCurrentItem();
	a2 = pointHA[0]->getCurrentItem();
	b2 = pointHB[0]->getCurrentItem();
	return;
}

void AtlasNodeSelectionWindow::setDumbbellIndices(int a1,int b1,int a2,int b2){
	pointA->setCurrentItem(a1);
	pointB->setCurrentItem(b1);
	this->conCheck[0]->setCheck(TRUE);
	this->pointHA[0]->setCurrentItem(a2);
	this->pointHB[0]->setCurrentItem(b2);
	return;
}

//    Callback function for MyWindow
//
//    Rotate the triangle by incrementing rtri and redraw the triangle. If the glCanvas is double buffered
//    then swap the buffers.
//
long AtlasNodeSelectionWindow::onPaint(FXObject *, FXSelector , void *)
{

  glcanvas0->makeCurrent();           // Make our canvas current



  // Clear the screen
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);       // Clear screen and depth buffer

  cHeight = glcanvas0->getHeight();
  cWidth = glcanvas0->getWidth();
  	  glViewport(0,0,cWidth,cHeight);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
		GLfloat lft, rght,bot,top,ner,fr;
	if(cHeight < cWidth){
		GLfloat ratio = cWidth / float(cHeight);
		//negatives left bot near
		ner = -10;
		bot = -2 ;
		lft = bot * ratio;
		top = fr = 2;
		rght = top * ratio;
	}else{
		GLfloat ratio = cHeight / float(cWidth);
		//negatives left bot near
		ner = -10;
		lft = -2 ;
		bot = lft * ratio;
		rght = fr = 2;
		top = rght * ratio;

	}
	glOrtho(lft,rght,bot,top,ner*2,fr*2);
	glMatrixMode(GL_MODELVIEW);

  glLoadIdentity();

  this->display();

  if(glvisual0->isDoubleBuffer())     // If our canvas is double buffered then swap the buffers
    glcanvas0->swapBuffers();         // A buffer swap

  glcanvas0->makeNonCurrent();        // Make our canvas noncurrent

  getApp()->addTimeout(this, ID_ANIMATION, 30, 0);            // Add a timer event to call this function back after 30 milliseconds
//  glcanvas0->update();
  return 1;
}

void AtlasNodeSelectionWindow::drawString(float x, float y, float z, string strn)
{
   glRasterPos3f(x, y, z);
   int row = 0;
   for (string::size_type c = 0; c < strn.length(); c++)
   {
	   if(strn[c] == '\n'){
		   row++;
		   glRasterPos3f(x, y - (row * .04), z);
	   }
      glutBitmapCharacter(GLUT_BITMAP_TIMES_ROMAN_10, strn[c]); // Updates the position
   }
}


void AtlasNodeSelectionWindow::drawMolecularUnit(MolecularUnit *hel,float mid[3],size_t index[6],float dmblPos[2][3], float helixScale){
	if(hel == NULL){
		for(int i = 0;i<2;i++)for(int j=0;j<3;j++){
			dmblPos[i][j] = 0.0;
		}
		return;
	}
	float mview[16];
	vector<Atom*> atoms = hel->getAtoms();

	glScalef(helixScale,helixScale,helixScale);
	glTranslatef(-mid[0],-mid[1],-mid[2]);
	for(size_t iter = 0; iter < atoms.size();iter++){

		float radius = atoms[iter]->getRadius();
		glPushMatrix();
			glTranslatef(atoms[iter]->getLocation()[0],atoms[iter]->getLocation()[1],atoms[iter]->getLocation()[2]);
			glutSolidSphere(radius,16,12);

			glPushMatrix();
				glGetFloatv(GL_MODELVIEW_MATRIX, mview);
				float pos[3];
				pos[0] = mview[12];
				pos[1] = mview[13];
				pos[2] = mview[14];
				for(size_t d = 0;d<6;d++){
					if(iter == index[d]){
						for(size_t i = 0;i<3;i++){
							dmblPos[d][i] = pos[i];
						}
					}
				}

				if(this->displayNumbersOnMolecularUnit)
				{
					glLoadIdentity();
					glColor3f(1,1,1);
					glTranslatef(pos[0],pos[1],pos[2] + (scale * helixScale * radius));
					glScalef(.0007,.0007, .0007);
					stringstream ss;
					ss << iter;
					glDisable(GL_LIGHTING);
						string strn = ss.str();
						glTranslatef(-40.0 * strn.length(),-50.0,0);
					   for (string::size_type c = 0; c < strn.length(); c++)
					   {
						  glutStrokeCharacter(GLUT_STROKE_ROMAN, strn[c]); // Updates the position
					   }
					glEnable(GL_LIGHTING);
				}

			glPopMatrix();

		glPopMatrix();

	}
}

void AtlasNodeSelectionWindow::display(){
	glInitNames();
	glLineWidth(2.5);
	GLfloat xrot,yrot;
	size_t index[6];//dumbbell index location
	float dmblPos[2][6][3];

	// Reset the current modelview matrix

  // Draw things!
	xrot = dial1x->getValue();
	yrot = dial1y->getValue();
	index[0] = this->pointA->getCurrentItem();
	for(size_t i = 0;i<this->optCont;i++){
		index[1+i] = this->pointHA[i]->getCurrentItem();
	}
	glPushMatrix();
		glTranslatef(-1.2,0,0);
		glRotatef(xrot,0.0f,1.0f,0.0f);
		glRotatef(yrot,0.0f,0.0f,-1.0f);
		glScalef(scale,scale,scale);

		glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, mat_green);
		this->drawMolecularUnit(hela,mid1,index,dmblPos[0],this->scaleA);
	glPopMatrix();

	xrot = dial2x->getValue();
	yrot = dial2y->getValue();

	index[0] = this->pointB->getCurrentItem();
	for(size_t i = 0;i<this->optCont;i++){
		index[1+i] = this->pointHB[i]->getCurrentItem();
	}

	glPushMatrix();
		glTranslatef(1.2,0,0);
		glRotatef(xrot,0.0f,1.0f,0.0f);
		glRotatef(yrot,0.0f,0.0f,-1.0f);
		glScalef(scale,scale,scale);

		glMaterialfv(GL_FRONT_AND_BACK,GL_DIFFUSE, mat_blue);
		this->drawMolecularUnit(helb,mid2,index,dmblPos[1],this->scaleB);
	glPopMatrix();


	if(this->displayBonds)
	{
		glPushMatrix();
			glDisable(GL_LIGHTING);
			glDisable(GL_DEPTH_TEST);
			glColor3f(1,1,1);
			glBegin(GL_LINES);
			for(size_t i=0;i<6;i++){
				if(i<1){
					glVertex3fv(dmblPos[0][i]);
					glVertex3fv(dmblPos[1][i]);
				}else if(this->conCheck[i-1]->getCheck()){
					glVertex3fv(dmblPos[0][i]);
					glVertex3fv(dmblPos[1][i]);
				}
			}
			glEnd();
			glEnable(GL_DEPTH_TEST);
			glEnable(GL_LIGHTING);
		glPopMatrix();
	}

}



//    MyWindow::create()
//
//    Initialization function for our window
//
//    Show the main window on the screen and do some OpenGL initialization.
//
void AtlasNodeSelectionWindow::create()
{

  FXMainWindow::create();
  show(PLACEMENT_SCREEN);           // Show our window on the screen

  // OpenGL initialization
  glcanvas0->makeCurrent();         // Make our canvas current

  cHeight = glcanvas0->getHeight();
  cWidth = glcanvas0->getWidth();

	  glViewport(0,0,cWidth,cHeight);

	glLightfv(GL_LIGHT0, GL_POSITION, light_position);
	glLightfv(GL_LIGHT0, GL_AMBIENT, light_ambient);
	glLightfv(GL_LIGHT0, GL_DIFFUSE, light_diffuse);
	glLightfv(GL_LIGHT0, GL_SPECULAR, light_specular);

	glMaterialfv(GL_FRONT, GL_SPECULAR, mat_specular);
	glMaterialfv(GL_FRONT, GL_AMBIENT, mat_ambient);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, mat_white);
	glMaterialf(GL_FRONT, GL_SHININESS, mat_shininess);

	glEnable(GL_LIGHTING);
	glEnable(GL_LIGHT0);

  glShadeModel(GL_SMOOTH);  // Enable smooth shading
  glEnable (GL_LINE_SMOOTH);
  glEnable (GL_BLEND);
  glBlendFunc (GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
  glHint (GL_LINE_SMOOTH_HINT, GL_FASTEST);
  glClearDepth(1.0f);                                           // Setup the depth buffer
  glEnable(GL_DEPTH_TEST);  // Enable depth testing
  glDepthFunc(GL_LEQUAL);                                       // The type of depth test to do
  glClearColor(0.0f, 0.0f, 0.0f, 0.0f);                         // Set background to BLACK!
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);           // Clear the screen and the depth buffer
  glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NEAREST_MIPMAP_LINEAR);            // Do really nice perspective calculations

  glcanvas0->makeNonCurrent();      // Make our canvas noncurrent
}


void AtlasNodeSelectionWindow::resize(FXint w, FXint h){// resize function for main window

	FXMainWindow::resize(w,h);
}
