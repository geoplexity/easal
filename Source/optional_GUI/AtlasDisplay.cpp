/*
 This file is part of EASAL. 

 EASAL is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 EASAL is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * AtlasDisplay.cpp
 *
 *  Created on: Sep 1, 2015
 *      Author: aysegul
 */

#include "AtlasDisplay.h"


#ifdef __APPLE__
    #include <GLUT/glut.h>
#else
    #include <GL/glut.h>
#endif


static float mat_grey[] = {0.7,0.7,0.7,1.0};
static float mat_yellow[] = {0.8,0.7,0.3,1.0};


AtlasDisplay::AtlasDisplay(Atlas * atlas, SaveLoader* snl) {

    this->treeView = false;  //not to show all atlas at the beginning otherwise takes too much time
    this->numberView = true;
    this->grav = true;
    this->force = false;
    this->levels = false;
    this->forceCurrent = 0;
    active =0;

    this->showall = false;

    this->treeCenter = 0;

    this->updating = false;

    this->snl = snl;
    this->nodes = atlas;
    this->wireIcosahedron = true;
    this->drawAtlasEdges = true;
}



AtlasDisplay::~AtlasDisplay() {

}

void AtlasDisplay::display(int active,bool select)
{

	//pthread_mutex_lock(&rmInUse);
	this->active = active;
  if(this->nodes->number_of_nodes()==0 || this->updating){
    return;
  }
  if(this->levels){
	  this->applyLevels();
  }
    this->select = select;
   glTranslatef(-1 * (*nodes)[active]->getLocation()[0] ,-1 * (*nodes)[active]->getLocation()[1] ,-1 * (*nodes)[active]->getLocation()[2]);

	if(!this->treeView){
		size_t size = nodes->number_of_nodes();
		for(size_t i = 0; i < size;i++){
			if(showall || (*nodes)[i]->hasAnyGoodOrientation() )//if no good orientation, then do not display
			{
				if(select)glPushName(i);
				this->drawNode((*nodes)[i]);
				this->drawConnections(i,false);
				if(select)glPopName();
			}
		}
   }else{
	   if(select)glPushName(this->treeCenter);

	   this->drawNode( (*nodes)[this->treeCenter] );
	   if(select)glPopName();
	   this->recTreeDraw(this->treeCenter,true);
	   this->recTreeDraw(this->treeCenter,false);

   }

	//pthread_mutex_unlock(&rmInUse);
}



void AtlasDisplay::displayHUD(double ratio,bool select,int x, int y){
	glDisable(GL_DEPTH_TEST);
	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	if(select)
	{
		GLint viewport[4];
		glGetIntegerv(GL_VIEWPORT,viewport);
		gluPickMatrix(x,viewport[3]-y,3,3,viewport);
	}
	glOrtho(-1*ratio,ratio,-1,1,-10,10);
	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

	glDisable(GL_LIGHTING);
	glPushName(ROADHUDID);
	glPushName(0);
	glColor3f(.5,.5,.5);

	glLineWidth(2.0);
	drawString(-.14,-.98,0,"Tree");
	if(this->treeView){
		glPushMatrix();
			glTranslatef(-.1,-.9,0);
			glScalef(.05,.05,.05);
			glColor3f(.5,.5,.5);
			glBegin(GL_QUADS);
				glVertex3f(-1,1,0);
				glVertex3f(1,1,0);
				glVertex3f(1,-1,0);
				glVertex3f(-1,-1,0);
			glEnd();

			if(!select){
			glColor3f(.3,.9,.3);
			glBegin(GL_LINES);
				glVertex3f(0,0.8,0);
				glVertex3f(0,-0.8,0);

				glVertex3f(0.0,0.0,0);
				glVertex3f(-0.8,-0.8,0);

				glVertex3f(-0.0,0.0,0);
				glVertex3f(0.8,-0.8,0);
			glEnd();
			}
		glPopMatrix();
	}else{

		glPushMatrix();
			glTranslatef(-.1,-.9,0);
			glScalef(.05,.05,.05);
			glColor3f(.4,.4,.4);
			glBegin(GL_QUADS);
				glVertex3f(-1,1,0);
				glVertex3f(1,1,0);
				glVertex3f(1,-1,0);
				glVertex3f(-1,-1,0);
			glEnd();

			if(!select){
			glColor3f(.3,.9,.3);
			glBegin(GL_LINES);
				glVertex3f(0,0.8,0);
				glVertex3f(0,-0.8,0);

				glVertex3f(0.8,0.8,0);
				glVertex3f(-0.8,-0.8,0);

				glVertex3f(-0.8,0.8,0);
				glVertex3f(0.8,-0.8,0);

				glVertex3f(0.8,0.8,0);
				glVertex3f(0.8,-0.8,0);

				glVertex3f(-0.8,0.8,0);
				glVertex3f(-0.8,-0.8,0);
			glEnd();
			}
		glPopMatrix();
	}

	glLoadName(1);
	glColor3f(.5,.5,.5);
	if(this->force){
		drawString(.045,-.98,0,"Gravity");
	}else if(this->levels){
		drawString(.045,-.98,0,"Gravity (level alignment on)");
	}else{
		drawString(.045,-.98,0,"Gravity (all forces off)");
	}
	if(this->grav){
		glPushMatrix();
			glTranslatef(.1,-.9,0);
			glScalef(.05,.05,.05);
			glColor3f(.5,.5,.5);
			glBegin(GL_QUADS);
				glVertex3f(-1,1,0);
				glVertex3f(1,1,0);
				glVertex3f(1,-1,0);
				glVertex3f(-1,-1,0);
			glEnd();
			glColor3f(.3,.9,.3);
			glPushMatrix();
//			glLineWidth(1.0);

			if(!select){
				for(int i = 0;i<8;i++){
				glRotatef(45,0,0,1);
				glBegin(GL_LINES);
					glVertex3f(.8,0,0);
					glVertex3f(.4,0,0);

					glVertex3f(.5,.1,0);
					glVertex3f(.4,0,0);

					glVertex3f(.5,-.1,0);
					glVertex3f(.4,0,0);
				glEnd();
				}
			}
			glPopMatrix();
		glPopMatrix();
	}else{

		glPushMatrix();
			glTranslatef(0.1,-.9,0);
			glScalef(.05,.05,.05);
			glColor3f(.4,.4,.4);
			glBegin(GL_QUADS);
				glVertex3f(-1,1,0);
				glVertex3f(1,1,0);
				glVertex3f(1,-1,0);
				glVertex3f(-1,-1,0);
			glEnd();
			glColor3f(.3,.9,.3);
			glPushMatrix();
			if(!select){
				for(int i = 0;i<8;i++){
				glRotatef(45,0,0,1);
				glBegin(GL_LINES);
					glVertex3f(.8,0,0);
					glVertex3f(.4,0,0);
				glEnd();
				}
			}
			glPopMatrix();
		glPopMatrix();
	}

	glPopName();
	glPopName();
	glEnable(GL_LIGHTING);

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();
	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();
	glEnable(GL_DEPTH_TEST);
}

void AtlasDisplay::takePick(vector<int> hitnames){
	if(hitnames.size() == 2 && hitnames[0] == ROADHUDID){
		switch(hitnames[1]){

		case 0 :
			this->toggleTree(this->active);
			break;

		case 1 :
			this->toggleGrav();
			break;
		}

	}
}



void AtlasDisplay::applyForces(){
	if(this->force){
		size_t iter;
	   size_t nodesize = nodes->number_of_nodes();
	   size_t inc =200;
		for(iter = this->forceCurrent; iter < (this->forceCurrent + inc) && iter < nodesize;iter++){
			accumForce(iter);
		}


		if(this->forceCurrent < nodesize){
			this->forceCurrent += inc;
		}else{
			this->forceCurrent = 0;
			for(iter = 0;iter < nodesize;iter++){
				(*nodes)[iter]->applyForce();
				(*nodes)[iter]->applyVelocity();
			}
		}
	}
}



void AtlasDisplay::accumForce( size_t t ){
	size_t allSize = nodes->number_of_nodes();
	AtlasNode* tnode = (*nodes)[t];

	if( tnode->getForceMag() < .5 && tnode->numPass < 15){

        for (int i =0; i<3; i++) tnode->getForce()[i]=0;

	// calc attraction to the dimension level
		double *level = new double[3];
		if(this->grav){
			level[0] = -.003 * tnode->getLocation()[0];
			level[1] = -.003 * tnode->getLocation()[1];
		}else{
			level[0] = level[1] = 0;
		}
		level[2] =.5* ((15 * (maxDim - tnode->getDim() )) - tnode->getLocation()[2]);//(double)all.size();

		for (int i=0; i<3; i++) tnode->getForce()[i] += level[i];

		delete [] level;

		//dampening
		for(int i = 0; i<3 ; i++){
			tnode->getVelocity()[i] *= .6;
		}
		tnode->numPass++;
	}else{
		tnode->numPass = 1;
	for(int i =0;i<3;i++){
		tnode->getForce()[i]=0;
	}

	for (size_t iter = 0; iter < allSize; iter++)
	{
		if( iter != tnode->getID() )
		{

			AtlasNode *node = (*nodes)[iter];

			//calc distance
			double d = Utils::dist(tnode->getLocation(), node->getLocation() );
			tnode->numPass++;
			if(d < 40 && d != 0){
			// get unit vector
				double *dir = Utils::vectSub(node->getLocation(), tnode->getLocation());
				double *unit = Utils::scalProd((1.0/d),dir);
			//calc repulsion
				double* rep;
				if(node->getDim() == tnode->getDim() ){
					rep = Utils::scalProd((-40 /(d*d)),unit);
				}else{
					rep = new double[3];
					rep[0] = rep[1] = rep [2] = 0.0;
				}
			//calc attraction
				double level[3];
				if(this->grav){
				level[0] = -.003 * tnode->getLocation()[0];
				level[1] = -.003 * tnode->getLocation()[1];
				}else{
					level[0] = level[1] = 0;
				}
				level[2] =.3* ((15 * (maxDim - tnode->getDim())) - tnode->getLocation()[2]);//(double)all.size();

				for(int i =0; i<3; i++){
					tnode->getForce()[i]+= rep[i] + level[i];
				}

				delete [] dir;
				delete [] unit;
				delete [] rep;
	//			delete [] spring;
				//delete [] level;
			}else{

					// // calc attraction to the dimension level
					double level[3];
					if(this->grav){
					level[0] = -.003 * tnode->getLocation()[0];
					level[1] = -.003 * tnode->getLocation()[1];
					}else{
						level[0] = level[1] = 0;
					}
					level[2] =.3* ((15 * (maxDim - tnode->getDim() )) - tnode->getLocation()[2]);//(double)all.size();
					for(int i =0; i<3; i++){
						tnode->getForce()[i] += level[i];
					}

			}
        }

	//calc attraction
		vector<int> conn = tnode->getConnection();
		for(size_t i = 0; i < conn.size(); i++){
			double *spring;
			AtlasNode *node = (*nodes)[ conn[i] ];
			double d = Utils::dist(tnode->getLocation(),node->getLocation());
		// get unit vector
			double *dir = Utils::vectSub(node->getLocation(), tnode->getLocation());
			double *unit = Utils::scalProd((1.0/d),dir);
			spring = Utils::scalProd(0.3 * (d-2), unit);
			for(int i =0; i<3; i++){
				tnode->getForce()[i] += spring[i];
			}
			delete [] dir;
			delete [] unit;
			delete [] spring;

		}
    }
}
}



void AtlasDisplay::applyLevels(){
	this->levels = true;
	this->force = false;
	size_t iter;
   size_t nodesize = this->nodes->number_of_nodes();
	set<int> locset;
   vector<AtlasNode*> dumbbells;
	for(iter = 0; iter < nodesize;iter++){
		vector<int> conect = (*nodes)[iter]->getConnection();
		bool higher = false;
		for(size_t i = 0;i < conect.size();i++){
			if( (*nodes)[conect[i]]->getDim() > (*nodes)[iter]->getDim()){
				higher = true;
				break;
			}
		}
		if ( !higher){
			locset.insert(iter);
			dumbbells.push_back((*nodes)[iter]);
		}
	}
	double lDim = ceilf( sqrtf(nodesize) );
	int gridSide = ceilf( sqrtf( dumbbells.size() ) );
	double gridDist =15 * lDim / sqrtf( dumbbells.size() );
	for(iter = 0; iter < dumbbells.size();iter++){
		dumbbells[iter]->setLocation(gridDist * (iter % gridSide), gridDist * floorf(iter / gridSide),0);
	}

	for(iter = 0; iter < nodesize;iter++){
		int dim = (*nodes)[iter]->getDim();
			vector<int> con = (*nodes)[iter]->getConnection();
			size_t cSize = con.size();
			double inc = 6.28318 / (double)(cSize) ;
			double *loc = (*nodes)[iter]->getLocation();
			for(size_t i = 0; i < cSize; i++){
				if(dim == (*nodes)[con[i]]->getDim() + 1 && locset.find(con[i]) == locset.end()){
					(*nodes)[con[i]]->setLocation(loc[0] + (gridDist * .025 * dim * dim * cos(i * inc) ),loc[1] + (gridDist * .025 * dim * dim * sin(i * inc) ),loc[2] + 15);
					locset.insert(con[i]);
				}
			}

	}

}

void AtlasDisplay::toggleGrav(){
	this->grav = !this->grav;
}

void AtlasDisplay::toggleTree(int node){
	this->treeView = !this->treeView;
	this->treeCenter = node;
}

void AtlasDisplay::toggleNums(){
	this->numberView = !this->numberView;
}


void AtlasDisplay::toggleShowAll(){
	this->showall = !this->showall;
}

/*
 * Turn off force not to mess up the graph
 */
void AtlasDisplay::toggleForce(){
	cout << "toggleForce\n" << endl;
	this->levels = false;
	this->force = !this->force;
}


void AtlasDisplay::showAtlasEdges(){
	this->drawAtlasEdges = !this->drawAtlasEdges;
}

void AtlasDisplay::drawNode(AtlasNode* node){
	if(!showall && !node->hasAnyGoodOrientation() )//if no good orientation, then do not display
		return;

	double dimcol[7][3] = {{1,.5,1},{.5,.5,1},{0,1,0},{.8,.8,.3},{1,0,0},{.5,.2,.5},{.1,.1,.6}};

	glPushMatrix();
		glEnable(GL_LIGHTING);
		glTranslatef(node->getLocation()[0],node->getLocation()[1],node->getLocation()[2]);

		if(node->isComplete() == 1 || !this->wireIcosahedron){
			glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE, mat_grey);
			glPushMatrix();
				glScalef(0.3,0.3,0.3);
				glutSolidIcosahedron();
			glPopMatrix();
		}else if(node->isComplete() == 0  && this->wireIcosahedron){
			glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE, mat_yellow);
			glPushMatrix();
				glScalef(0.5,0.5,0.5);
				glutWireIcosahedron();
			glPopMatrix();
		}
		glTranslatef(-1,1,0);

		glDisable(GL_LIGHTING);
		int curDim = node->getDim();
		double alpha = 0.5;

		switch(curDim){

					case 5:
                        glColor4f(0,0,1,alpha);
                        hyperCubePlane();
                        break;

                    case 4:
                            glColor4f(1,0,0,alpha);
                            hyperCube();
                    break;

                    case 3:
                            glColor4f(.8,.8,.3,alpha);
                            cube();
                    break;

                    case 2:
                            glColor4f(0,1,0,alpha);
                            plane();
                    break;

                    case 1:
                            glColor4f(.5,.5,1,alpha);
                            line();
			    break;

                    case 0:
                            glColor4f(1,.5,1,alpha);
                            point();
			    break;

                    default:
			glColor4f(dimcol[curDim][0],dimcol[curDim][1],dimcol[curDim][2],alpha);
			cube();
			break;
		}

	glPopMatrix();
	if(this->numberView){
		glColor3f(.5,.5,.3);
		stringstream ss;
		ss << node->getID();
		ss.flush();
		string prnt = ss.str();
		drawString(node->getLocation()[0] +1,node->getLocation()[1]+1,node->getLocation()[2],prnt);
	}
	glEnable(GL_LIGHTING);

}


void AtlasDisplay::drawString(double x, double y, double z, string strn)
{
   glRasterPos3f(x, y, z);

   for (string::size_type c = 0; c < strn.length(); c++)
   {
      glutBitmapCharacter(GLUT_BITMAP_TIMES_ROMAN_10, strn[c]); // Updates the position
   }
}


void AtlasDisplay::recTreeDraw(int current,bool higher){
        if(this->select)glPushName(  (*nodes)[current]->getID());
	this->drawConnections(current,higher);
        this->drawNode( (*nodes)[current]);
        if(this->select)glPopName();
        int dim = (*nodes)[current]->getDim();
        vector<int> con = (*nodes)[current]->getConnection();
	for(size_t i = 0;i != con.size(); i++){
            int x = con[i];
            if( (*nodes)[x]->getDim() > dim && higher)
            {
                recTreeDraw(x,higher);
            }
            else if (((*nodes)[x]->getDim() < dim && !higher))
            {
                recTreeDraw(x,higher);
            }
	}
}

void AtlasDisplay::drawConnections(int from,bool higher){

	if(!showall && !(*nodes)[from]->hasAnyGoodOrientation() )//if no good orientation, then do not display
		return;

	vector<int> con =(*nodes)[from]->getConnection();
	glDisable(GL_LIGHTING);
        int dim = (*nodes)[from]->getDim();
	glColor4f(0,0,0,0.2);
	glLineWidth(1);

	if(this->drawAtlasEdges)
	{
			for(size_t it = 0; it < con.size(); it++){
				if (((*nodes)[con[it]]->getDim() < dim && !higher) || 	((*nodes)[con[it]]->getDim() > dim && higher))
				{// to avoid double drawing lines
					if(higher || (*nodes)[ con[it] ]->hasAnyGoodOrientation() ){//do not draw child connections if they are empty.
						glBegin(GL_LINES);
							glVertex3f(( (*nodes)[from])->getLocation()[0],((*nodes)[from])->getLocation()[1],((*nodes)[from])->getLocation()[2]);
							glVertex3f(( (*nodes)[con[it]])->getLocation()[0],( (*nodes)[con[it]])->getLocation()[1],( (*nodes)[con[it]])->getLocation()[2]);
						glEnd();
					}
				}
			}
	}
	glEnable(GL_LIGHTING);
}


void AtlasDisplay::hyperCubePlane(){

	hyperCube();
	plane();
}


void AtlasDisplay::hyperCube(){
    glPushMatrix();
//        glutSolidCube(1);
		glScalef(.7,.7,.7);
        for(int a = 0; a<3;a++)
            {
            switch(a){
                case 0 : break;
                case 1 :glRotatef(90,0,1,0);
                    break;
                case 2 :glRotatef(90,0,0,1);
                    break;
            }

        glBegin(GL_QUADS);
            glVertex3f(.5,.5,.5);
            glVertex3f(1,1,1);
            glVertex3f(-1,1,1);
            glVertex3f(-.5,.5,.5);

            glVertex3f(.5,-.5,.5);
            glVertex3f(1,-1,1);
            glVertex3f(-1,-1,1);
            glVertex3f(-.5,-.5,.5);

            glVertex3f(.5,.5,-.5);
            glVertex3f(1,1,-1);
            glVertex3f(-1,1,-1);
            glVertex3f(-.5,.5,-.5);

            glVertex3f(.5,-.5,-.5);
            glVertex3f(1,-1,-1);
            glVertex3f(-1,-1,-1);
            glVertex3f(-.5,-.5,-.5);
        glEnd();
        }
    glPopMatrix();
}


void AtlasDisplay::cube(){
    glPushMatrix();
        glutSolidCube(1.0);
    glPopMatrix();
}

void AtlasDisplay::plane(){
    glPushMatrix();
    glBegin(GL_QUADS);
        glVertex3f(1,1,1);
        glVertex3f(1,0,-1);
        glVertex3f(-1,-1,-1);
        glVertex3f(-1,0,1);
    glEnd();
    glPopMatrix();
}

void AtlasDisplay::line(){
    glPushMatrix();
    glRotatef(45,0,1,0);
    glRotatef(45,0,0,1);
    glScaled(20,1,1);
    glutSolidCube(.1);
    glPopMatrix();

}

void AtlasDisplay::point(){
    glPushMatrix();
        glScalef(.3,.3,.3 );
        glutSolidIcosahedron();
    glPopMatrix();
}





void AtlasDisplay::update(){
	if(this->updating){
		cerr << "\nMid update\n";
		return;
	}
	cerr << "\nupdating ";
	this->updating = true;

	this->snl->loadMapView( this->nodes );

	this->updating = false;
	cerr << "update done";

}



bool AtlasDisplay::isUpdating(){
	return this->updating;
}

