/*
 This file is part of EASAL. 

 EASAL is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 EASAL is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef SWEEPVIEW_H_
#define SWEEPVIEW_H_

#include <ctime>

#ifdef __APPLE__
    #include <GLUT/glut.h>
#else
    #include <GL/glut.h>
#endif

#include "ActiveConstraintGraph.h"
#include "ActiveConstraintRegion.h"
#include "AtlasNode.h"
#include "Orientation.h"
#include "ConstraintCheck.h"
#include "Utils.h"

using namespace std;

class SweepView {
public:
	//Constructor
	SweepView(MolecularUnit *helA,MolecularUnit *helB);
	//Set the sweep view for the current node
	void setSpace(int active, AtlasNode* rnod, vector<int> boundList);
	//Destructor
	virtual ~SweepView();
	//Function used to draw on the Canvas
	void display(bool wire = false);
	//Helper function to display the Heads Up Display
	void displayHUD(double ratio,bool select = false,int x = -1,int  y =-1);
	//Function used for determining the user selection
	void takePick(vector<int> hits);
	//Helper function to display a point
	void displayPoint(CayleyPoint *pnt,int bound,size_t &flip,bool wire = false);
	//Function to increment index
	void incIndex(short inc);
	//Function to toggle 
	void toggleShowall();
	//Set a particular realization active
	void setActive(size_t index);
	//Get the active realization
	size_t getActive();
	//Get the orientation that is being displayed
	Orientation* getDisplayedOrient();
	//Get the the molecular units
	void getSweepMolecularUnit(MolecularUnit* helA,MolecularUnit* helB);
	//Function to control the movie Play
	void moviePlay();
	//Function to control the movie Pause
	void moviePause();
	//Function to control the movie Rewind 
	void movieRew();
	//Function to control the movie Stop
	void movieStop();
	//Track all the flips of the molecules
	void trackFlip(size_t flip, bool track);
	void setMulti(size_t multi);
	//Function to toggle movie controls
	void togglesOff();
	//Toggles showing stree
	void toggleShowstree();
	//Toggles showing correct
	void toggleShowCorrect();
	//Toggles showing neighbor
	void toggleShowNeighbour();
	//Enables interior
	void showinterior();
	//Enables show lines
	void showLines();

private:

	//Function to render the sNode on the canvas
	void renderSnode(snode* node, const vector<double> &tran_mat, float *material, bool wire);
	//Helper function to draw a string on the canvas
	void drawString(double x, double y, double z, string strn);
	//Helper function to draw lines on the canvas
	void drawLines();
	//Function to find distortions
	void findDistortions();
	//The Active Constraint Graph of the current ID 
	ActiveConstraintGraph* currentID;
	//The current Active Constraint Region
	ActiveConstraintRegion* region;
	//All the cayley points in the current space
	vector<CayleyPoint*> currentSpace;
	//The two participating atoms
	MolecularUnit *helA, *helB;
	//x-coordinates of the helices
	vector<Atom*> xfHelA, xfHelB;
	//Bools for controling display elements
	bool showAll;
	bool showStree;
	bool showCorrect;
	bool showNeighbour;
	bool trackflip;
	//Variables various indices and sizes
	size_t playflip,active,curIndex,playIndex,spaceSize;
	int displayBoundary,flipFilter;

	//List of boundary points
	vector<int> curBoundlist;
	//x-coordinate of the boundary
	size_t boundex;

	//colors for boundaries
	map<int,GLfloat>boundaries;

	//Video porperties
	size_t fps, fpscount, framecount, multi;
	time_t lastTime;
	double delay;

	//Distortion in the A and B atoms
	double distortionA,distortionB;

	//Bools to control display elements
	bool showControls,showBound;
	bool play,rewind,pause;
	bool showInterior;
	bool showlines;

	//Function to render the canvas
	void render(MolecularUnit* hlx,const vector<double> &trans_mat,float *material,bool repeat = false,bool wire = false);
	//set true if view changes
	bool dirtyBit; 

	// class for balls
	struct Bal{
		double loc[3]; //location
		float col[4]; // color RGBA
		double rad;	//radius
		bool wire;
	};
	struct Lin{//class for lines
		double loc[2][3]; //end point locations
		float col[3]; // color RGB
		bool dot;	//dotted
	};


	//List of balls visible
	list<Bal> viewBalls;
	//List of lines visible
	list<Lin> viewLines;

	//Buffer lines
	void bufferLin(double *loca,double *locb, float *color, bool dotted);
	//Buffer Ball
	void bufferBall(double *locat,float *color, double rad,bool wire);
	//Draw the buffer
	void drawBuffer();

	double viewcenter[3];
	//Set the view center
	void setViewCenter(MolecularUnit* h1,MolecularUnit* h2,double fa[][3], double ta[][3],double fb[][3], double tb[][3]);
	void setViewCenter(MolecularUnit* h,const vector<double> &trans_mat);

	void renderCorrect();

};

#endif /* SWEEPVIEW_H_ */
