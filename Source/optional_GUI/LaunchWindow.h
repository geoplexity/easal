/*
 This file is part of EASAL. 

 EASAL is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 EASAL is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef LAUNCHWINDOW_H_
#define LAUNCHWINDOW_H_

#include "PredefinedInteractions.h"
#include "SelectionWindow.h"
// #include "DistanceSelectionWindow.h"

#include <fox/fx.h>

#include <string>
#include <vector>


class LaunchWindow: public FX::FXMainWindow {
public:
	LaunchWindow(FXApp *app);
	// LaunchWindow(std::string location);
	virtual ~LaunchWindow();

	virtual void create();

	enum {
		ID_ACCEPT=FXWindow::ID_LAST,
		ID_EXIT,
		ID_BROWSE_MUA,
		ID_SETDATA_MUA,
		ID_BROWSE_MUB,
		ID_SETDATA_MUB,
		ID_BROWSE_DISTDATA,
		ID_SETDATA_DISTDATA,
		ID_BROWSE_DATADIR,
		ID_BROWSE_ADVOPTIONS,
		ID_LAST
	};

	/**
	 * @brief Launches a file browser to select the file for the first molecular unit
	 * @details Uses the built in FOX file browser. Parameters sent by FOX. Bound to ID_BROWSE_MUA.
	 */
	long button_browse_MolecularUnitA(FXObject *sender, FXSelector, void *);
	/**
	 * @brief Launches a SelectionWindow to set the labels for the data in the file chosen for MolecularUnitA
	 * @details Parameters sent by FOX. Bound to ID_SETDATA_MUA.
	 */
	 long button_setData_MolecularUnitA(FXObject *sender, FXSelector, void *);


	/**
	 * @brief Launches a file browser to select the file for the second molecular unit
	 * @details Uses the built in FOX file browser. Parameters sent by FOX.
	 *     Bound to ID_BROWSE_MUB.
	 */
	long button_browse_MolecularUnitB(FXObject *sender, FXSelector, void *);
	/**
	 * @brief Launches a SelectionWindow to set the labels for the data in the
	 *     file chosen for MolecularUnitB
	 * @details Parameters sent by FOX. Bound to ID_SETDATA_MUB.
	 */
	 long button_setData_MolecularUnitB(FXObject *sender, FXSelector, void *);


	/**
	 * @brief Launches a file browser to select the file for the distance data
	 * @details Uses the built in FOX file browser. Parameters sent by FOX.
	 *     Bound to ID_BROWSE_DISTDATA.
	 */
	long button_browse_DistanceData(FXObject *sender, FXSelector, void *);
	/**
	 * @brief Launches a SelectionWindow to set the labels for the data in the
	 *     file chosen for Distance Data
	 * @details Parameters sent by FOX. Bound to ID_SETDATA_DISTDATA.
	 */
	 long button_setData_DistanceData(FXObject *sender, FXSelector, void *);


	/**
	 * @brief Launches a directory browser to select the dir for saving data
	 * @details Uses the built in FOX directory browser. Parameters sent by FOX.
	 *     Bound to ID_BROWSE_DATADIR.
	 */
	long button_browse_DataDir(FXObject *sender, FXSelector, void *);

	/**
	 * @brief Stores the affected values into the global "Settings" namespace
	 * @details If the data directory alread has data in it, pressing accept
	 *     will prompt the user to either discard the data and use the selected
	 *     settings or to use the old data. Parameters sent by FOX. Bound to
	 *     ID_ACCEPT.
	 */
	long button_accept(FXObject *sender, FXSelector, void *);
	/**
	 * @brief Cancels the LaunchWindow leaving settings unchanged and
	 *     terminating the program.
	 */
	long button_exit(FXObject *sender, FXSelector, void *);


	/**
	 * @brief Doesn't actually do anything now. Could be used to disable buttons
	 *     unless input choices satisfy certain conditions.
	 */
	long approve_selections(FXObject *sender,FXSelector, void *);
	
	/*
	 * Used to set the advanced options
	 */ 
	long button_browse_AdvOptions(FXObject *sender, FXSelector, void *);

	FXDECLARE(LaunchWindow);

private:

	LaunchWindow();

	/**
	 * @brief A more general function that button_browse_MolecularUnitA,
	 *     button_browse_MolecularUnitB, and button_browse_DistanceData call
	 */
	void browse_file(FXTextField *tb);

	/**
	 * General functions for retrieving values from the FOX input boxes
	 */
	std::string getTextBox_string(unsigned int box_number);
	std::string getTextBox_string_directory(unsigned int box_number);
	double getTextBox_double(unsigned int box_number);
	bool getCheckBox(unsigned int box_number);
	bool good_input(std::string&);


	FXApp *app;

	// Text fields for Input window
	static const unsigned int
		tb_molecularUnitA = 0,
		tb_molecularUnitB = 1,
		tb_distanceData = 2,
		tb_dataDirectory = 3, 
		tb_stepSize = 4,
		tb_threshold = 5,
		tb_min = 6,
		tb_max = 7,
		tb_lambda_low = 8,
		tb_lambda_high = 9,
		tb_delta_low = 10,
		tb_delta_high = 11,
		tb_collision_lambda = 12,
		tb_collision_delta = 13,
		tb_collision_theta_low = 14,
		tb_collision_theta_high = 15;
	FXTextField *text[16];

	//Check boxes for the input window
	static const unsigned int
		cb_sample = 0,
		cb_solver = 1, // old maple
		cb_multi = 2,  // autoGenDumbbells
		cb_angle = 3,
		cb_virus = 4;
	FXCheckButton *check[5];

	//Buttons for the Input window
	FXButton *acptButton, *aBrowse, *bBrowse, *aTable, *bTable, *cTable, *advOpt;
	//Pointer to sub windows in the input window
	DataSelectionWindow *dsw_MolecularUnitA, *dsw_MolecularUnitB;
	DistanceSelectionWindow *dsw_distance;
	AdvancedOptionsWindow *aow_options;

};

#endif
