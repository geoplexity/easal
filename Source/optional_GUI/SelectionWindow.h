/*
 This file is part of EASAL. 

 EASAL is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 EASAL is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef SELECTIONWINDOW_H_
#define SELECTIONWINDOW_H_

#include <fox/fx.h>

#include <vector>
#include <string>

#include"Settings.h"


/**
 * @brief Used to make windows that have a table displayed in them where
 * you select the column labels and which rows to ignore.
 */
class SelectionWindow: public FX::FXDialogBox {
public:
	//enum for window actions
	enum {
		ID_NEW=FXDialogBox::ID_LAST,
		ID_ACCEPT,
		ID_APPROVE,
		ID_CLEAR,
		ID_LAST
	};

	//Default Constructor
	SelectionWindow() {}
	//Constructor
	SelectionWindow(FXWindow *owner, const FXString &name)
		: FXDialogBox(owner,name) {}
	//Destructor
	virtual ~SelectionWindow() {}

	// FX bound functions
	long button_accept(FXObject *sender, FXSelector, void *);
	long button_clearTable(FXObject *sender, FXSelector, void *);
	long approve_selections(FXObject *sender, FXSelector, void *);

	// called after setting all the column labels and ignore checks for additional settings
	virtual void set_default_values() = 0;
	// called after button_accept to save any changes beyond ignored rows and col labels
	virtual void save_changes() = 0;
	// if it returns true, then approve selections will enable the accept button
	virtual bool good_input() = 0;

	virtual FXuint execute();

	// to see if this is using the same file
	bool same_file(std::string filename);


	// column label stuff
	void set_column_of(unsigned int label, int *column);

	// ignored rows stuff
	void ignored_rows(std::vector<int> *rows);


	/**
	 * @brief Call when you are ready to save all of the input data into the
	 * pointers given with set_column_of and ignored_rows.
	 */
	void save_input();

protected:
	//Function to intialize the table
	void init_table(FXVerticalFrame *vertical_frame, std::string filename);
	//initialize the buttons
	void init_bottom_buttons(FXVerticalFrame *vertical_frame);

	struct column_label {
		std::string label; // the label
		int position; // the column the label is associated with
		int *position_save_pointer; // where to save the accepted value at the end

		column_label(std::string l)
			: label(l), position(-1), position_save_pointer(NULL) {}
	};

	std::string file_used;

	//The accept button
	FXButton *accept_button_p;
	//Check button for ignore checks
	std::vector<FXCheckButton*> ignore_checks;
	//Combo box for label dropdowns
	std::vector<FXComboBox*> label_dropdowns;

	//Column labels
	std::vector<column_label> _column_labels;
	//Indices of rows ignored
	std::vector<int> _ignored_rows;
	std::vector<int> *_ignored_rows_save_pointer;
};








class DataSelectionWindow: public SelectionWindow {
public:
	// these numbers are dictated by the order in which column_labels are pushed
	// back into private member _column_labels in the constructor.
	// They are to be used with set_ and get_column_of methods.
	static const unsigned int
		x_col = 0,
		y_col = 1,
		z_col = 2,
		radius_col = 3,
		label_col = 4,
		atomNo_col = 5;

	//Default Constructor
	DataSelectionWindow() {}
	//Constructor
	DataSelectionWindow(FXWindow *owner, const FXString &name,
		std::string filename);
	//Function to set default values in the sub windows
	void set_default_values() {}
	//Save changes made in the sub windows
	void save_changes() {}
	//Validity check for the input
	bool good_input();

	FXDECLARE(DataSelectionWindow);
};


class DistanceSelectionWindow: public SelectionWindow {
public:
	// these numbers are dictated by the order in which column_labels are pushed
	// back into private member _column_labels in the constructor.
	// They are to be used with set_ and get_column_of methods.
	static const unsigned int
		label1_col = 0,
		label2_col = 1,
		radius_col = 2,
		radiusMin_col = 3,
		radiusMax_col = 4;

	//Default Constructor
	DistanceSelectionWindow() {}
	//Constructor
	DistanceSelectionWindow(FXWindow *owner, const FXString &name,
		std::string filename);
	//Function to set default values in the data selection window
	void set_default_values();
	//Save changes made in the distance window
	void save_changes();
	//Function used to validate the windows
	bool good_input();

	void setSettings(bool matrixCheck, bool radiusCheck, double rangeLow, double rangeHigh);

/*	bool matrix();
	bool radius();
	double radiusMin();
	double radiusMax();
*/
	FXDECLARE(DistanceSelectionWindow);

private:
	// only updated with setSettings and at the time of accepting
	bool matrixCheck, radiusCheck;
	double rangeLow, rangeHigh;

	//Check buttons for the data selection window
	FXCheckButton *cb_matrix, *cb_radius;
	//Text fields for the data selection window
	FXTextField *tf_rangeLow, *tf_rangeHigh;
};





class AdvancedOptionsWindow: public SelectionWindow {
public:
	//Default Constructor
	AdvancedOptionsWindow() {}
	//Constructor
	AdvancedOptionsWindow(FXWindow *owner, const FXString &name, int max_atoms, double max_len, double max_zlen);
	//Function to set default values
	void set_default_values();
	//Save changes made in the advanced options window
	void save_changes();
	//Functions used to validate the input
	bool good_input();
	//Helper function used for validation of data
	bool validate_input(std::string &errors);
	//Function to save settings
    void save_settings();
	//
	//void setSettings(bool matrixCheck, bool radiusCheck, double rangeLow, double rangeHigh);
	long button_accept(FXObject *sender, FXSelector, void *d);

	FXDECLARE(AdvancedOptionsWindow);

private:
	bool reverseWitness, 
		 wholeCollisions,
		 fourDRootNode,
		 dynamicStepSizeAmong,
		 dynamicStepSizeWithin, 
		 reversePairDumbbells, 
		 useParticipatingAtomZDistance,
		 short_range_sampling;
	int ParticipatingAtomZDistance,
		participatingAtomIndex_low, 
		participatingAtomIndex_high, 
		frequency,
		max_atoms;
	float initial4DContactSeparation_low, 
		  initial4DContactSeparation_high,
		  max_len,
		  max_zlen;

	// only updated with setSettings and at the time of accepting
	FXCheckButton *cb_reverseWitness, 
				  *cb_wholeCollisions,
				  *cb_fourDRootNode,
				  *cb_dynamicStepSizeAmong,
				  *cb_dynamicStepSizeWithin, 
				  *cb_reversePairDumbbells, 
				  *cb_useParticipatingAtomZDistance,
				  *cb_shortRangeSampling;
	FXTextField *tf_ParticipatingAtomZDistance,
				*tf_participatingAtomIndex_low,
				*tf_participatingAtomIndex_high,
				*tf_initial4DContactSeparation_low,
				*tf_initial4DContactSeparation_high,
				*tf_frequency;
};

#endif
