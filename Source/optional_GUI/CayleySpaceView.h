/*
 This file is part of EASAL. 

 EASAL is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 EASAL is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef CAYLEYSPACEVIEW_H_
#define CAYLEYSPACEVIEW_H_

#define SPACEHUDID 4

#include "AtlasNode.h"
#include "Atlas.h"
#include "SaveLoader.h"


#ifdef __APPLE__
    #include <GLUT/glut.h>
#else
    #include <GL/glut.h>
#endif


#include <vector>
#include <list>
#include <utility>



class CayleySpaceView {
public:
	//Constructor
	CayleySpaceView(AtlasNode* rnode, Atlas *atlas, SaveLoader* snl);
	//Destructor
	virtual ~CayleySpaceView();


	//Set the current cayley space based on the node selected
	void setSpace(AtlasNode *rnod);
	//getter for Space
	std::vector<CayleyPoint*> getSpace();

	//Function to display the Heads Up Display
	void displayHUD(ActiveConstraintGraph* active,int activeNum,double ratio,bool select = false,int x =0,int y=0);


	//Set a particular cayley point active
	void setActive(size_t active);
	//Get the currently active cayley point 
	CayleyPoint* getActive();

	//Get the index of the currently active cayley point 
	int getActiveIndex();

	//Function to draw the Cayley Space canvas
	void display(bool select = false);

	//Helper function to draw a point
	void drawPoint(CayleyPoint *a,int index,float colr[3],double alpha = 1.0);

	//Helper function to draw a Norm point
	void drawNormPoint(CayleyPoint *a,int index,double alpha = 1.0);

	//Helper function to select the unrealizable, good and bad cayley points
    bool getColort(CayleyPoint *a, float out[3]);

	// void huetoColor(double hue, double color[3]);

	//Helper function to draw a slider
	void drawSlider(double valMin,double valMax,double val,double hilight,double xLeft, double xRight, double yMid, float material[4]);

	// int processHits (int hits, GLuint buffer[]);
	void takePick(std::vector<int> hits);

	//Get the Boundary Point
	CayleyPoint* getCanonPnt();
	CayleyPoint* getBoundPnt(int bound);

	//Toggle Boundaries
	void toggleBoundaries();
	//Toggle Collision
	void toggleCollision();
	//Tells whether we want to show collisions or not
	bool getCollision();
	//Toggle Trans Boundaries
	void toggleTransBoundaries();

	//Function used to increment the boundary index displayed
	void incBoundaries(short inc);

	//Toggles showing of lesser dimensions
	void toggleLesserDim();

	//Set the lesser dimension
	void setLesserDim(size_t inc);

	//Set the dimension currently being viewed
	void setViewdim(int ndim);

	//Increment the dimension being viewed
	void incDim(size_t dim,double inc);

	//Get the middle of the XYZ
	double* getXYZmid();

	//Get a list of all boundary points
	std::vector<int> getBoundList();

	//Set Grid view when we are using Jacobian Sampling
	void setGridView();


	//Project space
	std::vector<CayleyPoint*> projectSpace(std::vector<CayleyPoint*> spc);

	Matrix3d quaterniontoRotation(double q0, double q1, double q2, double q3 );



//private:
protected:
	//Helper function to draw strings on the canvas
	void drawString(double x, double y, double z, string strn);

	//This function assigns indices to positions in the names array
	void addNames(int *names,std::vector<std::pair<int,int> > links,int &left,int &right);

	// Atlas for getting translated spaces of lesser dimensions
	Atlas *atlas; 
	//Active constraint graph of the current node
	ActiveConstraintGraph *curGraph;
	//Active Constraint Region of the current node
	ActiveConstraintRegion* region;
	//The root node
	AtlasNode *rnode;

	// number of dimensions in display equals 2 or 3
	size_t dim; 
	//Step size
	double step;
	
	//dimVal is the current selected value on the slider
	std::vector<double> dimMax,dimMin,dimMid,dimVal; 
	double *xyzMid;

	int t; 
	std::vector<CayleyPoint*> currentSpace;
	int activeIndex;
	
	//Bools to toggle display elements
	bool showBound,transBound,select,witness, showCollision;
	bool showBoundIndices;
	bool showLesserDim, transDim;
	bool allSlider;
	bool projectOnOtherSpace;

	//Boundary Index
	size_t boundaryIndex;
	//List of all boundary points
	std::vector<int> boundList;
	//Map of boundaries to their locations
	map<int,GLfloat>boundaries;
	CayleyPoint* blankpoint;

	//Index of the lesser dimension
	size_t lesserDimindex,witSize;
	std::vector<CayleyPoint*> lesserSpace;
	// pairs of <node number,color(hue value)>
	std::vector< std::pair<size_t,double> > lesserColor;
	//  the list for display
	std::list<std::pair <size_t,double> > dimcol; 
	// pairs of <point index,energy value>
	std::vector< std::pair<size_t,float> > energyValue;

	//Pointer to the SaveLoader object
    SaveLoader* snl;
};

#endif /* SPACEVIEW_H_ */
