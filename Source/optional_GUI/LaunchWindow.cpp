/*
 This file is part of EASAL. 

 EASAL is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 EASAL is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "LaunchWindow.h"

#include "SelectionWindow.h"
#include "Settings.h"

#include <iostream>
#include <cstdio>
#include <sys/stat.h>

#include <fstream>
#include <sstream>
#include <vector>
#include <algorithm>
#include <utility>


using namespace std;


/*
 * This list provides information for the callbacks from the foxtoolkit
 * It maps the particular Enum values to the specific functions.
 */
FXDEFMAP(LaunchWindow) HMainWinMap[]={
	FXMAPFUNC(SEL_COMMAND, LaunchWindow::ID_ACCEPT,           LaunchWindow::button_accept),
	FXMAPFUNC(SEL_COMMAND, LaunchWindow::ID_EXIT,             LaunchWindow::button_exit),
	FXMAPFUNC(SEL_COMMAND, LaunchWindow::ID_BROWSE_MUA,       LaunchWindow::button_browse_MolecularUnitA),
	FXMAPFUNC(SEL_COMMAND, LaunchWindow::ID_SETDATA_MUA,      LaunchWindow::button_setData_MolecularUnitA),
	FXMAPFUNC(SEL_COMMAND, LaunchWindow::ID_BROWSE_MUB,       LaunchWindow::button_browse_MolecularUnitB),
	FXMAPFUNC(SEL_COMMAND, LaunchWindow::ID_SETDATA_MUB,      LaunchWindow::button_setData_MolecularUnitB),
	FXMAPFUNC(SEL_COMMAND, LaunchWindow::ID_BROWSE_DISTDATA,  LaunchWindow::button_browse_DistanceData),
	FXMAPFUNC(SEL_COMMAND, LaunchWindow::ID_SETDATA_DISTDATA, LaunchWindow::button_setData_DistanceData),
	FXMAPFUNC(SEL_COMMAND, LaunchWindow::ID_BROWSE_DATADIR,   LaunchWindow::button_browse_DataDir),
	FXMAPFUNC(SEL_COMMAND, LaunchWindow::ID_BROWSE_ADVOPTIONS,LaunchWindow::button_browse_AdvOptions),
	FXMAPFUNC(SEL_UPDATE,  LaunchWindow::ID_UPDATE,           LaunchWindow::approve_selections)
};

FXIMPLEMENT(LaunchWindow,FXMainWindow,HMainWinMap,ARRAYNUMBER(HMainWinMap));

bool validate_interactions_file(string muA, string muB, string predefInt) {
	ifstream fileA;
	fileA.open(muA.c_str());

	ifstream fileB;
	fileB.open(muB.c_str());

	ifstream filePI;
	filePI.open(predefInt.c_str());

	// check that the files were good.
    if (!fileA.is_open()) {
        cout << "File \"" << muA << "\" does not exist." << endl;
        fileA.close();
        fileB.close();
        filePI.close();
		return false;
    }
    if (!fileB.is_open()) {
        cout << "File \"" << muB << "\" does not exist." << endl;
        fileA.close();
        fileB.close();
        filePI.close();
        return false;
    }
    if (!filePI.is_open()) {
        cout << "File \"" << predefInt << "\" does not exist." << endl;
        fileA.close();
        fileB.close();
        filePI.close();
        return false;
    }

	vector<string> atomsA;
	vector<string> atomsB;
	for(int row = 0; !fileA.eof(); row++) {
		string line;
		getline(fileA, line);

		if( line.empty() || line == " ")
        	continue;

		vector<string> line_data;
		std::stringstream line_ss(line);
		string s;
		while(line_ss >> s) line_data.push_back(s);

        stringstream col_ss(line_data[1]);
		string atomNo;
		col_ss >>atomNo;
		atomsA.push_back(atomNo);
	}

	for(int row = 0; !fileB.eof(); row++) {
		string line;
		getline(fileB, line);

		if( line.empty() || line == " ")
        	continue;

		vector<string> line_data;
		std::stringstream line_ss(line);
		string s;
		while(line_ss >> s) line_data.push_back(s);

        stringstream col_ss(line_data[1]);
		string atomNo;
		col_ss >>atomNo;
		atomsB.push_back(atomNo);
	}

	for(int row = 0; !filePI.eof(); row++) {
		string line;
		getline(filePI, line);

		if( line.empty() || line == " ")
        	continue;

		vector<string> line_data;
		std::stringstream line_ss(line);
		string s;
		while(line_ss >> s) line_data.push_back(s);

		string atomNo;
        stringstream col_ss(line_data[0]);
		col_ss >>atomNo;
		if(std::find(atomsA.begin(), atomsA.end(), atomNo) == atomsA.end()) {
			fileA.close();
			fileB.close();
			filePI.close();
			return false;
		}


		stringstream col_ss1(line_data[1]);
		col_ss1 >>atomNo;
		if(std::find(atomsB.begin(), atomsB.end(), atomNo) == atomsB.end()) {
			fileA.close();
			fileB.close();
			filePI.close();
			return false;
		}
	}

	fileA.close();
	fileB.close();
	filePI.close();
	return true;
}

// This will be used for validation of the input files
bool LaunchWindow::good_input(string &errors) {
	bool validity = true;
	struct  stat buffer;
	//Validate if the input molecular files are valid
	string filename = text[tb_molecularUnitA]->getText().text();
	if(stat(filename.c_str(), &buffer) != 0) {
		errors+= "File " + filename +" does not exist.\n";
		validity = false;
	}

	filename = text[tb_molecularUnitB]->getText().text();
	if(stat(filename.c_str(), &buffer) != 0) {
		errors+= "File " + filename +" does not exist.\n";
		validity = false;
	}
	//Check if the distance data file is valid or not
	filename = text[tb_molecularUnitB]->getText().text();
	if(stat(filename.c_str(), &buffer) != 0) {
		//Set Virus_mer to false
		Settings::General::candidate_interactions= false;
	}

	if(validate_interactions_file(text[tb_molecularUnitA]->getText().text(),text[tb_molecularUnitB]->getText().text(),text[tb_distanceData]->getText().text())){
		Settings::General::candidate_interactions = true;
	} else {
		Settings::General::candidate_interactions = false;
	}


	//Validate the data dir
	filename = text[tb_dataDirectory]->getText().text();
	int pos = filename.find_last_of("/");
	string directory = filename.substr(0,pos);
	if(stat(directory.c_str(), &buffer) != 0) {
		errors+= "Directory" + directory + " does not exist.\n";
		validity = false;
	}
	//Validate the lambdas - should be betwen 0-1
	int lambda = atoi(text[tb_lambda_low]->getText().text());
	if(lambda > 1 || lambda < 0) {
		errors+= "lambda_low should be between 0 and 1.\n";
		validity = false;
	}

	lambda = atoi(text[tb_lambda_high]->getText().text());
	if(lambda > 1 || lambda < 0) {
		errors+= "lambda_high should be between 0 and 1.\n";
		validity = false;
	}
	lambda = atoi(text[tb_collision_lambda]->getText().text());
	if(lambda > 1 || lambda < 0) {
		errors+= "lambda_high should be between 0 and 1.\n";
		validity = false;
	}
	//Validate the Thetas - should be within 0 - 360
	int theta_high = atoi(text[tb_collision_theta_high]->getText().text());
	if(theta_high > 360 || theta_high < 0) {
		errors+= "Inter-helical Angle Constraint theta_high should be between 0 and 360.\n";
		validity = false;
	}
	int theta_low = atoi(text[tb_collision_theta_low]->getText().text());
	if(theta_low > 360 || theta_low < 0) {
		errors+= "Inter-helical Angle Constraint theta_low should be between 0 and 360.\n";
		validity = false;
	}

	if(theta_low > theta_high) {
		errors+= "Inter-helical Angle Constraint theta_low should be less than theta_high.\n";
		validity = false;
	}

	//Validate the step size - should be +ve
	double stepSize = atof(text[tb_stepSize]->getText().text());
	if(stepSize < 0) {
		errors+= "Step size should be greater than 0.\n";
		validity = false;
	}
	return validity;
}

LaunchWindow::LaunchWindow() {
	// This class won't work without being passed a FXApp pointer.
}


/*
 * the main constructor of the window.
 */
LaunchWindow::LaunchWindow(FXApp *app) : FXMainWindow(app, "EASAL- Input Window",
	NULL, NULL,	DECOR_BORDER|DECOR_TITLE|DECOR_RESIZE, 0, 0, 0, 0)
{
	this->app = app;
	dsw_MolecularUnitA = NULL;
	dsw_MolecularUnitB = NULL;
	dsw_distance = NULL;
	aow_options = NULL;
}


LaunchWindow::~LaunchWindow() {
}

// This function sets the layout and contents of the window.
void LaunchWindow::create() {
	// for converting numbers to strings to fill textboxes
	std::stringstream ss;

	FXVerticalFrame *tab1  = new FXVerticalFrame(this, LAYOUT_FILL_Y);


	FXMatrix *matrix;
	FXHorizontalFrame *row;
	FXHorizontalFrame *cthreshold_row;
	FXHorizontalFrame *cinpthreshold_row;
	FXHorizontalFrame *cinpthreshold_row2;
	FXHorizontalFrame *Constraint_threshold_row;
	FXHorizontalFrame *Constraint_threshold_input_row;

	matrix = new FXMatrix(tab1, 4, MATRIX_BY_COLUMNS|FRAME_GROOVE); //sets the frame for the data visual elements to be stacked


	//////////////////////
	// Data for Molecule A
	//////////////////////
	// label
	new FXLabel(matrix, "Data for Molecule A", NULL, LAYOUT_CENTER_Y|LAYOUT_RIGHT);

	// textbox
	this->text[tb_molecularUnitA] = new FXTextField(matrix, 30, NULL,
		0, LAYOUT_CENTER_X|LAYOUT_CENTER_Y);
	this->text[tb_molecularUnitA]->setText(
		Settings::MolecularUnitA::file.c_str());

	// button - browse
	aBrowse = new FXButton(matrix, "Browse", NULL, this, LaunchWindow::ID_BROWSE_MUA,
		BUTTON_NORMAL|LAYOUT_BOTTOM|LAYOUT_FILL_X,
		0,0,0,0,10,10,5,5);// button sends the ID_DIRDIA value which means to open the file dialog for data A

	// button - set data
	aTable = new FXButton(matrix,"Set Data", NULL, this, LaunchWindow::ID_SETDATA_MUA,
		BUTTON_NORMAL|LAYOUT_BOTTOM|LAYOUT_FILL_X,
		0,0,0,0,10,10,5,5);// opens the Data A file for  category selections

	//////////////////////
	// Data for Molecule B
	//////////////////////
	// label
	new FXLabel(matrix, "Data for Molecule B", NULL, LAYOUT_CENTER_Y|LAYOUT_RIGHT);

	// textbox
	this->text[tb_molecularUnitB] = new FXTextField(matrix, 30, NULL,
		0, LAYOUT_CENTER_X|LAYOUT_CENTER_Y);
	this->text[tb_molecularUnitB]->setText(
		Settings::MolecularUnitB::file.c_str());

	// button - browse
	bBrowse = new FXButton(matrix, "Browse", NULL, this, LaunchWindow::ID_BROWSE_MUB,
		BUTTON_NORMAL|LAYOUT_BOTTOM|LAYOUT_FILL_X,
		0,0,0,0,10,10,5,5);// button sends the ID_DIRDIA value which means to open the file dialog for data A

	// button - set data
	bTable = new FXButton(matrix,"Set Data", NULL, this, LaunchWindow::ID_SETDATA_MUB,
		BUTTON_NORMAL|LAYOUT_BOTTOM|LAYOUT_FILL_X,
		0,0,0,0,10,10,5,5);// opens the Data A file for  category selections


	//////////////////////
	// Distance data
	//////////////////////
	// TODO: different
	new FXLabel(matrix, "Predefined Interactions", NULL, LAYOUT_CENTER_Y|LAYOUT_RIGHT);

	// textbox
	this->text[tb_distanceData] = new FXTextField(matrix, 30, NULL,
		0, LAYOUT_CENTER_X|LAYOUT_CENTER_Y);
	this->text[tb_distanceData]->setText(
		Settings::DistanceData::file.c_str());

	// button - browse
	new FXButton(matrix, "Browse", NULL, this, LaunchWindow::ID_BROWSE_DISTDATA,
		BUTTON_NORMAL|LAYOUT_BOTTOM|LAYOUT_FILL_X,
		0,0,0,0,10,10,5,5);// button sends the ID_DIRDIA value which means to open the file dialog for data A

	// button - set data
	cTable = new FXButton(matrix,"Set Data",NULL,this,LaunchWindow::ID_SETDATA_DISTDATA,
		BUTTON_NORMAL|LAYOUT_BOTTOM|LAYOUT_FILL_X,
		0,0,0,0,10,10,5,5);// opens the Data A file for  category selections



	//////////////////////
	// Data directory
	//////////////////////
	// label
	new FXLabel(matrix, "Data Directory", NULL, LAYOUT_CENTER_Y|LAYOUT_RIGHT);

	// text box
	this->text[tb_dataDirectory] = new FXTextField(matrix, 30, NULL, 0,
		LAYOUT_CENTER_X|LAYOUT_CENTER_Y);
	this->text[tb_dataDirectory]->setText(
		Settings::Output::dataDirectory.c_str());

	// button - browse
	new FXButton(matrix, "Browse" ,NULL, this, LaunchWindow::ID_BROWSE_DATADIR,
			BUTTON_NORMAL|LAYOUT_BOTTOM|LAYOUT_FILL_X,
			0,0,0,0,10,10,5,5);


	//////////////////////
	// Other
	//////////////////////

	//These are boxes for the lower range of thresholds
	matrix = new FXMatrix(tab1, 4, MATRIX_BY_COLUMNS|FRAME_GROOVE);

	new FXLabel(matrix, "Bonding Threshold ", NULL, LAYOUT_LEFT);
	new FXLabel(matrix, "lambda", NULL, LAYOUT_LEFT);
	new FXLabel(matrix, "", NULL, LAYOUT_LEFT);
	new FXLabel(matrix, "delta", NULL, LAYOUT_LEFT);

	new FXLabel(matrix, "Lower Bound = ", NULL, LAYOUT_CENTER_X|LAYOUT_CENTER_Y);
	this->text[tb_lambda_low] = new FXTextField(matrix, 15, NULL, 0,
		LAYOUT_CENTER_X|LAYOUT_CENTER_Y);
	ss.str("");
	ss << Settings::Constraint::bondingLowerLambda;
	this->text[tb_lambda_low]->setText(ss.str().c_str());

	new FXLabel(matrix, "* (ri+rj) + ", NULL, LAYOUT_CENTER_X|LAYOUT_CENTER_Y);
	this->text[tb_delta_low] = new FXTextField(matrix, 15, NULL, 0,
		LAYOUT_CENTER_X|LAYOUT_CENTER_Y);
	ss.str("");
	ss << Settings::Constraint::bondingLowerDelta;
	this->text[tb_delta_low]->setText(ss.str().c_str());

	//These are boxes for the higher range of thresholds
	new FXLabel(matrix, "Upper Bound = ", NULL, LAYOUT_CENTER_X|LAYOUT_CENTER_Y);
	this->text[tb_lambda_high] = new FXTextField(matrix, 15, NULL, 0,
		LAYOUT_CENTER_X|LAYOUT_CENTER_Y);
	ss.str("");
	ss << Settings::Constraint::bondingUpperLambda;
	this->text[tb_lambda_high]->setText(ss.str().c_str());

	new FXLabel(matrix, "* (ri+rj) + ", NULL, LAYOUT_CENTER_X|LAYOUT_CENTER_Y);
	this->text[tb_delta_high] = new FXTextField(matrix, 15, NULL, 0,
		LAYOUT_CENTER_X|LAYOUT_CENTER_Y);
	ss.str("");
	ss << Settings::Constraint::bondingUpperDelta;
	this->text[tb_delta_high]->setText(ss.str().c_str());

	new FXLabel(matrix, "Collision Threshold ", NULL, LAYOUT_LEFT);
	new FXLabel(matrix, "", NULL, LAYOUT_LEFT);
	new FXLabel(matrix, "", NULL, LAYOUT_LEFT);
	new FXLabel(matrix, "", NULL, LAYOUT_LEFT);

	new FXLabel(matrix, "Lower Bound = ", NULL, LAYOUT_CENTER_X|LAYOUT_CENTER_Y);
	this->text[tb_collision_lambda] = new FXTextField(matrix, 15, NULL,
			0, LAYOUT_CENTER_X|LAYOUT_CENTER_Y);
	ss.str("");
	ss << Settings::Constraint::collisionLambda;
	this->text[tb_collision_lambda]->setText(ss.str().c_str());

	new FXLabel(matrix, "* (ri+rj) + ", NULL, LAYOUT_CENTER_X|LAYOUT_CENTER_Y);
	this->text[tb_collision_delta] = new FXTextField(matrix, 15, NULL,
			0, LAYOUT_CENTER_X|LAYOUT_CENTER_Y);
	ss.str("");
	ss << Settings::Constraint::collisionDelta;
	this->text[tb_collision_delta]->setText(ss.str().c_str());



	//These are boxes for the Angle Constraints
	new FXLabel(matrix, "Inter-helical Angle Constraint", NULL, LAYOUT_LEFT);
	new FXLabel(matrix, "", NULL, LAYOUT_LEFT);
	new FXLabel(matrix, "", NULL, LAYOUT_LEFT);
	new FXLabel(matrix, "", NULL, LAYOUT_LEFT);
	new FXLabel(matrix, "theta_low = ", NULL, LAYOUT_CENTER_X|LAYOUT_CENTER_Y);
	this->text[tb_collision_theta_low] = new FXTextField(matrix, 15, NULL,
			0, LAYOUT_CENTER_X|LAYOUT_CENTER_Y);
	ss.str("");
	ss << Settings::Constraint::angleLow;
	this->text[tb_collision_theta_low]->setText(ss.str().c_str());

	new FXLabel(matrix, "theta_high = ", NULL, LAYOUT_CENTER_X|LAYOUT_CENTER_Y);
	this->text[tb_collision_theta_high] = new FXTextField(matrix, 15, NULL,
			0, LAYOUT_CENTER_X|LAYOUT_CENTER_Y);
	ss.str("");
	ss << Settings::Constraint::angleHigh;
	this->text[tb_collision_theta_high]->setText(ss.str().c_str());


	matrix = new FXMatrix(tab1, 3, MATRIX_BY_COLUMNS); //sets the frame for the data visual elements to be stacked



	// step size
	row = new FXHorizontalFrame(tab1, LAYOUT_FILL_X);
	new FXLabel(row, "Step Size = ", NULL, LAYOUT_LEFT|LAYOUT_FILL_X);
	this->text[tb_stepSize] = new FXTextField(row, 15, NULL, 0,
		LAYOUT_LEFT|LAYOUT_FILL_X);
	ss.str("");
	ss << Settings::Sampling::stepSize;
	this->text[tb_stepSize]->setText(ss.str().c_str());
	advOpt = new FXButton(row, "Advanced Options" ,NULL, this, LaunchWindow::ID_BROWSE_ADVOPTIONS,
			BUTTON_NORMAL|LAYOUT_FILL_X,
			0,0,0,0,10,10,3,3);

	row = new FXHorizontalFrame(tab1, FRAME_GROOVE| LAYOUT_BOTTOM |LAYOUT_FILL_X);

	// accept
	acptButton = new FXButton(row, "Accept", NULL, this, LaunchWindow::ID_ACCEPT,
		BUTTON_NORMAL|LAYOUT_FILL_Y|LAYOUT_FILL_X,
		0,0,0,20,10,10,5,5);

	// exit
    new FXButton(row, "Exit", NULL, this, LaunchWindow::ID_EXIT,
		BUTTON_NORMAL|LAYOUT_FILL_Y|LAYOUT_FILL_X,
		0,0,0,20,10,10,5,5);


	//////////////////////
	// Finishing touches
	//////////////////////

	show(PLACEMENT_SCREEN); // Places the window in the center of the screen.
	FXMainWindow::create(); // Creates the behind the scenes Fox window attributes.
}

//This is the function called when the accept button is pushed.
long LaunchWindow::button_accept(FXObject *sender, FXSelector, void *){

	bool use_existing_settings = false;


	// Handle pre-existing data
	string data_dir = getTextBox_string(tb_dataDirectory);

	stringstream node0_file, settings_file;
	node0_file << data_dir << "/Node0.txt";
	settings_file << data_dir << "/settings.ini";

	string nf = node0_file.str();
	string sf = settings_file.str();

	ifstream testFile;

	testFile.open(node0_file.str().c_str());
	bool node_exists = testFile.is_open();
	if (node_exists) testFile.close();

	testFile.open(settings_file.str().c_str());
	bool settings_exists = testFile.is_open();
	if (settings_exists) testFile.close();

	string errors;

	if(!good_input(errors)) {
		FXMessageBox::warning(this,MBOX_OK,
				"Warning",
				errors.c_str()
				);
		return 1;
	}
	if(node_exists && settings_exists) {
		int choice = FXMessageBox::warning(this,MBOX_YES_NO_CANCEL,
			"Warning",
			"A data directory with this name already exists and\ncontains previous nodes and settings.\n\nWould you like to load the old settings and nodes (yes)?\n(OR overwrite and use the new settings (no))."
			);

		switch(choice) {
			case FX::MBOX_CLICKED_CANCEL:
				return 1;
				break;
			case FX::MBOX_CLICKED_YES:
				use_existing_settings = true;
				Settings::load(settings_file.str().c_str());
				Settings::Sampling::runSample = false;
				break;
			case FX::MBOX_CLICKED_NO:

				string command = string("exec rm -r ");
				command += data_dir;
				system(command.c_str());
				break;
		}

	}



	if (!use_existing_settings) {
	    ///////////////////////
	    // MolecularUnitA
	    ///////////////////////
		Settings::MolecularUnitA::file = getTextBox_string(tb_molecularUnitA);
		if (dsw_MolecularUnitA != NULL) dsw_MolecularUnitA->save_input();


	    ///////////////////////
	    // MolecularUnitB
	    ///////////////////////
	    Settings::MolecularUnitB::file = getTextBox_string(tb_molecularUnitB);
		if (dsw_MolecularUnitB != NULL) dsw_MolecularUnitB->save_input();


	    ///////////////////////
	    // DistanceData
	    ///////////////////////
	    Settings::DistanceData::file = getTextBox_string(tb_distanceData);
		if (dsw_distance != NULL) {
			dsw_distance->save_input();


		}

	    ///////////////////////
	    // Output
	    ///////////////////////
	    Settings::Output::dataDirectory = getTextBox_string_directory(tb_dataDirectory);

	    ///////////////////////
	    // General
	    ///////////////////////
	    Settings::Sampling::stepSize  = getTextBox_double(tb_stepSize);

	    //TODO: check if the order of the following lines matters ?!!!
	    Settings::Constraint::bondingLowerLambda = getTextBox_double(tb_lambda_low);
	    Settings::Constraint::bondingLowerDelta = getTextBox_double(tb_delta_low);
	    Settings::Constraint::bondingUpperLambda = getTextBox_double(tb_lambda_high);
	    Settings::Constraint::bondingUpperDelta = getTextBox_double(tb_delta_high);
	    Settings::Constraint::collisionLambda = getTextBox_double(tb_collision_lambda);
	    Settings::Constraint::collisionDelta = getTextBox_double(tb_collision_delta);
	    Settings::Constraint::angleLow = getTextBox_double(tb_collision_theta_low);
	    Settings::Constraint::angleHigh = getTextBox_double(tb_collision_theta_high);

		if(use_existing_settings || Settings::Sampling::runSample == Stopped) {
			Settings::Sampling::runMode = Stopped;
		} else {
			Settings::Sampling::runMode = ForestSampleAS;
		}

	    // save settings
		mkdir(Settings::Output::dataDirectory.c_str(), S_IRWXU | S_IRWXG | S_IRWXO ); //the folder has to exist first
	    Settings::save(settings_file.str().c_str());
	}
    // app stuff
    this->hide();
	this->app->stop();
	return 1;
}

//This is the function called when the close button is pushed.
long LaunchWindow::button_exit(FXObject *sender, FXSelector, void *){
	cout << "Terminating program, settings not changed." << std::endl;
	exit(0);
	return 1;
}


void LaunchWindow::browse_file(FXTextField *tb) {
	FXFileDialog open(this, "Open File");
	open.setFilename(tb->getText());
	open.setPatternList("Data Files (*.dat)\nText Files (*.txt)\nAll Files (*.*)");
	open.allowPatternEntry(true);
	if (open.execute()) {
		tb->setText(open.getFilename());
	}
}


//This is the function called when the top browse button is pushed.
// it opens a predefined file dialog window.
long LaunchWindow::button_browse_MolecularUnitA(FXObject *sender, FXSelector, void *){
	this->browse_file(text[tb_molecularUnitA]);
	return 1;
}

//This is the function called when the middle browse button is pushed.
// it opens a predefined file dialog window.
long LaunchWindow::button_browse_MolecularUnitB(FXObject *sender, FXSelector, void *){
	this->browse_file(text[tb_molecularUnitB]);
	return 1;
}
//This is the function called when the middle browse button is pushed.
// it opens a predefined file dialog window.
long LaunchWindow::button_browse_DistanceData(FXObject *sender, FXSelector, void *){
	this->browse_file(text[tb_distanceData]);
	return 1;
}

//This is the function called when the bottom browse button is pushed.
// it opens a predefined directory dialog window.
long LaunchWindow::button_browse_DataDir(FXObject *sender, FXSelector, void *){
	FXDirDialog open(this,"Open directory");
	open.setDirectory(this->text[tb_dataDirectory]->getText());
	open.showFiles(TRUE);
	string dir = ".";
	if(open.execute()) {
		this->text[tb_dataDirectory]->setText(open.getDirectory().text());
	}

	dir = getTextBox_string(tb_dataDirectory);
	if(dir[dir.size()-1] != '/' && dir[dir.size()-1] != '\''){
			dir.append("/");
	} else {
		this->text[tb_dataDirectory]->setText(dir.substr(0,dir.size()-1).c_str());
	}

	return 1;
}



long LaunchWindow::button_setData_MolecularUnitA(FXObject *sender, FXSelector, void *) {
	string filename = getTextBox_string(tb_molecularUnitA);

	ifstream file;
	file.open(filename.c_str());
	if (!file.is_open()) {
		cout <<  "Not a valid file." << endl;
		return 1;
	} else {
		file.close();
	}

	// If we already made dsw, check if it's useing the same file. If it isn't delete the old.
	if (dsw_MolecularUnitA && !dsw_MolecularUnitA->same_file(filename)) {
		delete dsw_MolecularUnitA;
		dsw_MolecularUnitA = NULL;
	}

	if (!dsw_MolecularUnitA) {
		dsw_MolecularUnitA = new DataSelectionWindow(this, "Set Data", filename);

		dsw_MolecularUnitA->set_column_of(DataSelectionWindow::x_col,      &Settings::MolecularUnitA::x_col);
		dsw_MolecularUnitA->set_column_of(DataSelectionWindow::y_col,      &Settings::MolecularUnitA::y_col);
		dsw_MolecularUnitA->set_column_of(DataSelectionWindow::z_col,      &Settings::MolecularUnitA::z_col);
		dsw_MolecularUnitA->set_column_of(DataSelectionWindow::radius_col, &Settings::MolecularUnitA::radius_col);
		dsw_MolecularUnitA->set_column_of(DataSelectionWindow::label_col,  &Settings::MolecularUnitA::label_col);
		dsw_MolecularUnitA->set_column_of(DataSelectionWindow::atomNo_col, &Settings::MolecularUnitA::atomNo_col);

		dsw_MolecularUnitA->ignored_rows(&Settings::MolecularUnitA::ignored_rows);
	}

	dsw_MolecularUnitA->execute();

	return 0;
}

long LaunchWindow::button_setData_MolecularUnitB(FXObject *sender, FXSelector, void *) {
	string filename = getTextBox_string(tb_molecularUnitB);

	ifstream file;
	file.open(filename.c_str());
	if (!file.is_open()) {
		cout <<  "Not a valid file." << endl;
		return 1;
	} else {
		file.close();
	}

	// If we already made dsw, check if it's useing the same file. If it isn't delete the old.
	if (dsw_MolecularUnitB && !dsw_MolecularUnitB->same_file(filename)) {
		delete dsw_MolecularUnitB;
		dsw_MolecularUnitB = NULL;
	}

	if (!dsw_MolecularUnitB) {
		dsw_MolecularUnitB = new DataSelectionWindow(this, "Set Data", filename);

		dsw_MolecularUnitB->set_column_of(DataSelectionWindow::x_col,      &Settings::MolecularUnitB::x_col);
		dsw_MolecularUnitB->set_column_of(DataSelectionWindow::y_col,      &Settings::MolecularUnitB::y_col);
		dsw_MolecularUnitB->set_column_of(DataSelectionWindow::z_col,      &Settings::MolecularUnitB::z_col);
		dsw_MolecularUnitB->set_column_of(DataSelectionWindow::radius_col, &Settings::MolecularUnitB::radius_col);
		dsw_MolecularUnitB->set_column_of(DataSelectionWindow::label_col,  &Settings::MolecularUnitB::label_col);
		dsw_MolecularUnitB->set_column_of(DataSelectionWindow::atomNo_col, &Settings::MolecularUnitB::atomNo_col);

		dsw_MolecularUnitB->ignored_rows(&Settings::MolecularUnitB::ignored_rows);
	}

	dsw_MolecularUnitB->execute();

	return 0;
}



long LaunchWindow::button_setData_DistanceData(FXObject *sender, FXSelector, void *) {
	string filename = getTextBox_string(tb_distanceData);

	ifstream file;
	file.open(filename.c_str());
	if(!file.is_open()){
		cout <<  "Not a valid file." << endl;
		return 1;
	}else{
		file.close();
	}

	// If we already made dsw, check if it's useing the same file. If it isn't delete the old.
	if (dsw_distance && !dsw_distance->same_file(filename)) {
		delete dsw_distance;
		dsw_distance = NULL;
	}

	if (!dsw_distance) {
		dsw_distance = new DistanceSelectionWindow(this, "Set Distances", filename);
		dsw_distance->set_column_of(DistanceSelectionWindow::label1_col,    &Settings::DistanceData::label1_col);
		dsw_distance->set_column_of(DistanceSelectionWindow::label2_col,    &Settings::DistanceData::label2_col);
		dsw_distance->set_column_of(DistanceSelectionWindow::radius_col,    &Settings::DistanceData::radius_col);
		dsw_distance->set_column_of(DistanceSelectionWindow::radiusMin_col, &Settings::DistanceData::radiusMin_col);
		dsw_distance->set_column_of(DistanceSelectionWindow::radiusMax_col, &Settings::DistanceData::radiusMax_col);

		dsw_distance->ignored_rows(&Settings::DistanceData::ignored_rows);

	}

	dsw_distance->execute();

	return 0;
}

int find_max_atoms(string filename) {
	ifstream file;
	file.open(filename.c_str());
    if (!file.is_open()) {
        cout << "File \"" << filename << "\" does not exist." << endl;
        file.close();
		return 0;
    }
	int max_atoms = 0;
	for(int row = 0; !file.eof(); row++) {
		string line;
		getline(file, line);
		if( line.empty() || line == " ")
        	continue;
		max_atoms++;
	}
	return max_atoms-1;
}

struct point {
	double x,
		   y,
		   z;
};
double find_distance(point *p1, point * p2) {
	return sqrt(pow(p1->x - p2->x, 2)+pow(p1->y - p2->y, 2)+pow(p1->z - p2->z, 2));
}
double find_max_len(string filename) {
	ifstream file;
	file.open(filename.c_str());
    if (!file.is_open()) {
        cout << "File \"" << filename << "\" does not exist." << endl;
        file.close();
		return 0;
	}
	vector<point*> distances;
	for(int row = 0; !file.eof(); row++) {
		point *loc = new point();
		string line;
		getline(file, line);

		if( line.empty() || line == " ")
        	continue;

		vector<string> line_data;
		std::stringstream line_ss(line);
		string s;
		while(line_ss >> s) line_data.push_back(s);

        stringstream col_sx(line_data[6]);
		string pos;
		col_sx >>pos;
		loc->x = atof(pos.c_str());

		stringstream col_sy(line_data[7]);
		col_sy >>pos;
		loc->y = atof(pos.c_str());

		stringstream col_sz(line_data[8]);
		col_sz >>pos;
		loc->z = atof(pos.c_str());

		distances.push_back(loc);
	}
	double max = 0;

	for(vector<point*>::iterator it = distances.begin(); it != distances.end(); ++it) {
		for(std::vector<point*>::iterator it2 = it; it2 != distances.end(); ++it2) {
			double d = find_distance(*it, *it2);
			if(d> max)
				max = d;

		}
	}
	return max;
}



double find_max_zlen(string filename) {
	ifstream file;
	file.open(filename.c_str());
    if (!file.is_open()) {
        cout << "File \"" << filename << "\" does not exist." << endl;
        file.close();
		return 0;
	}

	vector<double> zdistances;
	for(int row = 0; !file.eof(); row++) {
		string line;
		getline(file, line);

		if( line.empty() || line == " ")
        	continue;

		vector<string> line_data;
		std::stringstream line_ss(line);
		string s;
		while(line_ss >> s) line_data.push_back(s);

        stringstream col_ss(line_data[8]);
		string atomNo;
		col_ss >>atomNo;
		zdistances.push_back(atof(atomNo.c_str()));
	}
	std::sort(zdistances.begin(), zdistances.end());
	return fabs(zdistances.front() - zdistances.back());

}

//Function for setting advanced options
long LaunchWindow::button_browse_AdvOptions(FXObject *sender, FXSelector, void *){
	if(!aow_options) {
		aow_options = new AdvancedOptionsWindow(this, "Advanced Options",find_max_atoms(text[tb_molecularUnitA]->getText().text()),find_max_len(text[tb_molecularUnitA]->getText().text()),find_max_zlen(text[tb_molecularUnitA]->getText().text()));
	}

	aow_options ->execute();
    aow_options->save_settings();

	return 0;
}

// TODO: maybe disable in some cases?
/*
 * And attempt to enable/disable the accept button due to the state
 * of pieces of the window.
 */
long LaunchWindow::approve_selections(FXObject *sender, FXSelector, void *){
	// if(this->helixA == NULL || this->helixB == NULL){
	// 	this->cTable->disable();
	// 	this->acptButton->disable();
	// }else{
	// 	this->cTable->enable();
	// 	this->acptButton->enable();
	// }
	this->repaint();
	return 1;
}


std::string LaunchWindow::getTextBox_string(unsigned int box_number) {
	return this->text[box_number]->getText().text();
}

std::string LaunchWindow::getTextBox_string_directory(unsigned int box_number) {
	string directory = getTextBox_string(box_number);
	#ifdef WIN32
		if(directory[directory.size()-1] != '\\'){
			directory.append("\\");
		}
	#else
		if(directory[directory.size()-1] != '/'){
			directory.append("/");
		}
	#endif
	return directory;
}

double LaunchWindow::getTextBox_double(unsigned int box_number) {
	stringstream ss;
	ss << getTextBox_string(box_number);
	double output;
	ss >> output;
	return output;
}

bool LaunchWindow::getCheckBox(unsigned int box_number) {
	return this->check[box_number]->getCheck();
}
