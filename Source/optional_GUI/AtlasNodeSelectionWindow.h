/*
 This file is part of EASAL. 

 EASAL is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 EASAL is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef ATLASNODESELECTIONWINDOW_H_
#define ATLASNODESELECTIONWINDOW_H_


#include "MolecularUnit.h"
#include "ActiveConstraintGraph.h"
#include "Atlas.h"


#include <fox/fx.h>                       // Header for FOX-toolkit
#include <fox/fx3d.h>                     // Header for FXGL stuff

#ifdef __APPLE__
    #include <GLUT/glut.h>
#else
    #include <GL/glut.h>
    #include <GL/gl.h>
#endif



#ifdef WIN32
  #include <windows.h>
#endif

/*
 * This is the class used for creating the Constraint Selection
 * Dialogue Box.
 */

class AtlasNodeSelectionWindow : public FXMainWindow
{
  FXDECLARE(AtlasNodeSelectionWindow)
 private:

  /* Default Constructor */
  AtlasNodeSelectionWindow() {}

  /* Pointer to the Constraint Selection Window App */
  FXApp *app;
  /* Used for OpenGL drawing */
  FXGLCanvas *glcanvas0;
  /* OpenGL Visual */
  FXGLVisual *glvisual0;

  /* Dials for zooming in and out of the atoms */
  FXDial *dial1x, *dial1y,*dial2x,*dial2y;
  /* Box for selecting the constraints */
  FXComboBox *pointA,*pointB;

  FXButton* acceptButton;
  FXButton* loadButton;

  /* optional contacts */
  int optCont ;
  /* Check boxes to connect the optional buttons*/
  FXCheckButton *conCheck[5];

  /* Comboboxes to specify the optional constraints */
  FXComboBox *pointHA[5], *pointHB[5];

  /* Text field to specify the node number to load*/
  FXTextField *nodeNum;

  /* Canvas height and width.*/
  int cHeight,cWidth; 

  /* The two helicies to display.*/
  MolecularUnit *hela, *helb;

 
  /* Mid points of the atom locations */
  float mid1[3],mid2[3];

  float scale,scaleA,scaleB;

  /* Active Constraint graph of the node loaded */

  ActiveConstraintGraph *cgK;
  Atlas * mapView;

  /* This function draws the Constraint Selection Dialogue*/
  void display();

  /* Helper function used to draw a string on the conavas */
  void drawString(float x, float y, float z, string strn);

  /* Helper function used to draw a molecule */
  void drawMolecularUnit(MolecularUnit *hel,float mid[3],size_t index[3],float dmblPos[2][3], float helixScale);

 public:
  /* Constructor declaration */
  AtlasNodeSelectionWindow(FXApp *,Atlas * mapview);
  /* window destructor declaration */
  virtual ~AtlasNodeSelectionWindow();
  /* Initialization function for window */
  virtual void create();
  // resize function for window
  virtual void resize(FXint w, FXint h);

  // Function Callbacks
  // Painting callback function declaration
  long onPaint(FXObject *, FXSelector, void *);
  // mouse callback function
  long mouse(FXObject *, FXSelector, void *);
  // Function for the accept button
  long accept(FXObject *, FXSelector, void *);
  // Function for the cancel button
  long cancel(FXObject *, FXSelector, void *);
  // Function to load a active constraint graph
  long loadGraph(FXObject *, FXSelector, void *);


  //Setter for molecular units
  void setMolecularUnits(MolecularUnit *hela,MolecularUnit *helb);
  //Getter for molecular units
  void getMolecularUnits(MolecularUnit *hela,MolecularUnit *helb);
  //Getter for dumbbell indices
  void getDumbbellIndices(int &a1,int &b1,int &a2,int &b2);
  //Setter for dumbbell indices
  void setDumbbellIndices(int a1,int b1,int a2,int b2);
  //Getter for Active Constraint Graph
  ActiveConstraintGraph* getGraph();

  // Define message target for this window
  enum                                
  {
    ID_INTEXT=FXMainWindow::ID_LAST,
    ID_CANVAS,                        // Added for possible canvas callbacks (not actively used in this particular example)
    ID_ANIMATION,                     // ID for animation callbacks
    ID_ACCEPT,                        // ID for accept button presses.
    ID_CANCEL,                        // ID for cancel button presses.
    ID_LOAD,                        // ID for Load Graph button presses.
    ID_LAST                           // This is always last
  };


  // Function to draw numbers on each atom of the molecule
  bool displayNumbersOnMolecularUnit;
  // Function to display the bonds between the two molecules
  bool displayBonds;

};

#endif /* DumbbellSELECTWINDOW_H_ */
