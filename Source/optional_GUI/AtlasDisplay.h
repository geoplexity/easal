/*
 This file is part of EASAL. 

 EASAL is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 EASAL is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * AtlasDisplay.h
 *
 *  Created on: Sep 1, 2015
 *  Author: aysegul
 */

#ifndef ATLASDISPLAY_H_
#define ATLASDISPLAY_H_

#include "AtlasNode.h"
#include "Atlas.h"
#include "SaveLoader.h"
//class SaveLoader;

class AtlasDisplay {
public:

	static const int maxDim = 5; //is used in force computation


	//Constructor declaration
	AtlasDisplay(Atlas * atlas, SaveLoader* snl);

	//Destructor declaration
	virtual ~AtlasDisplay();

	//Helper function to draw an atlas node
	void drawNode(AtlasNode* node);
	//Function to draw the Atlas View Window
    void display(int active, bool select = false);
	//Function to draw the Heads Up Display icons
    void displayHUD(double ratio,bool select = false,int x = 0, int y = 0);

	//Function that tells us which HUD was clicked
    void takePick(std::vector<int> hitnames);
	//Function that is called to enable forces on the nodes
    void applyForces();
	//Function that is called to enable level ordering of nodes
    void applyLevels();

	//Helper function to draw a string on the canvas
    void drawString(double x, double y, double z, string strn);
	//Function to toggle gravity
    void toggleGrav();
	//Function to toggle the tree view
    void toggleTree(int node);
	//Function to toggle node numbers
    void toggleNums();
	//Function to toggle show all
    void toggleShowAll();
	//Function to toggle force
    void toggleForce();
	//Function to show the edges between nodes of the atlas
    void showAtlasEdges();
	//Function to update the mapview
    void update();
	//Function to check if update is in progress
    bool isUpdating();


private:
	void accumForce(size_t t);  
	//Function to recursively draw the Atlas
	void recTreeDraw(int current,bool higher);
	//Function to draw a hypercube plane a.k.a. 5D node
	void hyperCubePlane();
	//Function to draw a hypercube a.k.a. 4D node
    void hyperCube();
	//Function to draw a cube a.k.a. 3D node
	void cube();
	//Function to draw the plane a.k.a. 2D node
    void plane();
	//Function to draw the line a.k.a. 1D node
    void line();
	//Function to draw the point a.k.a. 0D node
    void point();

	//Draw edges between nodes in the atlas
	void drawConnections(int from,bool higher);

	//Bool indicating if update is happening or not.
	bool updating;

	//Bools for various options for atlas display
	bool treeView,grav,numberView,force,levels;
	bool wireIcosahedron, drawAtlasEdges;
	bool select;
	bool showall;
	size_t forceCurrent;

    int active;
	int treeCenter;


	//The atlas node
	Atlas * nodes;
	//The saveloader
    SaveLoader* snl;


};

#endif /* ATLAS_H_ */
