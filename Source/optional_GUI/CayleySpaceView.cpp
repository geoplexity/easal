/*
 This file is part of EASAL. 

 EASAL is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 EASAL is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * SpaceView.cpp
 *
 *  Created on: Mar 19, 2009
 *      Author: James Pence
 */

#include "CayleySpaceView.h"
#include "CayleyPoint.h"
#include "ActiveConstraintGraph.h"
#include "ActiveConstraintRegion.h"


#include <fox/fx.h> //to use PI

#include "Eigen/Core"
#include "Eigen/Geometry"
#include "Eigen/LU"
using Eigen::Vector3d;
using Eigen::Matrix3d;
using Eigen::Quaterniond;


#include <cstring>
#include <set>
#include <cmath>
#include <sstream>
#include <iostream>
#include <algorithm>
#include <map>
#include <vector>
using namespace std;



CayleySpaceView::CayleySpaceView(AtlasNode* rnod, Atlas *atlas, SaveLoader* snl) {
	this->blankpoint = new CayleyPoint();
	this->atlas = atlas; 
	this->step = .2;
	this->activeIndex = 0;
	this->xyzMid = new double[6];
	this->dim = 3;
	this->allSlider = false;
	this->setSpace( rnod );
	this->showBoundIndices = true;
	this->showCollision= false;
	this->projectOnOtherSpace = false;
	this->snl = snl;
}

CayleySpaceView::~CayleySpaceView() {
	delete this->xyzMid;
}

void CayleySpaceView::drawString(double x, double y, double z, string strn)
{
   glRasterPos3f(x, y, z);
   int row = 0;
   for (string::size_type c = 0; c < strn.length(); c++)
   {
	   if(strn[c] == '\n'){
		   row++;
		   glRasterPos3f(x, y - (row * .04), z);
	   }
      glutBitmapCharacter(GLUT_BITMAP_TIMES_ROMAN_10, strn[c]); // Updates the position
   }
}

void CayleySpaceView::setSpace(AtlasNode *rnod){

	//We can keep a pointer to rnode since we mostly have fixed size members except connections. 
	//For connections, check how many connections we have per node and decide.
	this->rnode =  rnod;
	//We can keep a pointer fo this too. Mostly fixed stuff.
	this->curGraph =  rnod->getCG();
	//Need to copy all the items here. 
	//Has a vector of CayleyPoints which in turn has more vectors. 
	/*if(this->region != NULL)
		delete this->region;*/
	this->region = new ActiveConstraintRegion(*rnod->getACR());

	size_t curdim = this->curGraph->getDim();
	if(curdim < this->dim){
		this->dim = curdim;
	}else if(curdim >= 3){
		this->dim = 3;
	}
	this->dimMax.clear();
	this->dimMin.clear();
	this->dimMid.clear();
	this->dimVal.clear();
	this->t = 0;

	if(this->projectOnOtherSpace)
		this->currentSpace = projectSpace( this->region->getSpace() );
	else{
		this->currentSpace = this->region->getSpace();
	}

	this->witSize = this->region->getWitnessSize();
	this->boundaryIndex = 0;
	this->activeIndex = 0;

	this->witness = true; //showing witnesses with an icosoherdron
	this->showBound = false;//showing the boundaries
	this->showCollision = false;//showing the boundaries
	this->transBound = true;//when looking at one bound show the others transparently
	this->showLesserDim = false;//showing lower dimensions
	this->transDim = false;//when showing lower dimensions show regular view transparent
	this->lesserSpace.clear();
	this->lesserDimindex = 0;

	list<int>boundlist;
	{
		size_t iter = 0;
		while(iter < this->currentSpace.size()){
			if(this->currentSpace[iter] == NULL)
				continue;
			if( this->currentSpace[iter]->hasOrientation() ){
				this->activeIndex = iter;
			}
			iter++;
		};
	}


	if( !currentSpace.empty() ){
		CayleyPoint* p = currentSpace.front();
		for(size_t d = 0;d < 6;d++){
			this->dimMin.push_back((*p)[d]);
			this->dimMax.push_back((*p)[d]);
			this->dimVal.push_back((*this->currentSpace[this->activeIndex])[d]);
		}
		for(size_t iter = 0;iter < this->currentSpace.size();iter++){
			p = this->currentSpace[iter];

			for(size_t d = 0;d < 6;d++){
				this->dimMin[d] = fmin((*p)[d] , this->dimMin[d]);
				this->dimMax[d] = fmax((*p)[d] , this->dimMax[d]);
			}

			if( p->hasOrientation() )
			{
				list<int> bound = p->getBoundaries();

				boundlist.insert(boundlist.end(),bound.begin(),bound.end());
			}
		}

	}
	else
	{
		for(size_t d = 0;d < 6;d++){
			this->dimMin.push_back(0);
			this->dimMax.push_back(0);
		}
	}

	for(size_t d = 0;d < 6;d++){
		this->dimMid.push_back( (dimMin[d] + dimMax[d]) / 2.0);
		this->xyzMid[d] = this->dimMid[d];
	}

	boundlist.sort();
	boundlist.unique();

	this->boundList.assign(boundlist.begin(),boundlist.end());
	this->boundaries.clear();
	if(! boundlist.empty()){
		double amt = 0;
		double inc = 1.0 / double(boundlist.size());

		for(list<int>::iterator it = boundlist.begin();it != boundlist.end(); it++){
			this->boundaries[*it] = amt;
			amt += inc;
		}
	}

}

CayleyPoint* CayleySpaceView::getCanonPnt(){
	if(this->currentSpace.empty() ){
		return this->blankpoint;
	}

	int ind = -1;
	double pnt[6];
	double d = 0;// = Utils::dist(this->xyzMid,pnt);
	double d2 = 0;
	for(size_t i = 0; i < this->currentSpace.size(); i++){
		if( this->currentSpace[i]->hasOrientation() )
		{
			if(ind < 0){
				ind = i;
				this->currentSpace[i]->getPoint(pnt);
				d = Utils::dist6(this->xyzMid,pnt);
			}
			this->currentSpace[i]->getPoint(pnt);
			d2 = Utils::dist6(this->xyzMid,pnt);
			if(d2 < d){
				d = d2;
				ind = i;
			}
		}
	}
	return this->currentSpace[ind];
}

CayleyPoint* CayleySpaceView::getBoundPnt(int bound){
	if(find(this->boundList.begin(),this->boundList.end(),bound) != this->boundList.end() && bound != -1){
		if(this->currentSpace.size() >0){
			size_t ind = 0;
			double pnt[6];
			double d = -1;
			double d2;
			for(size_t i = 0; i < this->currentSpace.size(); i++){
				list<int> bndlst = this->currentSpace[i]->getBoundaries();
				if(find(bndlst.begin(),bndlst.end(),bound) != bndlst.end()){
					this->currentSpace[i]->getPoint(pnt);
					if(d <0){
						d = Utils::dist6(this->xyzMid,pnt);
						ind = i;
					}
					d2 = Utils::dist6(this->xyzMid,pnt);
					if(d2 < d){
						d = d2;
						ind = i;
					}
				}
			}
			return this->currentSpace[ind];
		}
	}
	return this->blankpoint;

}

void CayleySpaceView::displayHUD(ActiveConstraintGraph* active,int activeNum,double ratio,bool select,int x, int y){

	double axis[6][3]= {  {.8, 0.0, 0.0},{0.0, .8, 0.0},{0.0, 0.0, .8},{.8, .8, 0.0},{.8, 0.4, 0.0},{0.0, .8, .8}};

	this->step = active->getStepSize();
	glDisable(GL_DEPTH_TEST);
	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	if(select)
	{
		GLint viewport[4];
		glGetIntegerv(GL_VIEWPORT,viewport);
		gluPickMatrix(x,viewport[3]-y,3,3,viewport);
	}
	glOrtho(-1*ratio,ratio,-1,1,-10,10);
	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();
	
	glColor3f(.5,.5,.5);
	CayleyPoint* pnt;
	if(! this->currentSpace.empty()){
		glDisable(GL_LIGHTING);
		pnt = this->currentSpace[this->activeIndex];
		stringstream coord;
		coord << "x(red) " << (*pnt)[0] << "\n y(green) " << (*pnt)[1] << "\n z(blue) " << (*pnt)[2] << "\n w(yellow) " << (*pnt)[3] << "\n  " << (*pnt)[4] << "\n  " << (*pnt)[5];
		drawString(-.95,-.5,0,coord.str());

		glEnable(GL_LIGHTING);
		if(this->allSlider){
			for(size_t d = 3; d < 6;d++){
					float material[] = {(float)axis[d][0],(float)axis[d][1],(float)axis[d][2],1.0};
					glPushName(d);
					drawSlider(this->dimMin[d],this->dimMax[d],this->dimVal[d],(*pnt)[d],0.4,0.9,-1.15 + (d * .10), material);
					glPopName();//end d
			}
		}else{
			for(size_t d = 2; d < pnt->dim();d++){
				if((this->dimMin[d] != this->dimMax[d] && this->dim <= d) ){
					float material[] = {(float)axis[d][0],(float)axis[d][1],(float)axis[d][2],1.0};
					glPushName(d);
					drawSlider(this->dimMin[d],this->dimMax[d],this->dimVal[d],(*pnt)[d],0.4,0.9,-1.15 + (d * .10), material);
					glPopName();//end d
				}
			}

		}

	}

	glDisable(GL_LIGHTING);
	glPushMatrix();
	glPushName(1);
	glPushName(5);
		if(this->showLesserDim){
			glPushName(0);
			glColor4f(0.9,1,0.9,.4);
			glBegin(GL_QUADS);
				glVertex3f(-.94,.50,0);
				glVertex3f(-.79,.50,0);
				glVertex3f(-.79,.46,0);
				glVertex3f(-.94,.46,0);
			glEnd();

			for(size_t x = 0;x < this->curGraph->getDim();x++){
				if(this->lesserDimindex == x ){
					glColor4f(0.5,1,0.5,1);
				}
				glLoadName(x+1);
				glBegin(GL_QUADS);
					glVertex3f(-.94 + (.04 * x),.44,0);
					glVertex3f(-.91 + (.04 * x),.44,0);
					glVertex3f(-.91 + (.04 * x),.40,0);
					glVertex3f(-.94 + (.04 * x),.40,0);
				glEnd();
				glColor4f(.5,.5,.5,1);
				stringstream ss;
				ss << x;
				drawString(-.93 + (.04 * x),.4 ,0.0,ss.str());
				glColor4f(0.9,1,0.9,.4);
			}
			glPopName();
			glColor4f(.5,.5,.5,1);
			drawString(-.94,.46 ,0.0,"{View dim}");


		}else{
			glPushName(0);
			glColor4f(0.9,1,0.9,.4);
			glBegin(GL_QUADS);
				glVertex3f(-.93,.50,0);
				glVertex3f(-.78,.50,0);
				glVertex3f(-.78,.46,0);
				glVertex3f(-.93,.46,0);
			glEnd();
			glPopName(); //end of 0

			glColor3f(.5,.5,.5);
			drawString(-.93,.46 ,0.0,"View dim");


		}
		glColor4f(1,1,1,1);
	glPopName();//end of 1-5
	glPopName();//end of 1

	glPopMatrix();

	double b[2] = {-0.93,.30};
	glPushName(1);
	double pos = b[1]-.07;//.9;
	if(this->showBound){
		glPushName(0);
			glColor4f(1,1,1,.3);
			glBegin(GL_QUADS);
			glVertex3f(b[0],b[1],0);
			glVertex3f(b[0]+.15,b[1],0);
			glVertex3f(b[0]+.15,b[1]-.04,0);
			glVertex3f(b[0],b[1]-.04,0);
			glEnd();
			glColor4f(.5,.5,.5,1);
		drawString(b[0]-.01,b[1]-.02 ,0.0,"[Boundaries]");
		glPopName(); // end 1-0
		glPushName(1);
			glBegin(GL_TRIANGLES);
			glVertex3f(b[0]-.02,b[1]-.04,0);
			glVertex3f(b[0]-.04,b[1],0);
			glVertex3f(b[0]-.06,b[1]-.04,0);
			glEnd();
		glPopName();//end 1-1
		glPushName(2);
			glBegin(GL_TRIANGLES);
			glVertex3f(b[0]+.17,b[1],0);
			glVertex3f(b[0]+.19,b[1]-.04,0);
			glVertex3f(b[0]+.21,b[1],0);
			glEnd();
		glPopName();//end 1-2


	}else{ // b[2] = {-0.93,.30};
		glPushName(0);
			glColor4f(1,1,1,.3);
			glBegin(GL_QUADS);
			glVertex3f(b[0],b[1],0);
			glVertex3f(b[0]+.15,b[1],0);
			glVertex3f(b[0]+.15,b[1]-.04,0);
			glVertex3f(b[0],b[1]-.04,0);
			glEnd();
			glColor4f(.5,.5,.5,1);
		drawString(b[0],b[1]-.02 ,0.0,"Boundaries");
		glPopName(); // end 1-0
	}
	glPopName();
	if(this->showBound || this->showLesserDim){
		bool trans;
		glPushName(1);
		if(this->showLesserDim){
			glPushName(6);
			glPushName(6);
			trans = this->transDim;
		}else{
			glPushName(3);
			trans = this->transBound;
		}
			glColor4f(1,1,1,.3);
			glBegin(GL_QUADS);
			glVertex3f(b[0],b[1] + .05,0);
			glVertex3f(b[0] + .15,b[1] + .05,0);
			glVertex3f(b[0] + .15,b[1] + .01,0);
			glVertex3f(b[0],b[1] + .01,0);
			glEnd();
			glColor4f(.5,.5,.5,1);
			if(trans){
				drawString(b[0]-.01,b[1]+.03 ,0.0,"[Transparent]");
			}else{
				drawString(b[0],b[1]+.03 ,0.0,"Transparent");
			}

		if(this->showLesserDim){
			glPopName();//end 1-5-5
		}
		glPopName();//end 1-3
		glPopName();// end 1
	}
	size_t ind = 1;
	if(!this->showLesserDim){
		for(map<int,GLfloat>::iterator iter = this->boundaries.begin(); iter != this->boundaries.end(); iter++) {
			float col[3] = {0,0,0};
			Utils::huetoColor( iter->second, col);
			glColor3f(col[0],col[1],col[2]);
			stringstream ss;
			string str;
			ss << iter->first;
			if(ind == this->boundaryIndex) {
				ss << "<-<";
			}
			str = ss.str();
			ss.str("");
			if(ind % 20 == 0){
				 pos = b[1]-.07;
			}
			if(showBoundIndices)
				drawString(b[0] + .08 * int(ind / 20),pos ,0.0,str);
			pos -= .03;
			ind++;
		}
	}else{

		drawString(b[0]+ .03,pos ,0.0,"lesser Dim nodes");
		pos -= .03;
		for(list<pair<size_t,double> >::iterator iter = this->dimcol.begin(); iter != this->dimcol.end(); iter++) {
			float col[3] = {0,0,0};
			Utils::huetoColor( iter->second, col);
			glColor3f(col[0],col[1],col[2]);
			stringstream ss;
			string str;
			ss << iter->first;
			str = ss.str();
			ss.str("");
			if(ind % 20 == 0){
				 pos = b[1]-.10;
			}
			drawString(b[0] + .08 * int(ind / 20),pos ,0.0,str);
			
			pos -= .03;
			ind++;
		}
	}

	glPushName(1);
	glPushName(4);

	glPushName(0);
	glPushMatrix();
	glTranslatef(-.1,-.8,0);
	glColor3f(0,1,0);
	if(this->t == 0){
		glScalef(.07,.07,.07);
	}else{
		glScalef(.05,.05,.05);
	}
		glutSolidCube(1.0);  //green square to show that option is selected
	glPopMatrix();

	glLoadName(1);//end 1-4-0
	glPushMatrix();
	glTranslatef(-.0,-.8,0);
	glColor3f(1,0,0);
	if(this->t == 1){
		glScalef(.07,.07,.07);
	}else{
		glScalef(.05,.05,.05);
	}
		glutSolidCube(1.0);  //red square to show that option is selected
	glPopMatrix();

	glLoadName(2);//end 4-1
	glPushMatrix();
	glTranslatef(.1,-.8,0);
	glColor3f(0,0,1);
	if(this->t == 2){
		glScalef(.07,.07,.07);
	}else{
		glScalef(.05,.05,.05);
	}
		glutSolidCube(1.0);  //blue square to show that option is selected
	glPopMatrix();
	if(this->showCollision) {
		glLoadName(3);//end 4-1
		glPushMatrix();
		glTranslatef(0,-.93,0);
		glColor3f(1,0,1);
		if(this->t == 3){
			glScalef(.07,.07,.07);
		}else{
			glScalef(.05,.05,.05);
		}
		glutSolidCube(1.0);  //For Bad Angle
		glPopMatrix();


		glLoadName(4);//end 4-1
		glPushMatrix();
		glTranslatef(.1,-.93,0);
		glColor3f(0,1,1);
		if(this->t == 4){
			glScalef(.07,.07,.07);
		}else{
			glScalef(.05,.05,.05);
		}
		glutSolidCube(1.0);  //For Distance Constraint
		glPopMatrix();

	}
	glPopName();//end 1-4-t
	glPopName();//end 1-4
	glPopName();//end 1
	glColor3f(.5,.5,.5);
	string hnt[] = {"Good","Violation","All", "Steric Violation", "Angle Violation"};
	stringstream ss;
	ss << this->t;
	string st = ss.str();
	if(t<5)  //quick fix
		st = hnt[this->t];
	drawString(-.1,-.99,0, st);



	glEnable(GL_LIGHTING);

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();
	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();
    glEnable(GL_DEPTH_TEST);

}

void CayleySpaceView::drawSlider(double valMin,double valMax,double val,double hilight,double xLeft, double xRight, double yMid, float material[4]){//drawing slider
	//TO SHOW IN SQUARE

	glDisable(GL_LIGHTING);
	double wslideloc[][2] = {{xLeft, yMid}, {xRight, yMid}};
	float material2[] = {(float)(material[0] * .7), (float)(material[1] * .7),(float)(material[2] * .7),(float)material[3]};
	float material3[] = {(material[0]) / 2, (material[1]) / 2, (material[2]) / 2,material[3]};


	glPushName(0);
	glColor4fv(material2);
	glBegin(GL_TRIANGLES);
	glVertex3f(wslideloc[0][0],wslideloc[1][1] + .02,0);
	glVertex3f(wslideloc[0][0] - 0.05,wslideloc[1][1],0);
	glVertex3f(wslideloc[0][0],wslideloc[1][1] - .02,0);
	glEnd();

	glLoadName(1);//end 0
	glBegin(GL_TRIANGLES);
	glVertex3f(wslideloc[1][0],wslideloc[1][1] + .02,0);
	glVertex3f(wslideloc[1][0] + 0.05,wslideloc[1][1],0);
	glVertex3f(wslideloc[1][0],wslideloc[1][1] - .02,0);
	glEnd();//end 1
	glPopName();

	//draw bar
	glPushMatrix();
	glTranslatef((wslideloc[0][0] + wslideloc[1][0]) / 2.0, wslideloc[1][1],0.0);
	glScalef((wslideloc[1][0] - wslideloc[0][0])/ 0.01 ,1,1);
	glColor4fv(material2);
	glutSolidCube(.01);
	glPopMatrix();

	if(valMax >valMin){
		//draw hilight
		if(hilight >= valMin && hilight <= valMax){
			glPushMatrix();
			double hiLx = (hilight - valMin)/(valMax - valMin);
			hiLx = (hiLx * (wslideloc[1][0] - wslideloc[0][0])) + wslideloc[0][0];
			glTranslatef(hiLx, wslideloc[1][1],0.0);
			glScalef(0.2 ,.2,1);
			glColor4fv(material3);
			glutSolidCube(.075);
			glPopMatrix();
		}

		//draw marker
		glPushMatrix();
		double posx = (val - valMin)/(valMax - valMin);
		posx = (posx * (wslideloc[1][0] - wslideloc[0][0])) + wslideloc[0][0];
		glTranslatef(posx, wslideloc[1][1],0.0);
		glScalef(0.1 ,.5,1);
		glColor4fv(material);
		glutSolidCube(.1);
		glPopMatrix();
	}


	glColor3f(.7,.7,0);
	stringstream ss;
	string str;
	ss << valMin;
	str = ss.str();
	ss.str("");
	drawString(wslideloc[0][0],wslideloc[1][1] + .03 ,0.0,str);
	ss << valMax;
	str = ss.str();
	ss.str("");
	drawString(wslideloc[1][0],wslideloc[1][1] + .03 ,0.0,str);
	ss << val;
	str = ss.str();
	ss.str("");
	drawString( (wslideloc[0][0] + wslideloc[1][0]) / 2,wslideloc[1][1] + .03 ,0.0,str);

	glEnable(GL_LIGHTING);
}


void CayleySpaceView::addNames(int* names, vector<pair<int,int> > links,int &left, int &right){

	bool l,r;

	for(size_t iter = 0;iter < links.size();iter++)
	{
		l = r = false;
		for(int i  = 0; i<6 && ( !l || !r );i++) {
			if(links[iter].second == names[i * 2]){
				r = true;
			}
			if(links[iter].first == names[(i * 2) + 1]){
				l = true;
			}
		}
		if(!r){
			names[right] = links[iter].second;
			right += 2;
		}
		if(!l){
			names[left] = links[iter].first;
			left += 2;
		}

	}
}


void CayleySpaceView::display(bool select){

	if(this->currentSpace.empty()){
		return;
	}
	glPushMatrix();

	this->select = select;
	double alpha = 1.0;
	if(this->showLesserDim){
		this->witness = false;
		for(size_t iter = 0;iter < this->lesserSpace.size();iter++){
			CayleyPoint* a = this->lesserSpace[iter];

			glPushName(0);
			bool dimcheck = true;
			if(this->allSlider){
				for(size_t d = this->dim; d< 6;d++){
					if( !(abs(this->dimVal[d] - (*a)[d]) < this->step / 2.0 )){
						dimcheck = false;
					}
				}

			}else{
				for(size_t d = this->dim; d< a->dim();d++){
					if( !(abs(this->dimVal[d] - (*a)[d]) < this->step / 2.0 )){
						dimcheck = false;
					}
				}
			}



			bool jacobian_dimcheck = true;
			if(this->projectOnOtherSpace  ) //&& this->dim==2
			{
				for(size_t d = this->dim; d< this->curGraph->getDim(); d++)
				{
					if( !(abs(this->dimVal[d] - (*a)[d]) < Settings::Sampling::gridSteps[d] / 2.0 )){
						jacobian_dimcheck = false;
					}
				}
			}

			if(dimcheck || (this->projectOnOtherSpace && jacobian_dimcheck) ){ //if this->projectOnOtherSpace, then do not just show points satisfying specific phi,theta, psi values. show all of them by projection on xyz
				float col[3];
				Utils::huetoColor(this->lesserColor[iter].second,col);
				drawPoint(a,iter,col,.99);
			}

			glPopName();

		}
		alpha = 0.3;

	}
	if( (! this->showLesserDim) || (this->showLesserDim && this->transDim) ){
		size_t index = 0;

		this->witness = true;
		for(size_t iter = 0;iter < this->currentSpace.size();iter++){
			CayleyPoint* a = this->currentSpace[iter];
			
			//TODO: This is a temporary fix. We need to figure out where the memory corruption is happening.
			if(a->dim() > 6)
				continue;
			

			glPushName(0);
			bool dimcheck = true;

			if(this->allSlider){
				for(size_t d = this->dim; d< 6;d++){
					if( !(abs(this->dimVal[d] - (*a)[d]) < this->step / 2.0 )){
						dimcheck = false;
					}
				}

			}else{
				for(size_t d = this->dim; d< a->dim();d++){
					if( !(abs(this->dimVal[d] - (*a)[d]) < this->step / 2.0 )){
						dimcheck = false;
					}
				}
			}

			bool jacobian_dimcheck = true;
			if(this->projectOnOtherSpace  ) //&& this->dim==2
			{
				for(size_t d = this->dim; d< this->curGraph->getDim(); d++)
				{
					if( !(abs(this->dimVal[d] - (*a)[d]) < Settings::Sampling::gridSteps[d] / 2.0 )){
						jacobian_dimcheck = false;
					}
				}
			}



			if(dimcheck || (this->projectOnOtherSpace && jacobian_dimcheck)){ //if this->projectOnOtherSpace, then do not just show points satisfying specific phi,theta, psi values. show all of them by projection on xyz
					if(this->showBound){
						if(! a->getBoundaries().empty()){
							float col[3];
							list<int>bounds = a->getBoundaries();
							if(this->boundaryIndex == 0){
								Utils::huetoColor(this->boundaries[a->getBoundaries().front()],col);
								drawPoint(a,index,col,alpha);
							}else if(find(bounds.begin(),bounds.end(),this->boundList[this->boundaryIndex-1]) != bounds.end()){
								Utils::huetoColor(this->boundaries[this->boundList[this->boundaryIndex-1]],col);
								drawPoint(a,index,col,alpha);
							}else if(this->transBound){
								Utils::huetoColor(this->boundaries[a->getBoundaries().front()],col);
								drawPoint(a,index,col,alpha * 0.2);
							}
						}
					}else{
						float col[3] = {0,0,0};
						drawPoint(a,index,col,alpha);
					}

			}

			glPopName();

			index += 1;
		}



	}
	glPopMatrix();



	glLineWidth(1);
}



void CayleySpaceView::drawPoint(CayleyPoint *a,int index,float colr[3],double alpha){
    double halfStep = this->step / 2.0;

    double col[3];
    if(colr[0] == 0 && colr[1] == 0 && colr[2] == 0){
    	this->drawNormPoint(a,index,alpha);
    	return;
    }else{   // in case of show boundary
    	col[0] = colr[0];
    	col[1] = colr[1];
    	col[2] = colr[2];
    }

    if(alpha == 1)glPushName(index);
    if(index != this->activeIndex){

		float material[] = {(float)col[0],(float)col[1],(float)col[2],(float)alpha};
		glMaterialfv(GL_FRONT, GL_AMBIENT_AND_DIFFUSE, material);
    }else{
		float material[] = {(float)((col[0] + 1)/2.0),(float)((col[1] + 1)/2.0),(float)((col[2] + 1)/2.0),(float)alpha};
		glMaterialfv(GL_FRONT, GL_AMBIENT_AND_DIFFUSE, material);
    }
    glPushMatrix();
		glTranslatef((*a)[0],(*a)[1],(*a)[2]);
		if(index < this->witSize && this->witness){
			glScalef(halfStep * .8,halfStep * .8,halfStep * .8);
			glutSolidIcosahedron();
		}else{
			glutSolidCube(halfStep * 1.7);
		}
    glPopMatrix();
    if(alpha == 1)glPopName();//end index
}




/* 
 * if green box is selected (t=0), then it will display all green points 
 * if red box is selected (t=1), then it will display red points 
 * if blue box is selected (t=2), then it will display all green, red and blue points
 * if purple box is selected (t=3), then it will display all purple points
 * if cyan box is selected (t=4), then it will display all cyan points
 * blue=nonrealizable, red=constraint violated, purple=collision, cyan=bad angle, green=feasible
 */
bool CayleySpaceView::getColort(CayleyPoint *a, float out[3]) {

	bool found = true;
	if(a->hasOrientation() && (this->t ==0 || this->t == 2) ) //feasible    // display the feasible ones in all selections
	{
        out[0] = 0.0;
        out[1] = 1.0;
        out[2] = 0.0;
    }
	else if ( (this->t==1 || this->t==2 ) && ( a->getCollidN()>0 || a->getBadAngleN()>0 ) )  //violation
	{
		out[0] = 1.0;
		out[1] = 0.0;
		out[2] = 0.0;
    }
	else if ( this->t==2 && !a->isRealizable() ) //VOLUME NEGATIVE
	{
		out[0] = 0.0;
		out[1] = 0.0;
		out[2] = 1.0;
	}
	else if ( this->t==3 && a->getCollidN()>0 )   //steric violation (purple)
	{
		out[0] = 1.0;
		out[1] = 0.0;
		out[2] = 1.0;
    }
	else if ( this->t==4 && a->getBadAngleN()>0 ) //angle violation (cyan)
	{
		out[0] = 0.0;
		out[1] = 1.0;
		out[2] = 1.0;
    }
	else
		found = false;

	return found;

}

void CayleySpaceView::drawNormPoint(CayleyPoint *a,int index,double alpha){

    double halfStep = this->step / 2.0;
    float col[3];

   	bool found = getColort(a, col);

	if( found )
	{

    	  if(alpha == 1)glPushName(index);
          if(index != this->activeIndex){
			  float material[] = {(float)col[0],(float)col[1],(float)col[2],(float) alpha};
			  glMaterialfv(GL_FRONT, GL_AMBIENT_AND_DIFFUSE, material);
          }else{
              float material[] = {(float)((col[0] + 1)/2.0),(float)((col[1] + 1)/2.0),(float)((col[2] + 1)/2.0),(float)alpha};
              glMaterialfv(GL_FRONT, GL_AMBIENT_AND_DIFFUSE, material);
          }

          glPushMatrix();
			  glTranslatef((*a)[0],(*a)[1],(*a)[2]); //IF LATER ON YOU WANT TO PROJECT ON SPECIFIC AXES, THEN CHANGE HERE 0,1,2 TO 3,4,5 etc.
			  glLineWidth(0.01);
			  glutWireCube(halfStep * .8 );
		   glPopMatrix();


          if(alpha == 1)glPopName();//end index

	 }

}




void CayleySpaceView::takePick(vector<int> hits){
	if(hits.size() > 1)
	{
		switch(hits[0])
		{
			case 0://point selected
				{
					this->setActive(hits[1]);
				}
			break;

			case 1://HUD
			{
				switch(hits[1]){
					case 0:
						this->toggleBoundaries();
					break;
					case 1:
						this->incBoundaries(-1);
					break;
					case 2:
						this->incBoundaries(1);
					break;
					case 3:
						this->toggleTransBoundaries();
					break;
					case 4:// tetra markers
						this->t = hits[2];
						if(hits[2] == 1) {
							if(!getCollision()) {
								toggleCollision();
							}
						} else if(hits[2] != 3 && hits[2] !=4) {
							if(getCollision()) {
								toggleCollision();
							}
						}
					break;
					case 5://lesser dim
						if(hits[2] == 0){
							this->toggleLesserDim();
						} else {
							this->setLesserDim(hits[2] - 1);
						}
					break;
					case 6://Transparent
						if(hits[2] == 6){
							this->transDim = ! this->transDim;
						}
					break;
				}
			}
			break;

			// slider cases
			case 2:
			case 3:
			case 4:
			case 5:
			{
				if(hits[1] == 0) {
					if(this->projectOnOtherSpace  )
						this->incDim(hits[0],-1 * Settings::Sampling::gridSteps[hits[0]] );
					else
						this->incDim(hits[0],-1 * this->step);
				} else {
					if(this->projectOnOtherSpace  )
						this->incDim(hits[0], Settings::Sampling::gridSteps[hits[0]] );
					else
						this->incDim(hits[0], this->step);
				}
			}
			break;

		}
	}
}

double* CayleySpaceView::getXYZmid(){
	return this->xyzMid;
}

void CayleySpaceView::setActive(size_t active){
	if(active < this->currentSpace.size()){
		this->activeIndex = active;
	}else{
		this->activeIndex = 0;
	}
}

CayleyPoint* CayleySpaceView::getActive(){
	if(this->currentSpace.empty()){
		return this->blankpoint;
	}
	return this->currentSpace[activeIndex];
}

int CayleySpaceView::getActiveIndex(){
	return this->activeIndex;
}

vector<CayleyPoint*> CayleySpaceView::getSpace(){
	return this->currentSpace;
}

void CayleySpaceView::setViewdim(int ndim){
	if(ndim == 6){
		this->allSlider = true;
		this->dim = 3;
	}else{
		this->allSlider = false;
		this->dim = ndim;
		this->dimVal[2] = this->dimMin[2];
	}

}


void CayleySpaceView::incDim(size_t dim,double inc){
	this->dimVal[dim] += inc;
	if(this->dimVal[dim] < this->dimMin[dim]){
		this->dimVal[dim]= this->dimMin[dim];
	}

	if(this->dimVal[dim] > this->dimMax[dim]){
		this->dimVal[dim] = this->dimMax[dim];
	}
}

void CayleySpaceView::toggleBoundaries(){
	this->showBound = ! this->showBound;
}

void CayleySpaceView::toggleCollision(){
	this->showCollision = ! this->showCollision;
}

bool CayleySpaceView::getCollision() {
	return showCollision;
}

void CayleySpaceView::toggleTransBoundaries(){
	this->transBound = ! this->transBound;
}

void CayleySpaceView::incBoundaries(short inc){
	if(this->boundaryIndex == 0 && inc < 0){
		this->boundaryIndex = this->boundList.size();
	}else{
		this->boundaryIndex += inc;
	}
	if(this->boundaryIndex > this->boundList.size()){
		this->boundaryIndex = 0;
	}
}

void CayleySpaceView::toggleLesserDim(){
	this->showLesserDim =  !(this->showLesserDim);
	if(this->showLesserDim){
		cout << "(lesser dim true)" <<endl;
		if(this->lesserSpace.empty()){
			this->setLesserDim(this->lesserDimindex);
		}
	}else{
		cout << "(lesser dim false)" <<endl;
		this->lesserSpace.clear();
		this->lesserColor.clear();
	}
}

void CayleySpaceView::setLesserDim(size_t setting){
	this->lesserDimindex = setting;
	if(this->showLesserDim){
		this->lesserSpace.clear();
		this->lesserColor.clear();
		list<size_t> kid = this->atlas->getChildren( this->rnode->getID(), this->lesserDimindex);
		size_t index =0;
		for(list<size_t>::iterator it = kid.begin(); it != kid.end(); it++){
			double hue = (double)index / (double)kid.size();
			AtlasNode * temprnod = (*this->atlas)[*it];
			ActiveConstraintGraph* tempgraph =  temprnod->getCG();
			ActiveConstraintRegion* tempregion =  temprnod->getACR();
			this->snl->loadNode(*it, tempregion);
			vector<CayleyPoint*> tempspace = this->region->convertSpace( tempregion, this->curGraph->getParamLines(), this->curGraph->getMolecularUnitA(), this->curGraph->getMolecularUnitB() );
			cout << "for dimension " << this->lesserDimindex << " children " << *it << " has " << tempspace.size() << " points" << endl;
			this->lesserSpace.insert(this->lesserSpace.end(),tempspace.begin(),tempspace.end());
			this->lesserColor.insert(lesserColor.end(),tempspace.size(),make_pair(*it,hue));

			tempregion->trim(); //22APRIL2013
			index++;
		}
		for(size_t iter = 0; iter < this->lesserSpace.size();iter++){
			CayleyPoint* pnt = this->lesserSpace[iter];
			for(size_t d = 0; d<6;d++){
				if((*pnt)[d] > this->dimMax[d]){
					this->dimMax[d] = (*pnt)[d];
				}else if((*pnt)[d] < this->dimMin[d]){
					this->dimMin[d] = (*pnt)[d];
				}
			}
		}
		this->dimcol.assign(this->lesserColor.begin(),this->lesserColor.end());
		this->dimcol.sort();
		this->dimcol.unique();
	}

}



vector<int> CayleySpaceView::getBoundList(){
	return this->boundList;
}


void CayleySpaceView::setGridView()
{
	this->projectOnOtherSpace = !this->projectOnOtherSpace;
	setSpace(this->rnode);
}




vector<CayleyPoint*> CayleySpaceView::projectSpace(vector<CayleyPoint*> spc) {
	vector<CayleyPoint*> projectionSpc;

	vector<Atom*> helB = this->curGraph->getMolecularUnitB()->getAtoms();

	vector<CayleyPoint*>::iterator its;
	for(its = spc.begin(); its != spc.end(); its++) {
		vector<Orientation*> sol = (*its)->getOrientations();
		for(size_t a = 0; a < sol.size(); a++) {
			vector<double> pos; 
			double fb[3][3], tb[3][3];
			sol[a]->getFromTo(fb,tb); 
			
			Vector3d p1(fb[0][0], fb[0][1], fb[0][2] );
			Vector3d p2(fb[1][0], fb[1][1], fb[1][2] );
			Vector3d p3(fb[2][0], fb[2][1], fb[2][2] ); 
			
			Vector3d P1(tb[0][0], tb[0][1], tb[0][2] );
			Vector3d P2(tb[1][0], tb[1][1], tb[1][2] );
			Vector3d P3(tb[2][0], tb[2][1], tb[2][2] ); 
			
			Vector3d v1 = p2-p1;
			Vector3d v2 = p3-p1;
			Vector3d v3 = v1.cross(v2); 
			
			Vector3d V1 = P2-P1;
			Vector3d V2 = P3-P1;
			Vector3d V3 = V1.cross(V2); 
			
			Matrix3d m, M, R;
			m << v1(0), v2(0), v3(0),   v1(1), v2(1), v3(1),  v1(2), v2(2), v3(2)  ;
			M << V1(0), V2(0), V3(0),   V1(1), V2(1), V3(1),  V1(2), V2(2), V3(2)  ; 
			
			R = M * m.inverse(); //rotation matrix
			Vector3d t = P1 - R * p1;  //translation 
			
			Quaterniond q(R); 
			
			//compute mean
			Vector3d mean(0,0,0);
			for(size_t iter = 0;   iter < helB.size();  iter++) {
				double *l =  helB[iter]->getLocation();
				Vector3d p(l[0], l[1], l[2]);
				mean += R*p;
			}
			mean = mean /  helB.size(); 
			
			Vector3d TB = t + mean;
			Matrix3d RB = quaterniontoRotation( q.w(), q.x(),  q.y(), q.z() );


			Vector3d eaB =  Utils::RotMatToEuler(R);
			eaB[0] = eaB[0] * 180 / PI;
			eaB[2] = eaB[2] * 180 / PI;

			if(this->curGraph->independent_directions.size() > 0)
				for(vector<int >::iterator iter = this->curGraph->independent_directions.begin();iter != this->curGraph->independent_directions.end();iter++) {
					if( (*iter) < 3)
						pos.push_back( TB( *iter) );
					else
						pos.push_back( eaB( *iter - 3) );

			} else {
				pos.push_back(TB(0));   pos.push_back(TB(1));   pos.push_back(TB(2));
				pos.push_back(eaB(0));   pos.push_back(eaB(1));   pos.push_back(eaB(2));
			}

			CayleyPoint * output = new CayleyPoint(pos);
			output->setRealizable( (*its)->isRealizable() );
			output->setBadAngleN( (*its)->getBadAngleN() );
			output->setCollidN( (*its)->getCollidN() );
			output->axis = (*its)->axis;
			output->zIndex = (*its)->zIndex;
			output->setOrientations( (*its)->getOrientations()  );

			projectionSpc.push_back(output);
		}
	}

	return projectionSpc;

}


Matrix3d CayleySpaceView::quaterniontoRotation(double q0, double q1, double q2, double q3 ){

	Matrix3d R;
	R <<  1 - 2*q2*q2 - 2*q3*q3,    2*q1*q2 + 2*q0*q3,     2*q1*q3 - 2*q0*q2,
	2*q1*q2-2*q0*q3,        1 - 2*q1*q1 - 2*q3*q3,   2*q2*q3 + 2*q0*q1,
	2*q1*q3 + 2*q0*q2,       2*q2*q3 - 2*q0*q1,    1 - 2*q1*q1 - 2*q2*q2;
	return R;
}
