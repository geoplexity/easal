/*
 This file is part of EASAL. 

 EASAL is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 EASAL is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * SweepView.cpp
 *
 *  Created on: Jun 9, 2009
 *      Author: James Pence
 */

#include "SweepView.h"

using namespace std;


static int SweephudId = 5;


float mat_white[4]={1.0, 1.0, 1.0, 1.0};
float mat_red[4]={1.0, 0.0, 0.0, 1.0};
float mat_green[4]={0.0, 1.0, 0.0, 1.0};
float mat_blue[4]={0.0, 0.0, 1.0, 1.0};

SweepView::SweepView(MolecularUnit* helA,MolecularUnit* helB) {
	this->helA = new MolecularUnit(helA->getAtoms());// making copies of the helices in order to keep them separate from the model.
	this->helB = new MolecularUnit(helB->getAtoms());
	this->helA->stree = helA->stree;	// copy the sphere tree as well
	this->helB->stree = helB->stree;
	this->dirtyBit =true;
	this->multi = 0;
	this->showStree = false;
	this->showCorrect = false;
	this->showlines = true;
	this->showNeighbour = false;

	this->showInterior = false;

}

SweepView::~SweepView() {
	delete this->helA;
	delete this->helB;
}


void SweepView::setSpace(int active, AtlasNode* rnod, vector<int> boundlist){
	this->dirtyBit =true;
	this->currentID =  rnod->getCG();
	this->region = rnod->getACR();
	vector<CayleyPoint*> space = this->region->getSpace();
	if(this->helA == NULL && !this->currentID->getMolecularUnitA()->empty()){
		this->helA = new MolecularUnit(this->currentID->getMolecularUnitA()->getAtoms());
		this->helB = new MolecularUnit(this->currentID->getMolecularUnitB()->getAtoms());
		this->helA->stree = this->currentID->getMolecularUnitA()->stree;	// copy the sphere tree as well
		this->helB->stree = this->currentID->getMolecularUnitB()->stree;

	}


	this->currentSpace.clear();
	if(!space.empty()){
		this->currentSpace.assign(space.begin(),space.end());

		if(! boundlist.empty()){
			this->boundaries.clear();
			this->curBoundlist.assign(boundlist.begin(),boundlist.end());
			double amt = 0;
			double inc = 1.0 / double(boundlist.size());

			for(vector<int>::iterator it = boundlist.begin();it != boundlist.end(); it++){
				this->boundaries[*it] = amt;
				amt += inc;
			}
		}
	}
	this->active = active;
	this->curIndex=0;
	this->showAll = false;
	this->showBound = false;
	this->showInterior = false;
	this->flipFilter = 8;//normal showall

	this->displayBoundary = -1;
	this->boundex = 0;

	time(&this->lastTime);

	this->fpscount = 0;
	this->framecount = 0;

	this->delay = .5;
	this->showControls = false;
	this->play = false;
	this->rewind = false;
	this->pause = false;
	this->trackflip = false;


	this->spaceSize = this->currentSpace.size();// caching for faster access

}

void SweepView::drawString(double x, double y, double z, string strn)
{
   glRasterPos3f(x, y, z);
   int row = 0;
   for (string::size_type c = 0; c < strn.length(); c++)
   {
	   if(strn[c] == '\n'){
		   row++;
		   glRasterPos3f(x, y - (row * .04), z);
	   }
      glutBitmapCharacter(GLUT_BITMAP_TIMES_ROMAN_10, strn[c]); // Updates the position
   }
}

void SweepView::display(bool wire){
			time_t curtime;
			time(&curtime); // Keeping tabs on the FPS so that the display doesn't go too fast or slow independent of system performance
			if(difftime(curtime,this->lastTime) == 1){
				this->framecount++;
				this->fpscount++;
				fps=this->fpscount;
				this->fpscount = 0;
			}else{
				this->fpscount++;
				this->framecount++;
			}
			this->lastTime = curtime;
	if(this->currentSpace.empty() || this->active < 0 || this->active >= spaceSize)return; // returning if nothing to display.
	if( !this->currentSpace[this->active]->hasOrientation() ) return; // returning if nothing to display.
	if(this->helA == NULL)return; // returning if nothing to display.

	if(this->dirtyBit == false){
		this->drawBuffer();
		return;
	}
	this->dirtyBit = false;
	this->viewBalls.clear();
	this->viewLines.clear();
	if(this->showControls){//do the movie stuff
		if((this->play || this->rewind) && ! this->pause){
			if(this->framecount > size_t(this->delay * fps)){
				this->framecount = 0;
				do{
					if(this->play){ //Play button is active
						if(this->playIndex < spaceSize){
							this->playIndex++;
						}
						if(this->playIndex == spaceSize){
							this->playIndex = 0;
						}
					}else{//Rewind button is active
						if(this->playIndex > 0){
							this->playIndex--;
						}
						if(this->playIndex == 0){
							this->playIndex = spaceSize - 1;
						}

					}
				}while( !this->currentSpace[this->playIndex]->hasOrientation() ); // skip empty points

			}
		}


			this->displayPoint(this->currentSpace[this->playIndex],-2,this->playflip,false);
			this->drawBuffer();
			this->dirtyBit = true;


	}else if(this->showAll){

		bool first = true;
				for(size_t sIter = 0; sIter < this->spaceSize;sIter++)
				{
					vector<Orientation*> sol = this->currentSpace[sIter]->getOrientations();


					for(size_t iter = 0;iter < sol.size();iter++){
						double fb[3][3], tb[3][3];
						sol[iter]->getFromTo(fb, tb);
						if(sol[iter]->getFlipNum() == this->flipFilter || this->flipFilter == 8){
							bool show =true;
							float mat_b[4] = {mat_blue[0],mat_blue[1],mat_blue[2],mat_blue[3]};
							float mat_a[4] = {mat_green[0],mat_green[1],mat_green[2],mat_green[3]};
							if(this->showBound){
								mat_b[0] = mat_b[1] = mat_b[2] = 0.4;
								if(! sol[iter]->getBoundary().empty() ){
									Utils::huetoColor(this->boundaries[sol[iter]->getBoundary()[0]],mat_a);
								}else{
									show = false;
								}

								for(int i=0; i<sol[iter]->getBoundary().size() && this->boundex != 0; i++) //i guess boundex=0 is to show all boundaries
								{
									if(sol[iter]->getBoundary()[i] == this->curBoundlist[this->boundex-1] ){
										break;
									}
									if(i == sol[iter]->getBoundary().size()-1)//if non of the boundaries equal to that chosen boundary
										show = false;
								}
							}


							if(!this->showBound && this->showInterior && !sol[iter]->getBoundary().empty() )
								show = false;


							if(show  ){
								if(first){
									this->setViewCenter(this->helA,Utils::getIdentityMatrix());
									this->render(this->helA,Utils::getIdentityMatrix(),mat_a,false,wire);
									vector<double> trans_b = Utils::getTransMatrix(fb,tb);
									this->render(this->helB, trans_b, mat_b, true, wire);
									this->renderCorrect();


								}else{

								}
							}
						}
					}
				}
	}else{
		vector<Orientation*> sol = this->currentSpace[this->active]->getOrientations();
		if(sol[this->curIndex] == NULL)
				return;
		this->playflip = sol[this->curIndex]->getFlipNum();
		double fb[3][3],  tb[3][3];
		sol[this->curIndex]->getFromTo(fb, tb);

		this->setViewCenter(this->helA,Utils::getIdentityMatrix());
		this->render(this->helA,Utils::getIdentityMatrix(),mat_green,false,wire);
		vector<double> trans_b = Utils::getTransMatrix(fb,tb);
		this->render(this->helB, trans_b, mat_blue, true, wire);
		this->renderCorrect();

		this->xfHelA = this->helA->getXFAtoms(Utils::getIdentityMatrix());
		this->xfHelB = this->helB->getXFAtoms(trans_b);  //Utils::getSingleTransformFromTwo(fa,ta,fb,tb));
		drawLines();
		findDistortions();
		vector<Atom*>::iterator it2;
		for(it2 = xfHelA.begin();it2 != xfHelA.end(); it2++){
			delete (*it2);
		}
		for(it2 = xfHelB.begin();it2 != xfHelB.end(); it2++){
			delete (*it2);
		}
		xfHelA.clear();
		xfHelB.clear();
	}

}

void SweepView::displayPoint(CayleyPoint *pnt, int bound, size_t &flip,bool wire){
	vector<Orientation*> sol = pnt->getOrientations();
	int dex = -1, dexb = -1;
	for(size_t a = 0; a < sol.size(); a++){
		if(bound == -2 || sol[a]->getBoundary()[0] == bound ){ // checking to maintain boundary if possible. -2 means its unimportant.
			dexb = a;
			if(sol[a]->getFlipNum() == flip){ // checking to maintain flip number if possible.
				dex = a;
				break;
			}
		}
	}

	if(dex == -1 && this->trackflip )
		return;

	if(dex == -1  ){ //Ensuring same flip number is more important that maintaining boundary.
		if(dexb == -1){
			dex = 0;
			flip = sol.front()->getFlipNum();
		}else{
			dex = dexb;
			flip = sol[dexb]->getFlipNum();
		}
	}



	if(sol[dex]->getBoundary().size()>0)						//yeni ekledim ama displayBoundary ninde vector olmasi lazim deilmi?
		this->displayBoundary = sol[dex]->getBoundary()[0];
	else
		this->displayBoundary = -1;

	double fb[3][3], tb[3][3];
	sol[dex]->getFromTo(fb, tb);
	this->setViewCenter(this->helA, Utils::getIdentityMatrix());
	vector<double> trans_b = Utils::getTransMatrix(fb,tb);

	this->render(this->helA,Utils::getIdentityMatrix(), mat_green,false,wire);
	this->render(this->helB, trans_b,mat_blue,true,wire);

	if(Settings::Constraint::wholeCollision){
		this->render(this->helA,ConstraintCheck::nei_matrix,mat_red,false,wire);
		this->render(this->helB,
				Utils::multiTrans(ConstraintCheck::nei_matrix,trans_b),
				mat_red,false,wire);
	 }
	 this->renderCorrect();

	this->xfHelA = this->helA->getXFAtoms(Utils::getIdentityMatrix());
	this->xfHelB = this->helB->getXFAtoms(trans_b);
	findDistortions();
	vector<Atom*>::iterator it2;
	for(it2 = xfHelA.begin();it2 != xfHelA.end(); it2++){
		delete (*it2);
	}
	for(it2 = xfHelB.begin();it2 != xfHelB.end(); it2++){
		delete (*it2);
	}
	xfHelA.clear();
	xfHelB.clear();
}

void SweepView::drawLines(){
	if(!showlines)
		return;

			vector<pair<int,int> > solid = this->currentID->getParticipants();
			vector<pair<int,int> > param = this->currentID->getParamLines();
			size_t it;
			for(it = 0; it < solid.size(); it++)
			{
				float color[] = {0.8,0.8,0.8};
				this->bufferLin(this->xfHelA[solid[it].first]->getLocation(),
						this->xfHelB[solid[it].second]->getLocation(),
						color,false);
			}
			GLfloat col[4][3] = {{1.0,0.0,0.0}, {0.0,1.0,0.0}, {0.0,0.0,1.0}, {1.0,1.0,0.0}};
			for(it = 0;it < param.size();it++)
			{

				this->bufferLin(this->xfHelA[param[it].first]->getLocation(),
						this->xfHelB[param[it].second]->getLocation(),
						col[it % 4],true);
			}
}

/*
 * this function is designed to give a value for amount of distortion within each helix when transformed to thei new positions.
 */
void SweepView::findDistortions(){
	double distA, distB, distorgA, distorgB;
	distA = Utils::dist(this->xfHelA.front()->getLocation(),this->xfHelA.back()->getLocation());
	distB = Utils::dist(this->xfHelB.front()->getLocation(),this->xfHelB.back()->getLocation());

	distorgA = Utils::dist(this->helA->getAtomAt(0)->getLocation(),this->helA->getAtomAt(this->xfHelA.size()-1)->getLocation());
	distorgB = Utils::dist(this->helB->getAtomAt(0)->getLocation(),this->helB->getAtomAt(this->xfHelB.size()-1)->getLocation());

	if(distorgA !=0){
		this->distortionA = distA / distorgA;
	}
	if(distorgB !=0){
		this->distortionB = distB / distorgB;
	}
}

Orientation* SweepView::getDisplayedOrient(){
	if(!showControls){
		return this->currentSpace[this->active]->getOrientations()[this->curIndex];
	}else{
		return (this->currentSpace[this->active]->getOrientations())[this->playIndex];
	}

}

void SweepView::incIndex(short inc){
	this->dirtyBit =true;
	if(this->showAll){
		if(this->flipFilter == 8 && inc > 0){
			this->flipFilter = 0;
		}else if(this->flipFilter == 0 && inc < 0){
			this->flipFilter = 8;
		}else{
			this->flipFilter += inc;
		}
	}else if(this->showControls){
		this->curIndex += inc;
		size_t siz = this->currentSpace[this->playIndex]->getOrientations().size(); //ayse
		if(this->curIndex >= siz || this->curIndex < 0){
			this->curIndex = 0;
		}

		cout << "flip [" << this->playflip;
		this->playflip = this->currentSpace[this->playIndex]->getOrientations()[this->curIndex]->getFlipNum(); //ayse
		cout << "->" << this->playflip << "]" << endl;

	}else{
		this->curIndex += inc;
		size_t siz = this->currentSpace[this->active]->getOrientations().size();
		if(this->curIndex >= siz || this->curIndex < 0){
			this->curIndex = 0;
		}
		cout << "realization [" << this->curIndex+1 << "/" << siz << "]" << endl;
	}
}
void SweepView::toggleShowall(){
	this->dirtyBit =true;
	if(!this->showControls){
		this->showAll = ! this->showAll;
		if(this->showAll){
			this->flipFilter = 8;
		}
	}
}


void SweepView::toggleShowstree(){
	this->dirtyBit = true;
	this->showStree = ! this->showStree;
}

void SweepView::toggleShowCorrect(){
	this->dirtyBit = true;
	this->showCorrect = ! this->showCorrect;
}

void SweepView::toggleShowNeighbour(){
	this->dirtyBit = true;
	this->showNeighbour = ! this->showNeighbour;
}

void SweepView::displayHUD(double ratio,bool select,int x, int y){
	bool empty = this->currentSpace.empty();
	glDisable(GL_DEPTH_TEST);
	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	if(select)
	{
		GLint viewport[4];
		glGetIntegerv(GL_VIEWPORT,viewport);
		gluPickMatrix(x,viewport[3]-y,3,3,viewport);
	}
	glOrtho(-1*ratio,ratio,-1,1,-10,10);
	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

	glDisable(GL_LIGHTING);
	glColor3f(.5,.5,.5);
	glPushName(SweephudId);
	if(this->displayBoundary > -1){
			glColor3f(.9,.9,.9);
			glPushName(10);
			glPushName(this->displayBoundary);
				glBegin(GL_QUADS);
					glVertex3f(0.25,0.80,0);
					glVertex3f(0.25,0.90,0);
					glVertex3f(0.6, 0.90,0);
					glVertex3f(0.6, 0.80,0);
				glEnd();
			glPopName();
			glPopName();
			glColor3f(.4,.4,.4);
		stringstream ss;
		ss << "Makes a boundary with node " << this->displayBoundary;
		drawString(.30,.85,0,ss.str());
	}

	if(this->showAll){
		double b[2] = {-0.93,.30};
		glPushName(5);
		if(this->showBound){
			glPushName(0);
				glColor3f(1,1,1);
				glBegin(GL_QUADS);
				glVertex3f(b[0],b[1],0);
				glVertex3f(b[0]+.15,b[1],0);
				glVertex3f(b[0]+.15,b[1]-.04,0);
				glVertex3f(b[0],b[1]-.04,0);
				glEnd();
				glColor3f(.5,.5,.5);
			drawString(b[0]-.01,b[1]-.02 ,0.0,"[Boundaries]");
			glPopName(); // end 5-0
			glPushName(1);
				glBegin(GL_TRIANGLES);
				glVertex3f(b[0]-.02,b[1]-.04,0);
				glVertex3f(b[0]-.04,b[1],0);
				glVertex3f(b[0]-.06,b[1]-.04,0);
				glEnd();
			glPopName();//end 5-1
			glPushName(2);
				glBegin(GL_TRIANGLES);
				glVertex3f(b[0]+.17,b[1],0);
				glVertex3f(b[0]+.19,b[1]-.04,0);
				glVertex3f(b[0]+.21,b[1],0);
				glEnd();
			glPopName();//end 5-2
			glPopName(); //end 5



		}else{ // b[2] = {-0.93,.30};
			glPushName(0);
				glColor3f(1,1,1);
				glBegin(GL_QUADS);
				glVertex3f(b[0],b[1],0);
				glVertex3f(b[0]+.15,b[1],0);
				glVertex3f(b[0]+.15,b[1]-.04,0);
				glVertex3f(b[0],b[1]-.04,0);
				glEnd();
				glColor3f(.5,.5,.5);
			drawString(b[0],b[1]-.02 ,0.0,"Boundaries");
			glPopName(); // end 5-0
			glPopName(); //end 5
		}
		if(this->boundex > 0){
			float col[3];
			Utils::huetoColor(this->boundaries[this->curBoundlist[this->boundex-1]],col);
			glColor3fv(col);
			stringstream ss;
			ss << this->curBoundlist[this->boundex-1];
			drawString(b[0],b[1] - .06 ,0.0,ss.str() );

			glColor3f(.5,.5,.5);
		}
	}

	{
		stringstream ss;
		ss << "Distortions a/b = " << this->distortionA << "/" << this->distortionB;
		drawString(-0.1,-.95,0,ss.str());
	}
	if(this->showAll){
		stringstream ss;
		ss << "Flips ";
		for(size_t i = 0; i < 8; i++){
			if(this->flipFilter == i || this->flipFilter == 8u){
				ss << "(" << i << ") ";

			}else{
				ss << "." << i << ". ";
			}
		}
		drawString(.4,-.75,0,ss.str());
	}else if(!empty){
		stringstream ss;
		ss << "Flips ";

		if(this->playIndex<0 ||  this->playIndex>this->currentSpace.size() )
			this->playIndex = this->active;
		vector<Orientation*> sol = this->currentSpace[this->playIndex]->getOrientations();  // ayse


		for(size_t j = 0; j < sol.size() ; j++){
			int jflip = sol[j]->getFlipNum();
			if(this->playflip == jflip){
				ss << "^" << jflip << "^ ";
			}else{
				ss << "(" << jflip << ") ";
			}
		}

		drawString(.4,-.75,0,ss.str());
	}
	glPushName(0);

	glColor3f(.8,.8,.8);
	if(this->showControls){


		glBegin(GL_QUADS);
			glVertex3f(.5,-1,0);
			glVertex3f(.5,-.8,0);
			glVertex3f(.9,-.8,0);
			glVertex3f(.9,-1,0);
		glEnd();


		glColor3f(.5,.5,.5);
		double ratio = double(this->playIndex) / double(this->spaceSize);
		{
			stringstream ss;
			ss << this->fps << "fps Point " << this->playIndex << "/" << this->spaceSize;
			drawString(0.0,-.99,0,ss.str());
		}




		glColor3f(.3,.3,.8);
		glBegin(GL_QUADS);
			glVertex3f(.55,-.985,0);
			glVertex3f(.85,-.985,0);
			glVertex3f(.85,-.99,0);
			glVertex3f(.55,-.99,0);


			glVertex3f(.55+(.3*ratio),-.98,0);
			glVertex3f(.555+(.3*ratio),-.98,0);
			glVertex3f(.555+(.3*ratio),-.995,0);
			glVertex3f(.55+(.3*ratio),-.995,0);
		glEnd();

		glLoadName(1);
		if(this->pause){
			glColor3f(.8,.8,.8);

			glBegin(GL_QUADS); //pause selected
				glVertex3f(.59,-.96,.5);
				glVertex3f(.59,-.84,.5);
				glVertex3f(.68,-.84,.5);
				glVertex3f(.68,-.96,.5);
			glEnd();

			glColor3f(.3,.3,.8);
		}
		glBegin(GL_QUADS); //pause
			glVertex3f(.6,-.95,1);
			glVertex3f(.6,-.85,1);
			glVertex3f(.63,-.85,1);
			glVertex3f(.63,-.95,1);

			glVertex3f(.64,-.95,1);
			glVertex3f(.64,-.85,1);
			glVertex3f(.67,-.85,1);
			glVertex3f(.67,-.95,1);
		glEnd();

		glLoadName(2);

		if(this->play){
			glColor3f(.8,.8,.8);

			glBegin(GL_QUADS); // play selected
				glVertex3f(.69,-.96,.5);
				glVertex3f(.69,-.84,.5);
				glVertex3f(.75,-.84,.5);
				glVertex3f(.75,-.96,.5);
			glEnd();

			glColor3f(.3,.3,.8);
		}
		glBegin(GL_TRIANGLES);//play
			glVertex3f(.7,-.95,1);
			glVertex3f(.74,-.9,1);
			glVertex3f(.7,-.85,1);
		glEnd();

		glLoadName(3);
		if(this->rewind){
			glColor3f(.8,.8,.8);

			glBegin(GL_QUADS); // rew selected
				glVertex3f(.51,-.96,.5);
				glVertex3f(.51,-.84,.5);
				glVertex3f(.59,-.84,.5);
				glVertex3f(.59,-.96,.5);
			glEnd();

			glColor3f(.3,.3,.8);
		}
		glBegin(GL_TRIANGLES);//rew
			glVertex3f(.58,-.95,1);
			glVertex3f(.55,-.90,1);
			glVertex3f(.58,-.85,1);
			glVertex3f(.55,-.95,1);
			glVertex3f(.52,-.90,1);
			glVertex3f(.55,-.85,1);
		glEnd();


		glLoadName(4);
		glBegin(GL_QUADS); //stop
			glVertex3f(.78,-.95,1);
			glVertex3f(.78,-.85,1);
			glVertex3f(.85,-.85,1);
			glVertex3f(.85,-.95,1);
		glEnd();


	}else{
		glBegin(GL_QUADS);
			glVertex3f(.5,-1,0);
			glVertex3f(.5,-.95,0);
			glVertex3f(.9,-.95,0);
			glVertex3f(.9,-1,0);
		glEnd();
		glColor3f(.3,.3,.8);
		drawString(.55,-.99,0,"VIDEO CONTROLS");
	}
	glPopName();//end 0

	glPopName();//end sweephudID
	glEnable(GL_LIGHTING);



	glMatrixMode(GL_PROJECTION);
	glPopMatrix();
	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();
	glEnable(GL_DEPTH_TEST);
}

void SweepView::takePick(vector<int> hitnames){
	if(hitnames.size() > 1 && hitnames[0] == SweephudId){
		this->dirtyBit =true;
		switch(hitnames[1]){
		case 0 : // control box
			{
				if(! this->currentSpace.empty()){
					this->showControls = !this->showControls;
					if(this->showControls){
						this->showAll = false;
					}
					this->playIndex =this->active;
					this->playflip = this->currentSpace[this->active]->getOrientations()[this->curIndex]->getFlipNum(); //setting the flip number of the normal view.
				}
			}
			break;
		case 1 ://pause
			{
				this->dirtyBit = false;
				this->moviePause();
			}
			break;
		case 2 ://play
			{
				this->moviePlay();
			}
			break;
		case 3 ://rew
			{
				this->movieRew();
			}
			break;
		case 4 ://stop
			{
				this->movieStop();
			}
			break;

		case 5 ://boundary view
			{
				switch(hitnames[2]){
					case 0 : // bounds
					{
						this->showBound = ! this->showBound;
						this->boundex = 0;
					}
					break;
					case 1 : // left arrow
					{
						if(this->boundex == 0){
							this->boundex = this->curBoundlist.size();
						}else{
							this->boundex--;
						}

					}
					break;
					case 2 : // right arrow
					{
						if(this->boundex == this->curBoundlist.size()){
							this->boundex = 0;
						}else{
							this->boundex++;
						}
					}
					break;
				}
			}
		}
	}
}

void SweepView::setActive(size_t index){
	this->active = index;
	this->dirtyBit = true;
}


size_t SweepView::getActive(){
	return this->active;
}

void SweepView::getSweepMolecularUnit(MolecularUnit* helA,MolecularUnit* helB){
	helA = this->helA;
	helB = this->helB;
}

void SweepView::moviePlay(){
	if(this->play){
		this->delay *= 0.5;
	}else{
		this->delay = .5;
	}
	this->play = true;
	this->rewind = false;
	this->pause = false;
}
void SweepView::moviePause(){
	if(this->play || this->rewind)
	{
		this->pause = ! this->pause;
	}
}

void SweepView::movieRew(){
	if(this->rewind){
		this->delay *= 0.5;
	}else{
		this->delay = 0.5;
	}
	this->rewind = true;
	this->pause = false;
	this->play = false;
}

void SweepView::movieStop(){
	this->play = false;
	this->rewind = false;
	this->pause = false;
	this->playIndex =this->active;
	this->playflip = (this->currentSpace[this->active]->getOrientations())[this->curIndex]->getFlipNum();
	this->delay = 0.5;
}

void SweepView::trackFlip(size_t flip, bool track){
	this->trackflip = track;
	if(this->trackflip)
	{
		this->playflip = flip;
	}

}

void SweepView::setMulti(size_t multi){
	this->dirtyBit=true;
	this->multi = multi;
}

void SweepView::togglesOff(){
	this->dirtyBit=true;
	this->showControls = false;
	this->showAll = false;
	this->flipFilter = 8u;
	this->showBound = false;
}

void SweepView::showLines()
{
	this->showlines = !this->showlines;
}

void SweepView::showinterior(){
	this->showInterior = !this->showInterior;

}


void SweepView::setViewCenter(MolecularUnit* h1,MolecularUnit* h2,double fa[][3], double ta[][3],double fb[][3], double tb[][3] ){
	
	vector<double> trans_a = Utils::getTransMatrix(fa,ta);
	vector<double> trans_b = Utils::getTransMatrix(fb,tb);
	Vector c = h1->stree->root.center.trans(trans_a) + h2->stree->root.center.trans(trans_b);
	for(int i = 0; i < 3; i++){
		this->viewcenter[i] = c[i]/2;
	}
	cout << h1->stree->root.center[0] << "," << h1->stree->root.center[1] <<"," << h1->stree->root.center[2] << endl;
	cout << h2->stree->root.center[0] << "," << h2->stree->root.center[1] <<"," << h2->stree->root.center[2] << endl;
	cout << this->viewcenter[0] << "," << this->viewcenter[1] <<"," << this->viewcenter[2] << endl;
	cout << "set center 970" << endl;
}

void SweepView::setViewCenter(MolecularUnit* h,const vector<double> &trans_mat){
	double vcenter[3] = {0};
	vector<Atom*> atoms = h->getAtoms();

	for(vector<Atom*>::iterator iter = atoms.begin();   iter != atoms.end();    iter++){
		for(int i = 0; i < 3; i++)
			vcenter[i] += (*iter)->getLocation()[i];
	}
	vcenter[0] /= atoms.size();
	vcenter[1] /= atoms.size();
	vcenter[2] /= atoms.size();
	Vector real_center = Vector(vcenter).trans(trans_mat);
	for(int i = 0; i < 3; i++){
		this->viewcenter[i] = real_center[i];
	}
}

void SweepView::render(MolecularUnit* hlx,const vector<double> &trans_mat,float *material,bool repeat,bool wire){
    if(this->showStree){
    	if(repeat){
    		vector<double> trans_m = Utils::getIdentityMatrix();
    		cout << "show " << this->multi+1 << endl;
    		for(unsigned int i = 0; i < this->multi+1; i++){
    			trans_m = Utils::multiTrans(trans_m,trans_mat);
    			renderSnode(&(hlx->getStree()->root),trans_m,material,wire);
    		}
    	}
    	else
    		renderSnode(&(hlx->getStree()->root),trans_mat,material,wire);
    }


	vector<Atom*> atoms = hlx->getXFAtoms(trans_mat);//be sure to properly delete XFAtoms

    //this loop adds successive copies for seeing Collections as in a virus' trimer and pentamer
	if(repeat)for(size_t m = 0; m+1 < this->multi;m++){
		vector<Atom*> atomsMer = hlx->getXFAtoms(trans_mat,m+1);
		atoms.insert(atoms.end(),atomsMer.begin(),atomsMer.end());
	}

	vector<Atom*>::iterator iter;
	for(iter = atoms.begin();iter!=atoms.end();iter++)//atoms is a local vector so the use of an iterator is OK
	{
		double* loc = (*iter)->getLocation();//makes new array
		double rad = (*iter)->getRadius();
		float act_mat[4];
		act_mat[0] = material[0];
		act_mat[1] = material[1];
		act_mat[2] = material[2];
		act_mat[3] = material[3];
//		glMaterialfv(GL_FRONT, GL_DIFFUSE, material);

		this->bufferBall(loc,act_mat,rad,wire);
		delete (*iter); // since they are used once and tossed this should be ok.
	}

    atoms.erase(atoms.begin(),atoms.end());
}


void SweepView::renderSnode(snode* node, const vector<double> &trans_mat, float *material, bool wire){
	if(node->is_leaf()){
		this->bufferBall(node->center.trans(trans_mat).getLoc(),material,node->radius,wire);
	}
	else{
		for(list<snode*>::iterator iter = node->children.begin();
			iter != node->children.end();
			iter++)
		{
			renderSnode((*iter),trans_mat,material,wire);
		}
	}
}

void SweepView::bufferBall(double *locat,float *color, double rad,bool wire){
	Bal buf;
	for(size_t i = 0; i<3;i++){
		buf.loc[i] = locat[i];
		buf.col[i] = color[i];
	}
	buf.col[3] = color[3]; //alpha
	buf.rad = rad;
	buf.wire = wire;
	this->viewBalls.push_back(buf);
}
void SweepView::bufferLin(double *loca,double *locb, float *color, bool dotted){
	Lin buf;
	for(size_t i = 0; i<3;i++){
		buf.loc[0][i] = loca[i];
		buf.loc[1][i] = locb[i];
		buf.col[i] = color[i];
	}
	buf.dot = dotted;
	this->viewLines.push_back(buf);
}



void SweepView::drawBuffer(){
	glPushMatrix();
#if 0
	double vcenter[3] = {0};
	int count  = 0;

	for(list<Bal>::iterator iter = this->viewBalls.begin();iter != this->viewBalls.end();iter++){
			count ++;
			vcenter[0] += iter->loc[0];
			vcenter[1] += iter->loc[1];
			vcenter[2] += iter->loc[2];
	}
	vcenter[0] /= count;
	vcenter[1] /= count;
	vcenter[2] /= count;

	glTranslatef(-vcenter[0],-vcenter[1],-vcenter[2]);
#endif
	glTranslatef(-this->viewcenter[0],-this->viewcenter[1],-this->viewcenter[2]);

	for(list<Bal>::iterator iter = this->viewBalls.begin();iter != this->viewBalls.end();iter++){
		glPushMatrix();
			glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE,iter->col);
			glTranslatef(iter->loc[0],iter->loc[1],iter->loc[2]);
			glutSolidSphere( iter->rad , 8, 6);
		glPopMatrix();
	}

	glDisable(GL_LIGHTING);
	glDisable(GL_DEPTH_TEST);

	glLineWidth(3.0);
	for(list<Lin>::iterator iter = this->viewLines.begin();iter != this->viewLines.end();iter++){
		glColor3fv(iter->col);
		if(iter->dot){
			glBegin(GL_LINES);
				for(double f = 1.0;f > 0;f -=.222){
					double x1,y1,z1;
					double x2,y2,z2;
					double f2 = f - .111;
					x1 = ((iter->loc[0][0] * f) + (iter->loc[1][0] * (1 - f)) );
					y1 = ((iter->loc[0][1] * f) + (iter->loc[1][1] * (1 - f)) );
					z1 = ((iter->loc[0][2] * f) + (iter->loc[1][2] * (1 - f)) );
					glVertex3f(x1,y1,z1);
					x2 = ((iter->loc[0][0] * f2) + (iter->loc[1][0] * (1 - f2)) );
					y2 = ((iter->loc[0][1] * f2) + (iter->loc[1][1] * (1 - f2)) );
					z2 = ((iter->loc[0][2] * f2) + (iter->loc[1][2] * (1 - f2)) );
					glVertex3f(x2,y2,z2);
				}
			glEnd();
		}else{
			glBegin(GL_LINES);
				glVertex3dv(iter->loc[0]);
				glVertex3dv(iter->loc[1]);
			glEnd();
		}

	}
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_LIGHTING);


	glPopMatrix();
}

void SweepView::renderCorrect(){

	static float mat[] = {-1,0,0,0,
						   0,-1,0,0,
						   0,0,1,0
	};
	static float mat_corr[4]={1.0, 0.0, 0.0, 1.0};
	if(showCorrect){
		vector<vector<double> > mats = Utils::getMatrixFromFileMat("mat.txt");
		for(int i = 0; i < mats.size(); i++){
			this->render(this->helA,mats[i],mat_corr);
		}
	}
}
