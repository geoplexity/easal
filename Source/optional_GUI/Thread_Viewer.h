/*
 This file is part of EASAL. 

 EASAL is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 EASAL is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef THREAD_VIEWER_H_
#define THREAD_VIEWER_H_

#include "Thread.h"

#include "MolecularUnit.h"
#include "SaveLoader.h"
#include "AtlasBuilder.h"
#include "AtlasDisplay.h"
#include "Atlas.h"
#include "ThreadShare.h"
#include "Thread_BackEnd.h"

// Thread_Viewer is the thread that handles everything about the GUI, GLUT, GL, etc.
class Thread_Viewer : public Thread {
public:
    Thread_Viewer(
        int argc,
        char **argv,
        MolecularUnit *muA,
        MolecularUnit *muB,
        SaveLoader *save_loader,
        AtlasBuilder *cycle,
        Atlas *mapView,
	Thread_BackEnd *tb
    );

    void run();
};

#endif
