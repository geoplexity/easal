```
    _________   _____ ___    __
   / ____/   | / ___//   |  / /
  / __/ / /| | \__ \/ /| | / /
 / /___/ ___ |___/ / ___ |/ /___
/_____/_/  |_/____/_/  |_/_____/


```

Description
===========
This software is the open-source project EASAL: Efficient Atlasing, Analysis and Search of Molecular Assembly Landscapes. For a detailed explanation of the theory behind the concepts in this program, refer to the papers referenced in Publications.

This project leverages the convexity of the Cayley parameter space for atlasing and sampling molecular assembly configuration spaces. With respect to many performance measures the method is provably superior to the typical Monte Carlo approach. In particular it achieves comparable coverage of the assembly configuration space far more efficiently and permits flexible tradeoff between accuracy and efficiency. Hybridizing monte carlo with EASAL promises the best of both worlds.

This project should be viewed primarily as mathematical software. For routine use by molecular scientists it would require significant improvement in its user friendliness.

A web version of the software is under development as of 20-May-2016.

This version of EASAL is not part of the TOMS submission.

The source code for this can be found in the Source/optional_GUI folder. Instructions on how to install, how to use and major functionalities offered by the GUI have been detailed 
in the `Complete User Guide'.


Requirements
============
- Operating system: 
	- Ubuntu 12.04 or higher, 
	- Fedora 23 or higher, 
	- OSX Darwin or higher.
- C++ compiler
	- g++ version 4.8 or higher
	- clang++ version 3.3 or higher
- The following requirements are for the optional GUI included
	- OpenGL support
	- Fox Toolkit 1.6
	- Nvidia Graphics card (with driver version-331 or above)

This software uses the FOX Toolkit (http://www.fox-toolkit.org).

Installation
============
- For the backend, all the required third party headers are included in the submission
	- Run ‘make‘ from the root/build directory.
	- To run EASAL run ‘bin/EASAL’ from the root/build directory.
- For the optional GUI, you will need to download the third party libraries:
	- Install GLUT
		- sudo apt-get install freeglut3 freeglut3-dev
		- sudo apt-get install binutils-gold
	- Install FOX-Toolkit (version 1.6)
		– sudo apt-get install libfox-1.6-0 libfox-1.6-dev
	- Install GNU Make
		- sudo apt-get install make
	- Run ‘make‘ from the root/build directory.
	- To run EASAL run ‘bin/guiEASAL’ from the root/build directory.

Resources
=========
1. Input Files
	The 'files' directory in the root folder contains all the example input molecular data.
2. Data Directory 
	The data directory chosen by the user in the Input Window stores the atlas.
3. path_matrix.txt - The file in which the path matrix of a particular user input length for all pairs of 0D and 1D nodes is stored.
4. paths.txt - The file in which we output the shortest paths between 0D nodes that were clicked in the atlas view.

Usage
=====
- Command-line arguments
    - `-settings` Gives the settings file from which to load the input.
- Input Window
    - The first thing you will see when running the software with GUI.
    - It will present you with standard settings that you can alter.
    - You will need to point the program to:
        - The two input point sets.
        - Distance data for the participating atoms.
        - A directory in which to store the atlas. If this directory already contains an atlas you will be prompted to either overwrite the old data or to continue where the other was left off.
- Main View
    - Center: Three distinct options are available: Atlas view, Cayley space view, and sweep view.
    - Top left: the "hints", a list of keyboard shortcuts.
    - Top right: the sweep view of the currently selected node.
    - Bottom left: the contact graph of the currently selected node.
    - Left: Controls for interacting with the current view.
- Atlas View
    - Shows the main atlas.
    - This has controls for stopping/re-starting sampling, choosing a different root node, sampling in a BFS fashion, and refining the sampling.
    - Clicking on 0D nodes finds the shortest path between the node and the previously selected node, if a path exists and writes it to the paths.txt file in the root directory.
- Constraint Selection Dialogue Box
    - Lets you choose a node based on the participating contacts.
    - If it doesn't already exist, it is created. Otherwise, the node is selected and loaded.
- Cayley Space View
    - Shows the Cayley Space of the currently selected node.
    - For nodes of dimensions higher than 3, sliders at the bottom right corner allow users to step through each dimension.
    - Allows the user to inspect the boundaries of each node.
- Sweep View
    - Shows the Cartesian Sweep of the currently selected node.
    - Pressing `v' generates a sweep of all possible realizations for the node selected.
    - The user can step through all possible flips by pressing the up and down arrow keys.
    - Clicking on boundaries generates the sweep of all realizations along different boundaries of the Cayley space.
    - Video controls at the bottom allow for animation of all the realizations and their flips.

Example
=======
- Run EASAL from the command line by running the following command.
    `$bin/EASAL' - for the backend
    `$bin/guiEASAL' - for the optional GUI

- The rest of the example covers featrues of the optional GUI

- In the input window, select the following either using the browse option or by entering the text in the text box provided
    - Data for Molecule A - files/A.pdb
    - Data for Molecule B - files/B.pdb
    - Distance Data - files/source_files/union computed desired distances.txt
    - Data Directory - data/

- The user can edit the data for Molecular Unit A, Molecular Unit B and Distance Data loaded from files by clicking on 'Set Data' and editing the values in the new pop-up that appears.

- Enter the values for Bonding Thresholds and step size . The user can either change the values for all these or just continue with the default values.

- The user can then click on the 'Advanced Options' button to set advanced user inputs. This opens a new pop-up where the user can enter either enter the data or choose to accept the default values.

- Click on 'Accept'. This opens the Atlas View.

- In the Atlas View, we initially see a root node at the center of a 3D grid. As and when more nodes are discovered, they are populated on the Atlas.

- The user can control how the atlas is displayed using the following keyboard controls
	- '+' - Zoom into the atlas
	- '-' - Zoom out of the atlas
	- 'f' - Toggle forces. EASAL implements a spring repulsion algorithm to display the atlas. This toggles the spring repulsion on and off.
	- 'l' - Enables level alignment. Enabling this arranges the nodes of the atlas according to their dimensionality.
	- 'i' - Align view to x axis.
	- 'j' - Align view to y axis.
	- 'k' - Align view to z axis.
	- 't' - Toggles the tree view. When the tree view is on, it shows only the tree the currently selected node is part of. When it is off, it shows the entire atlas. This can also be achieved by clicking the Tree control at the bottom.
	- '1' - Toggles the display of node numbers.

- In this view, the user can control how the sampling proceeds by using any of the controls on the left side of the atlas. The different controls available are (in the order they appear)
    - Stop Sampling.
    - Constraint Selection Dialogue Box.
    - Sample the Current Tree.
	- Sample all Incomplete Trees.
	- Auto-Solve.
    - BFS Sampling.
    - Refine Sampling.

- Clicking on a particular node does the following
    - Loads the Active Constraint Graph at the bottom left corner.
    - Loads the Cartesian realization for that node in the top right corner if the sampling for the node is complete.

- Pressing the space bar after selecting a node takes the user to the 'Cayley Space View' of that node.

- In the Cayley Space View, initially green points are shown which correspond to all the realizable points in the Cayley space.

- Clicking on the Red square at the bottom, shows the points that have collision. Clicking the red also shows two more options viz, cyan and pink. The Cyan points represent the points which have angle collision and the pink points represent points which have distance collision. 

- Clicking on the Blue square shows all the points sampled including, the good, the collision and the unrealizable.

- Clicking on the Boundaries shows the boundary points and the user can step through them along each dimension.

- Pressing the space bar here takes the user to the 'Realization View'

- Pressing 'v' on the keyboard generates the sweep view of the molecule.

- Once the sweep view is generated, the user can use the up and down arrow keys to view all the flips of the molecule.

- Clicking on boundaries shows the different realization along different boundaries.

- Clicking on the video controls at the bottom and clicking the play button on it animates and shows all the possible realizations of the molecule.

- Once sampling is completed, we find the number of paths between every pair of 0D and 1D nodes and write it to the path_matrix.txt file.

Basic Code Overview
===================
Note that ^ means, explained further below.
```
.main
|
|__PointSet
|
|__^Atlas
|
|__^AtlasBuilder
|
|__.Settings
|
|__.SaveLoader
|
|__.readIn
```

```
.Atlas
|
|__.AtlasNode
   |
   |__.ActiveConstraintRegion
   |  |
   |  |__.CayleyPoint
   |     |
   |     |__.Orientation
   |
   |__.ActiveConstraintGraph
      |
      |__.ConvexChart
      |
      |__.CayleyParameterization
      |
      |__.CgMarker
```

```
.AtlasBuilder
|
|__.PointSet A/B
|
|__.PredefinedInteractions
|
|__.SaveLoader
|
|__.Atlas
|
|__.CgMarker
|
|__.ConstraintCheck
|
|__.CartesianRealizer
```

License
=========
EASAL is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.
The GNU GPL license can be found at  <http://www.gnu.org/licenses/>.

Authors
=======
- James Pence
- Aysegul Ozkan
- Rahul Prabhu
- Troy Baker

Contact
=======
Meera Sitharam, CISE @ UF


Publications
============
 - Aysegul Ozkan and Meera Sitharam. 2011. ``EASAL: Efficient Atlasing, Analysis and Search of Molecular Assembly Landscapes''. In Proceedings of the ISCA 3rd International Conference on Bioinformatics and Computational Biology (BICoB-2011).
 - Aysegul Ozkan and Meera Sitharam. 2014. ``Best of Both Worlds: Uniform sampling in Cartesian and Cayley Molecular Assembly Configuration Space''. (2014). (on arxiv).
 - Aysegul Ozkan, Ruijin Wu, Jorg Peters, and Meera Sitharam. 2014b. ``Efficient Atlasing and Sampling of Assembly Free Energy Landscapes using EASAL: Stratification and Convexification via Customized Cayley Parametrization.'' (2014). (on arxiv).
