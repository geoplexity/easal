# C++ compiler
CC = g++

# C flags
CFLAGS= -w -g -c
CLINKFLAGS = -Wall -g
CSTANDARD = -std=c++11

# Paths
backEndSRC = Source/backend_TOMS_Submission
guiSRC = Source/optional_GUI

BIN = bin
backendBIN = bin/backend
GUIBIN = bin/gui

EXE = bin/EASAL
GUIEXE = bin/guiEASAL

SRC = Source
BIN = bin
EXE = $(BIN)/EASAL

# Check if windows
ifeq ($(OS), Windows_NT)
    uname_S := Windows
else
    uname_S := $(shell uname -s)
endif

# Include directories
INC_DIRS = -I ./include/ 

# Libraries
LIBS = -lFOX-1.6 -lglut -lGL -lGLU -lpthread

# Binary objects (add any new *.cpp file)
_OBJ = ActiveConstraintGraph ActiveConstraintRegion Atlas AtlasBuilder \
	AtlasNode Point CartesianRealizer \
	CayleyParameterization CayleyPoint CgMarker \
	ConstraintCheck ConvexChart main PointSet Orientation \
	PredefinedInteractions SaveLoader Settings Stree \
	Utils
OBJ = $(patsubst %, $(backendBIN)/%.o, $(_OBJ))

_GuiOBJ = AtlasDisplay AtlasNode AtlasNodeSelectionWindow Atom CartesianRealizer \
	 ActiveConstraintGraph ActiveConstraintRegion Atlas AtlasBuilder \
	CayleyParameterization CayleyPoint CayleySpaceView CgMarker \
	ConstraintCheck ConvexChart LaunchWindow main MolecularUnit Orientation \
	PredefinedInteractions SaveLoader SelectionWindow Settings Stree SweepView \
	Thread_Viewer  Thread_BackEnd Utils \
	
guiOBJ= $(patsubst %, $(GUIBIN)/%.o, $(_GuiOBJ))


# Make instructions
all: $(GUIBIN) $(GuiOBJ) $(GUIEXE)
	@echo "Finished."

backend: $(backendBIN) $(OBJ) $(EXE)
	@echo "Finished."

$(backendBIN): 
	mkdir -p bin/backend 

$(GUIBIN):
	@echo "\n\nWARNING: You are about to build the GUI version of EASAL. \nThis has been included solely for intuitive visual verification of the results.\nThis is NOT part of the TOMS submission.\n\n"
	mkdir -p bin/gui

$(backendBIN)/%.o: $(backEndSRC)/%.cpp
	$(CC) -c -o $@ $< $(CFLAGS) $(CSTANDARD) $(INC_DIRS)

$(GUIBIN)/%.o: $(guiSRC)/%.cpp
	$(CC) -c -o $@ $< $(CFLAGS) $(CSTANDARD) $(INC_DIRS)

$(EXE): $(OBJ)
	$(CC) -o $@ $^ $(CLINKFLAGS) $(LIB_DIRS) 

$(GUIEXE): $(guiOBJ)
	$(CC) -o $@ $^ $(CLINKFLAGS) $(LIB_DIRS) $(LIBS)

.PHONY: clean
clean:
	rm -rf $(BIN) 
