# Google Summer of Code 2021 

Ideas List
==========

Desktop GUI development
-----------------------------------
**Overview:**
Develop GUI features in the Desktop version of the EASAL open source mathematical software. EASAL implements a suite of algorithms that characterize the structure and geometric properties of the configuration space of point-sets constrained by distance intervals. Most of the results produced by the software are need to be visualized to aid in intuitive understanding. While the desktop version of the software is quite mature, it still needs several new GUI elements for visualizing results. This project would involve writing GUI elements to visualize the latest cutting edge results of EASAL.

**Required Skills:** 

- C++
- QT5
- OpenGL knowledge is a bonus.

Web GUI development
----------------------------
**Overview:**
Develop GUI features in the web version of the EASAL open source mathematical software. EASAL implements a suite of algorithms that characterize the structure and geometric properties of the configuration space of point-sets constrained by distance intervals. Most of the results produced by the software are need to be visualized to aid in intuitive understanding. The web version of the software is in a nascent stage and has very few features. This project would involve adding features that are present in the desktop version to the web version.

**Required Skills:**

- C++ 
- mongodb
- nodejs
- WebGL (Three.js) knowledge is a bonus 

Increasing EASAL Test Coverage 
---------------------------------------
**Overview**
This project involves increasing the unit and integration test coverage for the backend of the  EASAL software. This would require understanding the theory, algorithms and code related to the backend code and implementing test cases to test various features of EASAL.

**Required Skills:** 

- C++ 
- Google Test 
- Background in mathematical areas such as geometry and graph theory is a bonus
